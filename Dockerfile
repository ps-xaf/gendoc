###############################################################################
#
# GenDoc - Meta documentation generator for docToolchain
#
# (c) 2023 Holger Zahnleiter
#
###############################################################################

ARG DTC_DOCKER_IMAGE
FROM $DTC_DOCKER_IMAGE

COPY bin/gendoc-linux-amd64 /usr/local/bin/gendoc
COPY script/generate_docs.sh /usr/local/sbin/
COPY doc_templates /doc_templates

ENTRYPOINT ["/bin/bash", "-l", "-c"]
