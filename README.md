# gendoc

`gendoc` is kind of a preprocessor for [docToolchain](https://github.com/docToolchain/docToolchain).
Here we are using the containerized version of docToolchain (see [Docker Hub](https://hub.docker.com/r/doctoolchain/doctoolchain)).

This tool follows the [Arc42](https://arc42.org) way of documenting systems's architectures.
It is mainly inteded for documenting larger enterprise systems spanning multiple Git repositories.
However, it can also be applied on small projects living in a singel Repository.

2023-02-09, Holger Zahnleiter

## Introduction

### Why would I use gendoc instead of plain docToolchain?

`gendoc` tries to support you in your documentation efforts.

### How does gendoc support me?

-   `gendoc` collects information on your project from reliable (aka primary) sources so that you do not have to type them in.
-   It collects information on your project from "handwritten" (therefore less reliable) sources.
    These "handwritten" files can be used to purposefully override information automatically collected from the reliable sources.
-   `gendoc` generates [AsciiDoc](https://asciidoc.org) files for you.
    It therefore merges project information from automatically collected, reliable and handwritten sources with handwritten AsciiDoc from the `docs` folder.
-   And finally, docToolchain is used to generate a microsite for your GitLab Pages upload.
    (Of cause, docToolchain can also generate singel HTML page documentation, PDF and so on.)

### What are reliable sources?

Sources such as your Git repository, configurations (e.g. Helm charts), build files (e.g. Dockerfile, Gradle, Maven, CMake, etc.), formal models (openAPI aka Swagger) are considered "reliable".
This information is actually used to build your artifact, to deploy it etc.
By building, testing and deploying your artifact you are proving that the reliable sources are actually working - this is why they are considered "reliable".
They are an operative single source of truth.

### What are not so reliable sources?

This especially targets "handwritten" documentation.
By no meand does it say that documentation is worthless.
Especially in big systems documentation is important.
But it is prose, might be wrong or outdated.
And yet, well written documentation can really help understanding the system and its components.
Therefore `gendoc` relies on reliable sources.
But it also provides means to override and supplement these sources by hand.

`project.yaml` for example, provides the means to override everything from artifact name, to version and so on.

`version.properties` only overrides the version.
Such a file is seen on some projects.
It is used to provide version information for tagging Docker images for example.
However, I prefer a Git tag to determine the version (see trustworty sources).
Nevertheless, `version.properties` and `project.yaml` can be used to override the version information automatically collected by `gendoc`.

The `docs` folder holds AsciiDoc files that are merged with project information from various sources (see above).
It contains twelve files each dedicated to one topic as proposed by the Arc42 approach of documenting the architecture of software systems.

### But wait, there is more!

The meta information gathered by `gendoc` can be queried and connected with meta information from other Git repositories.
Real life systems are usually composed of many services and libraries.
These are usually kept in separate Git repositories.
`gendoc` can collect meta information on all of them.
You can than query for example which service is using which interface from another service.

## Command line use

`gendoc` accepts the following command line arguments.

| Argument    | Optional | Domain                 | Default      | Description |
|-------------|:--------:|------------------------|--------------|-------------|
| `lang`      | X        | `de`, `en`, `es`       | `en`         | Localization of diagrams and headline. |
| `type`      | X        | `single`, `enterprise` | `enterprise` | For an explanation see below. |
| `scm-url`   | X        | A URL                  | -            | Name the repository where other, related projects live. |
| `scm-token` | If `scm-url` not present | Arbitrary string | -  | An access token to access the SCM pointed to by `scm-url`. |

Here are some notes on the `type` of an project.
*   First, be advised that the artifact ID is made up of organization, system, domain, subdomain and artifact.
    In this order.
    GenDoc tries to interpret SCM paths this way.
    The interpretation can be controlled by the `type` option.
*   `single` - Most hobby projects fall into this category.
    They live in a single repository.
    Two things apply here:
    1. SCM pathes are interpreted as artifact ID so that it is populated "backwards" beginning with artifact.
        For example `https://gitlab.com/hzahnlei/gendoc` will be interpreted such that `hzahnlei` corresponds to organization and `gendoc` corresponds to artifact.
    2. Projects of that type must not have the `scm-url` and `sc-token` options set.
        They are assumed to be selfcontained projects.
        Hence, it does not make sense to consult further projects from the SCM.
*   `enterprise` - Most professional projects fall into this category.
    They are made up of various libraries, tools and (micro) services, all living in separate repos of the project's SCM.
    Two things apply here:
    1. SCM pathes are interpreted as artifact ID so that it is populated "foeward" beginning with system.
        For example `https://gitlab.com/hzahnlei/gendoc` will be interpreted such that `hzahnlei` corresponds to organization and `gendoc` corresponds to system.
    2. Projects of that type may have the `scm-url` and `sc-token` options set.
        GenDoc will then crawl additional `project.yaml` files from the projects in the given SCM.
        Hence, a more complete picture can be generated, such as complete list of all services etc.

## How to include generated diagrams and tables

```
include::system_context.adoc[]
````

## How to download Arc42 templates

Under the hood of `gendoc` we are using docToolchain to actually generate viewable documentation.
So we need the respective Arc42 templates.
These can be downloaded like so:

```bash
docker run --rm --interactive --tty --entrypoint /bin/bash \
           --volume ./download:/download \
           dmueller/doctoolchain:v2.0.5 \
           -c "doctoolchain . downloadTemplate -PinputPath=. -PmainConfigFile=docToolchain.groovy && exit"
```

However, `gendoc` has these templates already included.
Neverteless, it is good to know how to download updated versions - just in case.

## Integration testing

A few tests require internet connection and access to this project's GitLab repository.
These tests are flagged with the `int_test` Build Tag.
They are inactive unless the Build Tag is set, for example like so: `go test -tags int_test`.
For test without integration tests use `make test`.
If you also want the integration tests use `make test-all`.

Some tests need a valid access token to access GitLab's API.
This token has to be configured within your GitLab account.
It should only have read privileges.
It is passed to the tests via the `GIT_LAB_ACCESS_TOKEN` environment variable.

```bash
export GIT_LAB_ACCESS_TOKEN=<your access token here>
make test-all
```

## Open issues

### Different behaviour Linux/macOS

I see different behaviours when running my tool on macOS and Linux.
(See [Makefile](./Makefile) for details.)
The problem is, that on macOS my tool (and hence docToolchain) can write to the host folder.
On Linux on the other side this does not work.
There, I need to pass the Linux user's user and group IDs when starting the container.
But it seems that the original docToolchain container does not experience this problem.
This needs further investigation.

This worked on macOS but not on Linux:

```bash
docker run --rm --volume $(PROJECT_ROOT):$(DOCS_GUEST_DIR) \
           $(TARGET_DOCKER_IMAGE) \
           "generate_docs.sh && exit"
```

This worked on Linux but not on macOS:

```bash
docker run --rm --volume $(PROJECT_ROOT):$(DOCS_GUEST_DIR) \
           --user "$(shell id -u):$(shell id -g)" \
           $(TARGET_DOCKER_IMAGE) \
           "generate_docs.sh && exit"
```

This finally works on both, Linux and macOS:

```bash
docker run --rm --volume $(PROJECT_ROOT):$(DOCS_GUEST_DIR) \
           --user "$(shell id -u):$(shell id -g)" \
           --workdir $(DOCS_GUEST_DIR) \
           $(TARGET_DOCKER_IMAGE) \
           "generate_docs.sh && exit"
```
