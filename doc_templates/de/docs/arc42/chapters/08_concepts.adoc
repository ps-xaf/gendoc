:jbake-title: Querschnittliche Konzepte
:jbake-type: page_toc
:jbake-status: published
:jbake-menu: {{.PageDesign.ArcDocMenuEntry}}
:jbake-order: 8
:filename: /chapters/08_concepts.adoc
ifndef::imagesdir[:imagesdir: ../../images]

:toc:

ifndef::imagesdir[:imagesdir: ../images]

[[section-concepts]]
{{.ProjectDescription.DeveloperDocumentation.Arc42Chapter.Concepts}}
