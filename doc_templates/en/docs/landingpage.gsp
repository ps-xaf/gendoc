<div class="row flex-xl-nowrap">
    <main class="col-12 col-md-12 col-xl-12 pl-md-12" role="main">
        <div class="bg-light p-5 rounded">
            <h1>{{.ProjectDescription.DeveloperDocumentation.LandingPage.Title}} {{.BriefDescription.Version}}</h1>
            <p class="lead">
                {{.ProjectDescription.DeveloperDocumentation.LandingPage.Subtitle}}
            </p>
            <p>
                {{.ProjectDescription.DeveloperDocumentation.LandingPage.Description}}
            </p>
        </div>

{{if .ProjectDescription.DeveloperDocumentation.LandingPage.Features}}
        <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
{{range $feature := .ProjectDescription.DeveloperDocumentation.LandingPage.Features}}
            <div class="col">
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 fw-normal">{{$feature.Title}}</h4>
                    </div>
                    <div class="card-body">
                        {{$feature.Description}}
                    </div>
                </div>
            </div>
{{end}}
        </div>
{{end}}
    </main>
</div>
