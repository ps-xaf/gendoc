:imagesdir: ../images
:jbake-menu: -
// encabezado para arc42-template,
// incluye todos los textos de ayuda
//
// ====================================

// Opciones de configuración en español (ES) para asciidoc
include::chapters/config.adoc[]
= Plantilla image:arc42-logo.png[arc42]
:revnumber: 8.2 ES
:revdate: January 2023
:revremark: (based upon AsciiDoc version)
// toc-title La definición debe situarse abajo del título del documento sin líneas en blanco!
:toc-title: Contenido

//Estilos adicionales para las llamadas de ayuda de arc42
ifdef::backend-html5[]
++++
<style>
.arc42help {font-size:small; width: 14px; height: 16px; overflow: hidden; position: absolute; right: 0; padding: 2px 0 3px 2px;}
.arc42help::before {content: "?";}
.arc42help:hover {width:auto; height: auto; z-index: 100; padding: 10px;}
.arc42help:hover::before {content: "";}
@media print {
	.arc42help {display:none;}
}
</style>
++++
endif::backend-html5[]



include::chapters/about-arc42.adoc[]

// Línea horizontal
***



////
* This block is present in docToolChain but not in the original arc42-template

.Histórico de Cambios
[options="header",cols="1,2,6"]
|====
| Fecha
| Autor
| Comentario

include::../../build/docs/changelog.adoc[]

|====
////

// Numeramos las secciones a partir de aquí
:numbered:

<<<<
// 1. Introducción y Metas
include::chapters/01_introduction_and_goals.adoc[]

<<<<
// 2. Restricciones de la Arquitectura
include::chapters/02_architecture_constraints.adoc[]

<<<<
// 3. Alcance y Contexto del Sistema
include::chapters/03_system_scope_and_context.adoc[]

<<<<
// 4. Estrategia de solución
include::chapters/04_solution_strategy.adoc[]

<<<<
// 5. Vista de Bloques
include::chapters/05_building_block_view.adoc[]

<<<<
// 6. Vista de Ejecución
include::chapters/06_runtime_view.adoc[]

<<<<
// 7. Vista de Despliegue
include::chapters/07_deployment_view.adoc[]

<<<<
// 8. Conceptos Transversales (Cross-cutting)
include::chapters/08_concepts.adoc[]

<<<<
// 9. Decisiones de Diseño
include::chapters/09_architecture_decisions.adoc[]

<<<<
// 10. Requerimientos de Calidad
include::chapters/10_quality_requirements.adoc[]

<<<<
// 11. Riesgos y deuda técnica
include::chapters/11_technical_risks.adoc[]

<<<<
// 12. Glosario
include::chapters/12_glossary.adoc[]

////
* This block is present in docToolChain but not in the original arc42-template
// Apéndices:
// =====================================

<<<<
// Apéndice. Incidentes abiertos
include::chapters/appendix-open_issues.adoc[]

////
