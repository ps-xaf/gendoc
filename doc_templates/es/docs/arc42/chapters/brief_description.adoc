:jbake-title: Brief Description
:jbake-type: page_toc
:jbake-status: published
:jbake-menu: Brief Description
:jbake-order: 98
:filename: /chapters/brief_description.adoc
:linkattrs:
ifndef::imagesdir[:imagesdir: ../../images]

:toc:

ifndef::imagesdir[:imagesdir: ../images]

## {{.Title}} {{.ArtifactType}} {{.FriendlyName}} TODO localize

.Domain Membership
- System: {{.System}}
- Domain: {{.Domain}}
- Subdomain: {{.Subdomain}}
- Artifact: {{.Artifact}}
- Version: {{.Version}}

.Responsibilities
- Owner: {{.Owner}}
- Development Team: {{.Team}}

.References
- Source Repository: {{if .SCMWebURL}}{{.SCMWebURL}}[{{.SCMWebURL}}^, role="ext-link"]{{else}}n/a TODO localize{{end}}
