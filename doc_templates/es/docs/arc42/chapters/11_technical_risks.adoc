:jbake-title: Riesgos y deuda técnica
:jbake-type: page_toc
:jbake-status: published
:jbake-menu: {{.PageDesign.ArcDocMenuEntry}}
:jbake-order: 11
:filename: /chapters/11_technical_risks.adoc
ifndef::imagesdir[:imagesdir: ../../images]

:toc:

ifndef::imagesdir[:imagesdir: ../images]

[[section-technical-risks]]
{{.ProjectDescription.DeveloperDocumentation.Arc42Chapter.TechnicalRisk}}
