:jbake-title: About
:jbake-type: page_toc
:jbake-status: published
:jbake-menu: About
:jbake-order: 1
:filename: /chapters/about.adoc
ifndef::imagesdir[:imagesdir: ../..]

:toc:

== About this Documentation

This documentation has been built by GenDoc itself.

image::avatar256x256.png["Project Logo"]
