###############################################################################
#
# GenDoc - Meta documentation generator for docToolchain
#
# (c) 2023 Holger Zahnleiter
#
###############################################################################

.PHONY: local executable image docs debug-docs

.EXPORT_ALL_VARIABLES:
GO_BUILD_DIR := ../../bin
VERSION := $(shell git describe --tags)
GO_LD_FLAGS := -s -w -X main.version=$(VERSION)
PROJECT_ROOT := $(PWD)
# /project is default defined by docToolchain.
DOCS_GUEST_DIR := /project
DTC_DOCKER_IMAGE := doctoolchain/doctoolchain:v2.2.0
TARGET_DOCKER_IMAGE := gendoc:1.0.0

# Meant for local use, not for use in CI environments.  Builds most recent exe-
# cutable, Docker image and finaly this project's documentation.  Most conveni-
# ent for manual testing  during development.
local: executable image docs

# Build executables for different operating systems.  Use locally and in CI en-
# vironments.
executable:
	cd cmd/gendoc && \
	GO111MODULE=on CGO_ENABLED=0 GOARCH=amd64 GOOS=linux \
		go build \
			-ldflags "$(GO_LD_FLAGS)" \
			-o $(GO_BUILD_DIR)/gendoc-linux-amd64 && \
	GOOS=darwin \
		go build \
			-ldflags "$(GO_LD_FLAGS)" \
			-o $(GO_BUILD_DIR)/gendoc-darwin-amd64

# Run tests locally and in CI. This does EXCLUDE the integration tests.
test:
	go test -v ./...

# Run all test locally and in CI. This INCLUDES integration tests. Make sure an
# access token is provided in environment variable  GIT_LAB_ACCESS_TOKEN.  This
# access token needs to be a valid GitLab access token with API read access.
test-all:
	go test -tags int_test -v ./...

# Meant for local builds/tests. In CI we use Kaniko.
image: executable test
	docker build --build-arg DTC_DOCKER_IMAGE \
                 --no-cache \
                 --tag $(TARGET_DOCKER_IMAGE) .

# Generate this project's documentation.  This target can only be used locally,
# in CI we use our own image and no Docker in Docker.
# For collecting SCM projects call generate_docs.sh --lang=en --type=enterprise --scm-url=https://gitlab.com --scm-token=???? && exit
docs:
	docker run --rm --volume $(PROJECT_ROOT):$(DOCS_GUEST_DIR) \
	           --user "$(shell id -u):$(shell id -g)" \
	           --workdir $(DOCS_GUEST_DIR) \
	           $(TARGET_DOCKER_IMAGE) \
	           "generate_docs.sh --lang=en --type=single && exit"

# Run locally for the purpose of debugging. Have plain docToolchain process the
# generated documentation and analyse the results.
debug-docs:
	docker run --rm --interactive --tty --entrypoint /bin/bash \
	           --volume $(PROJECT_ROOT)/build_docs:$(DOCS_GUEST_DIR) \
			   --workdir $(DOCS_GUEST_DIR) \
	           $(DTC_DOCKER_IMAGE) \
	           -c "doctoolchain . generateSite -PinputPath=. -PmainConfigFile=docToolchain.groovy && exit"
