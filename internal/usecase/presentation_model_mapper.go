//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import (
	"fmt"
	"net/url"
	"sort"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

func PresentationModelFrom(
	language LanguageCode, prjDescr *domain.ProjectDescription, chapters *domain.DevelopersArc42Chapters, sysContext string, domainDiagram string, membershipStructure *domain.MembershipStructure) PresentationModel {
	return PresentationModel{
		ProjectDescription: presentableProjectDescriptionFrom(language, prjDescr, chapters),
		PageDesign:         presentablePageDesignFrom(language, prjDescr),
		SystemContext:      presentableSystemContextFrom(sysContext),
		DomainDiagram:      presentableDomainDiagramFrom(domainDiagram),
		DomainTable:        *presentableDomainTableFrom(membershipStructure),
		BriefDescription:   *presentableBriefDescription(language, prjDescr),
	}
}

func presentableProjectDescriptionFrom(language LanguageCode, prjDescr *domain.ProjectDescription, chapters *domain.DevelopersArc42Chapters) ProjectDescription {
	var model ProjectDescription
	model.ArtifactID = presentableArtifactID(&prjDescr.ArtifactID)
	model.ArtifactType = presentableArtifactType[language][prjDescr.ArtifactType]
	model.DefinedInterfaces = presentableDefinedInterfaces(&prjDescr.DefinedInterfaces)
	model.DeploymentVariants = presentableDeploymentVariants(&prjDescr.DeploymentVariants)
	model.Description = string(prjDescr.Description)
	model.FriendlyName = string(prjDescr.FriendlyName)
	model.Version = presentableSemanticVersion(&prjDescr.Version)
	model.DeveloperDocumentation.LandingPage = presentableLandingPage(&prjDescr.LandingPage)
	model.DeveloperDocumentation.Arc42Chapter = presentableDeveloperChapters(chapters)
	return model
}

func presentableArtifactID(artifactID *domain.FullyQualifiedArtifactName) string {
	var parts = make([]string, 0)
	if artifactID.System.IsPresent() {
		parts = append(parts, string(artifactID.System))
	}
	if artifactID.Domain.IsPresent() {
		parts = append(parts, string(artifactID.Domain))
		if artifactID.Subdomain.IsPresent() {
			parts = append(parts, string(artifactID.Subdomain))
		}
	}
	if artifactID.Artifact.IsPresent() {
		parts = append(parts, string(artifactID.Artifact))
	}
	return strings.Join(parts, "/")
}

var presentableArtifactType = map[LanguageCode]map[domain.ArtifactType]string{
	LangDE: {
		domain.ArtifactTypeApplication: "Anwendung",
		domain.ArtifactTypeDomain:      "Domäne",
		domain.ArtifactTypeLibrary:     "Bibliothek",
		domain.ArtifactTypeService:     "Dienst",
		domain.ArtifactTypeSubdomain:   "Subdomäne",
		domain.ArtifactTypeSystem:      "System",
		domain.ArtifactTypeTool:        "Werkzeug"},
	LangEN: {
		domain.ArtifactTypeApplication: "Application",
		domain.ArtifactTypeDomain:      "Domain",
		domain.ArtifactTypeLibrary:     "Library",
		domain.ArtifactTypeService:     "Service",
		domain.ArtifactTypeSubdomain:   "Subdomain",
		domain.ArtifactTypeSystem:      "System",
		domain.ArtifactTypeTool:        "Tool"},
	LangES: { //TODO localize, traducir del inglés al español
		domain.ArtifactTypeApplication: "Application",
		domain.ArtifactTypeDomain:      "Domain",
		domain.ArtifactTypeLibrary:     "Library",
		domain.ArtifactTypeService:     "Service",
		domain.ArtifactTypeSubdomain:   "Subdomain",
		domain.ArtifactTypeSystem:      "System",
		domain.ArtifactTypeTool:        "Tool"},
}

func presentableDefinedInterfaces(ifaces *[]domain.InterfaceDefinition) []InterfaceDefinition {
	var presentableIfaces = make([]InterfaceDefinition, len(*ifaces))
	for i, iface := range *ifaces {
		presentableIfaces[i] = presentableDefinedInterface(&iface)
	}
	return presentableIfaces
}

func presentableDefinedInterface(iface *domain.InterfaceDefinition) InterfaceDefinition {
	specURL := url.URL(iface.SpecificationURL)
	return InterfaceDefinition{
		LocalID:          string(iface.LocalID),
		FriendlyName:     string(iface.FriendlyName),
		Description:      string(iface.Description),
		SpecificationURL: specURL.String(),
		Application:      representableIfaceApplicationSpec(&iface.Application),
		Communication:    representableIfaceCommunicationSpec(&iface.Communication),
		Transport:        representableIfaceTransportSpec(&iface.Transport),
	}
}

func representableIfaceApplicationSpec(appSpec *domain.ApplicationSpec) ApplicationSpec {
	return ApplicationSpec{
		Format:   string(appSpec.Format),
		Security: string(appSpec.Security),
	}
}

func representableIfaceCommunicationSpec(commSpec *domain.CommunicationSpec) CommunicationSpec {
	return CommunicationSpec{
		Pattern:  string(commSpec.Pattern),
		Style:    string(commSpec.Style),
		Format:   string(commSpec.Format),
		Encoding: string(commSpec.Encoding),
	}
}

func representableIfaceTransportSpec(transportSpec *domain.TransportSpec) TransportSpec {
	return TransportSpec{
		Protocol: string(transportSpec.Protocol),
		Security: string(transportSpec.Security),
	}
}

func presentableDeploymentVariants(deployments *[]domain.DeploymentVariant) []DeploymentVariant {
	var presentableDeployments = make([]DeploymentVariant, len(*deployments))
	for i, deployment := range *deployments {
		presentableDeployments[i] = presentableDeploymentVariant(&deployment)
	}
	return presentableDeployments
}

func presentableDeploymentVariant(deployment *domain.DeploymentVariant) DeploymentVariant {
	depURL := url.URL(deployment.DeploymentDescriptorURL)
	return DeploymentVariant{
		LocalID:                 string(deployment.LocalID),
		FriendlyName:            string(deployment.FriendlyName),
		Purpose:                 string(deployment.Purpose),
		DeploymentDescriptorURL: depURL.String(),
		ExposedInterfaces:       presentableExposedInterfaces(&deployment.ExposedInterfaces),
		ConsumedInterfaces:      presentableConsumedInterfaces(&deployment.ConsumedInterfaces),
	}
}

func presentableExposedInterfaces(ifaces *[]domain.ExposedInterface) []ExposedInterface {
	var presentableIfaces = make([]ExposedInterface, len(*ifaces))
	for i, iface := range *ifaces {
		presentableIfaces[i] = presentableExposedInterface(&iface)
	}
	return presentableIfaces
}

func presentableExposedInterface(iface *domain.ExposedInterface) ExposedInterface {
	return ExposedInterface{
		LocalRef: string(iface.LocalRef),
	}
}

func presentableConsumedInterfaces(ifaces *[]domain.ConsumedInterface) []ConsumedInterface {
	var presentableIfaces = make([]ConsumedInterface, len(*ifaces))
	for i, iface := range *ifaces {
		presentableIfaces[i] = presentableConsumedInterface(&iface)
	}
	return presentableIfaces
}

func presentableConsumedInterface(iface domain.ConsumedInterface) ConsumedInterface {
	switch consumedIface := iface.(type) {
	case *domain.ConsumedInternalInterface:
		return ConsumedInterface{
			InternalInterface: &ConsumedInternalInterface{
				ArtifactRef:   presentableArtifactID(&consumedIface.ArtifactRef),
				DeploymentRef: string(consumedIface.DeploymentRef),
				InterfaceRef:  string(consumedIface.InterfaceRef),
			},
			ExternalInterface: nil,
		}
	case *domain.ConsumedExternalInterface:
		return ConsumedInterface{
			InternalInterface: nil,
			ExternalInterface: &ConsumedExternalInterface{
				OrganizationName: string(consumedIface.Organization),
				SystemName:       string(consumedIface.System),
				InterfaceName:    string(consumedIface.Interface),
			},
		}
	default:
		return ConsumedInterface{}
	}
}

func presentableSemanticVersion(semVer *domain.SemanticVersion) string {
	switch semVer.Addendum.Type {
	case domain.AddendumRelease:
		return fmt.Sprintf("%d.%d.%d", semVer.Major, semVer.Minor, semVer.Patch)
	case domain.AddendumReleaseCandidate:
		return fmt.Sprintf("%d.%d.%d-RC%d", semVer.Major, semVer.Minor, semVer.Patch, semVer.Addendum.RCVersion)
	case domain.AddendumSnapshot:
		return fmt.Sprintf("%d.%d.%d-SNAPSHOT", semVer.Major, semVer.Minor, semVer.Patch)
	default:
		return fmt.Sprintf("%d.%d.%d", semVer.Major, semVer.Minor, semVer.Patch)
	}
}

func presentableBriefDescription(language LanguageCode, prjDescr *domain.ProjectDescription) *BriefDescription {
	artifactType := localizedPageNamePrefix[language][prjDescr.ArtifactType]
	friendlyName := string(prjDescr.ArtifactID.Artifact)
	if prjDescr.FriendlyName.IsPresent() {
		friendlyName = string(prjDescr.FriendlyName)
	}
	title := localizedTitle[language]
	system := "-"
	if prjDescr.ArtifactID.System.IsPresent() {
		system = string(prjDescr.ArtifactID.System)
	}
	domain := "-"
	if prjDescr.ArtifactID.Domain.IsPresent() {
		domain = string(prjDescr.ArtifactID.Domain)
	}
	subdomain := "-"
	if prjDescr.ArtifactID.Subdomain.IsPresent() {
		domain = string(prjDescr.ArtifactID.Subdomain)
	}
	artifact := string(prjDescr.ArtifactID.Artifact)
	version := presentableSemanticVersion(&prjDescr.Version)
	owner := "-"
	if prjDescr.Organization.IsPresent() {
		owner = string(prjDescr.Organization)
	}
	team := lokalizedUnknown[language]
	if prjDescr.Team.IsPresent() {
		team = string(prjDescr.Team)
	}
	scmURL := url.URL(prjDescr.SCMURL)
	scmURLStr := scmURL.String()
	scmWebURL := url.URL(prjDescr.SCMWebURL)
	scmWebURLStr := scmWebURL.String()
	return &BriefDescription{
		ArtifactType: artifactType,
		FriendlyName: friendlyName,
		Title:        title,
		System:       system,
		Domain:       domain,
		Subdomain:    subdomain,
		Artifact:     artifact,
		Version:      version,
		Owner:        owner,
		Team:         team,
		SCMURL:       scmURLStr,
		SCMWebURL:    scmWebURLStr,
	}
}

var localizedTitle = map[LanguageCode]string{
	LangDE: "Kurzbeschreibung",
	LangEN: "Brief Description",
	LangES: "Brief Description", //TODO localize
}

var lokalizedUnknown = map[LanguageCode]string{
	LangDE: "unbekannt",
	LangEN: "unknown",
	LangES: "unknown", //TODO localize
}

func presentableLandingPage(page *domain.LandingPage) LandingPage {
	return LandingPage{
		Title:       string(page.Title),
		Subtitle:    string(page.Subtitle),
		Description: string(page.Description),
		Features:    presentableFeatures(&page.Features),
	}
}

func presentableFeatures(features *[]domain.LandingPageFeature) []LandingPageFeature {
	var mapped = make([]LandingPageFeature, len(*features))
	for i, feature := range *features {
		mapped[i] = presentableFeature(&feature)
	}
	return mapped
}

func presentableFeature(feature *domain.LandingPageFeature) LandingPageFeature {
	return LandingPageFeature{
		Title:       string(feature.Title),
		Description: string(feature.Description),
	}
}

func presentableDeveloperChapters(chapters *domain.DevelopersArc42Chapters) DevelopersArc42Chapters {
	return DevelopersArc42Chapters{
		IntroductionAndGoals:    string(chapters.IntroductionAndGoals),
		ArchitectureConstraints: string(chapters.ArchitectureConstraints),
		SystemScopeAndContext:   string(chapters.SystemScopeAndContext),
		SolutionStrategy:        string(chapters.SolutionStrategy),
		BuildingBlockView:       string(chapters.BuildingBlockView),
		RuntimeView:             string(chapters.RuntimeView),
		DeploymentView:          string(chapters.DeploymentView),
		Concepts:                string(chapters.Concepts),
		ArchitectureDecisions:   string(chapters.ArchitectureDecisions),
		QualityRequirements:     string(chapters.QualityRequirements),
		TechnicalRisks:          string(chapters.TechnicalRisks),
		Glossary:                string(chapters.Glossary),
	}
}

func presentablePageDesignFrom(language LanguageCode, prjDescr *domain.ProjectDescription) PageDesign {
	return PageDesign{
		ArcDocMenuEntry: localizedArcDocuMenuEntry[language],
		PageName:        presentablePageName(language, prjDescr),
	}
}

var localizedArcDocuMenuEntry = map[LanguageCode]string{
	LangDE: "Architekturdokumentation",
	LangEN: "Architecture Documentation",
	LangES: "Documentación de la Arquitectura",
}

func presentablePageName(language LanguageCode, prjDescr *domain.ProjectDescription) string {
	pageName := strings.TrimSpace(string(prjDescr.FriendlyName))
	if pageName == "" {
		if prjDescr.ProjectType == domain.ProjectTypeEnterprise {
			switch prjDescr.ArtifactType {
			case domain.ArtifactTypeApplication:
				pageName = string(prjDescr.ArtifactID.Artifact)
			case domain.ArtifactTypeDomain:
				pageName = string(prjDescr.ArtifactID.Domain)
			case domain.ArtifactTypeLibrary:
				pageName = string(prjDescr.ArtifactID.Artifact)
			case domain.ArtifactTypeService:
				pageName = string(prjDescr.ArtifactID.Artifact)
			case domain.ArtifactTypeSubdomain:
				pageName = string(prjDescr.ArtifactID.Subdomain)
			case domain.ArtifactTypeSystem:
				pageName = string(prjDescr.ArtifactID.System)
			case domain.ArtifactTypeTool:
				pageName = string(prjDescr.ArtifactID.Artifact)
			}
		} else {
			pageName = string(prjDescr.ArtifactID.Artifact)
		}
	}
	if pageName == "" {
		pageName = presentableArtifactID(&prjDescr.ArtifactID)
	}
	return localizedPageNamePrefix[language][prjDescr.ArtifactType] + " " + pageName
}

var localizedPageNamePrefix = map[LanguageCode](map[domain.ArtifactType]string){
	LangDE: {
		domain.ArtifactTypeApplication: "Anwendung",
		domain.ArtifactTypeDomain:      "Domäne",
		domain.ArtifactTypeLibrary:     "Bibliothek",
		domain.ArtifactTypeService:     "Dienst",
		domain.ArtifactTypeSubdomain:   "Subdomäne",
		domain.ArtifactTypeSystem:      "System",
		domain.ArtifactTypeTool:        "Werkzeug",
	},
	LangEN: {
		domain.ArtifactTypeApplication: "Application",
		domain.ArtifactTypeDomain:      "Domain",
		domain.ArtifactTypeLibrary:     "Library",
		domain.ArtifactTypeService:     "Service",
		domain.ArtifactTypeSubdomain:   "Subdomain",
		domain.ArtifactTypeSystem:      "System",
		domain.ArtifactTypeTool:        "Tool",
	},
	LangES: {
		domain.ArtifactTypeApplication: "TODO Application", //TODO localize
		domain.ArtifactTypeDomain:      "TODO Domain",
		domain.ArtifactTypeLibrary:     "TODO Library",
		domain.ArtifactTypeService:     "TODO Service",
		domain.ArtifactTypeSubdomain:   "TODO Subdomain",
		domain.ArtifactTypeSystem:      "TODO System",
		domain.ArtifactTypeTool:        "TODO Tool",
	},
}

func presentableSystemContextFrom(diagram string) Diagram {
	return Diagram{
		UniqueName: "systemContextGenerated",
		Diagram:    diagram,
	}
}

func presentableDomainDiagramFrom(diagram string) Diagram {
	return Diagram{
		UniqueName: "domainDiagramGenerated",
		Diagram:    diagram,
	}
}

func presentableDomainTableFrom(membershipStructure *domain.MembershipStructure) *[]DomainTableRow {
	var table = []DomainTableRow{}
	orgaNames := make([]string, 0, len(*membershipStructure))
	for orgaName := range *membershipStructure {
		orgaNames = append(orgaNames, string(orgaName))
	}
	sort.Strings(orgaNames)
	for _, orgaName := range orgaNames {
		appendRowsForOrganization(&table, orgaName, (*membershipStructure)[domain.OrganizationName(orgaName)])
	}
	return &table
}

func appendRowsForOrganization(table *[]DomainTableRow, orgaName string, membershipStructure domain.OranizationMembershipStructure) {
	systemNames := make([]string, 0, len(membershipStructure))
	for sysName := range membershipStructure {
		systemNames = append(systemNames, string(sysName))
	}
	sort.Strings(systemNames)
	for _, sysName := range systemNames {
		appendRowsForSystem(table, orgaName, sysName, membershipStructure[domain.SystemName(sysName)])
	}
}

func appendRowsForSystem(table *[]DomainTableRow, orgaName string, sysName string, membershipStructure domain.SystemMembershipStructure) {
	domainNames := make([]string, 0, len(membershipStructure))
	for domName := range membershipStructure {
		domainNames = append(domainNames, string(domName))
	}
	sort.Strings(domainNames)
	for _, domName := range domainNames {
		appendRowsForDomain(table, orgaName, sysName, domName, membershipStructure[domain.DomainName(domName)])
	}
}

func appendRowsForDomain(table *[]DomainTableRow, orgaName string, sysName string, domName string, membershipStructure domain.DomainMembershipStructure) {
	subdomainNames := make([]string, 0, len(membershipStructure))
	for subdomName := range membershipStructure {
		subdomainNames = append(subdomainNames, string(subdomName))
	}
	sort.Strings(subdomainNames)
	for _, subdomName := range subdomainNames {
		appendRowsForSubdomain(table, orgaName, sysName, domName, subdomName, membershipStructure[domain.SubdomainName(subdomName)])
	}
}

func appendRowsForSubdomain(table *[]DomainTableRow, orgaName string, sysName string, domName string, subdomName string, membershipStructure domain.SubdomainMembershipStructure) {
	artiNames := make([]string, 0, len(membershipStructure))
	for artiName := range membershipStructure {
		artiNames = append(artiNames, string(artiName))
	}
	sort.Strings(artiNames)
	for _, artiName := range artiNames {
		prjDescr := membershipStructure[domain.ArtifactName(artiName)]
		row := DomainTableRow{
			Organization: orgaName,
			System:       sysName,
			Domain:       domName,
			Subdomain:    subdomName,
			Artifact:     artiName,
			Team:         string(prjDescr.Team),
			SCMURL:       "TODO GitLab URL",
		}
		*table = append(*table, row)
	}
}
