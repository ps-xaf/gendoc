//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import "zahnleiter.org/gendoc/internal/domain"

type DevelopersArc42ChaptersSource interface {
	ReadAll() (domain.DevelopersArc42Chapters, error)
}
