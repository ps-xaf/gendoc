//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

type DocumentName int

const (
	// Content aka architecture documentation
	DocumentName01Introduction   DocumentName = 1
	DocumentName02Constraints    DocumentName = 2
	DocumentName03Context        DocumentName = 3
	DocumentName04Strategy       DocumentName = 4
	DocumentName05BuildingBlocks DocumentName = 5
	DocumentName06Runtime        DocumentName = 6
	DocumentName07Deployment     DocumentName = 7
	DocumentName08Concepts       DocumentName = 8
	DocumentName09Decisions      DocumentName = 9
	DocumentName10Quality        DocumentName = 10
	DocumentName11Risks          DocumentName = 11
	DocumentName12Glossary       DocumentName = 12
	// Configs etc.
	DocumentNameDocToolchainConf DocumentName = 100
	DocumentNameLandingPage      DocumentName = 101
	// Diagrams
	DocumentNameSystemContext        DocumentName = 200
	DocumentNameDefinedInterfaces    DocumentName = 201
	DocumentNameDeploymentVariants   DocumentName = 202
	DocumentNameDomainDiagram        DocumentName = 203
	DocumentNameDomainStructureTable DocumentName = 204
	DocumentNameBriefDescription     DocumentName = 205
)
