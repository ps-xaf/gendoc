//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v2"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
	"zahnleiter.org/gendoc/internal/usecase"
)

func TestRenderingOfArchitectureGraphE2E(t *testing.T) {
	// GIVEN
	prjYAML := `
ID: mydomain/mytool
friendly name: My Tool
type: tool

defines interfaces:
  - local ID: myapi
    friendly name: My API

deployment variants:
  - local ID: default
    exposes interfaces:
      - local ref: myapi
    consumes interfaces:
      - artifact ref: otherdomain/othertool
        deployment ref: default2
        interface ref: otherapi
`
	prjDescr, err := singleProjectDescriptionFrom(prjYAML)
	require.NoError(t, err)
	graph := domain.SystemContextGraphFrom(prjDescr)
	renderer := usecase.NewPlantUMLRenderer()
	// WHEN
	plantUML := renderer.Render(graph)
	// THEN
	require.Equal(
		t, `frame "My Tool" <<system context>> as CTX_0 {
    rectangle "default" <<deployment>> as DEP_1
    rectangle "default2" <<deployment>> as DEP_2
    rectangle "mydomain" <<domain>> as DOM_3 {
        rectangle "mytool" <<tool>> as TOOL_4 {
            component "myapi" <<interface definition>> as IFACEDEF_5
        }
    }
    rectangle "otherdomain" <<domain>> as DOM_6 {
        rectangle "othertool" <<app/service/...>> as PROV_7 {
            component "otherapi" <<interface definition>> as IFACEDEF_8
        }
    }
    component "myapi" <<interface>> as IFACE_9
    component "otherapi" <<interface>> as IFACE_10
}

TOOL_4 -down-> DEP_1:"deployed as"
PROV_7 -down-> DEP_2:"deployed as"

DEP_1 -down-> IFACE_9:"exposes"
DEP_2 -down-> IFACE_10:"exposes"

DEP_1 -down-> IFACE_10:"consumes"
`,
		plantUML)
}

func TestRenderingOfAnotherArchitectureGraphE2E(t *testing.T) {
	// GIVEN
	prjYAML := `
ID: gendoc
friendly name: GenDoc
type: tool

defines interfaces:
  - local ID: myapi
    friendly name: My API
    description: access my application

deployment variants:
  - local ID: default
    exposes interfaces:
      - interface ref: myapi
    consumes interfaces:
      - artifact ref: other_system
        deployment ref: default
        interface ref: otherapi
`
	prjDescr, err := singleProjectDescriptionFrom(prjYAML)
	require.NoError(t, err)
	graph := domain.SystemContextGraphFrom(prjDescr)
	renderer := usecase.NewPlantUMLRenderer()
	// WHEN
	plantUML := renderer.Render(graph)
	// THEN
	require.Equal(
		t, `frame "GenDoc" <<system context>> as CTX_0 {
    rectangle "other_system" <<app/service/...>> as PROV_1 {
        component "otherapi" <<interface definition>> as IFACEDEF_2
    }
    rectangle "default" <<deployment>> as DEP_3
    rectangle "default" <<deployment>> as DEP_4
    component "otherapi" <<interface>> as IFACE_5
    rectangle "gendoc" <<tool>> as TOOL_6 {
        component "myapi" <<interface definition>> as IFACEDEF_7
    }
}

PROV_1 -down-> DEP_4:"deployed as"
TOOL_6 -down-> DEP_3:"deployed as"

DEP_4 -down-> IFACE_5:"exposes"

DEP_3 -down-> IFACE_5:"consumes"
`,
		plantUML)
}

//---- Helper -----------------------------------------------------------------

func singleProjectDescriptionFrom(prjYAMLString string) (*domain.ProjectDescription, error) {
	var projectYAML infrastructure.ProjectYAML
	if err := yaml.Unmarshal([]byte(prjYAMLString), &projectYAML); err != nil {
		return nil, err
	}
	return infrastructure.DomainRepresentationFromProjectYAML(&projectYAML, domain.ProjectTypeSingle)
}
