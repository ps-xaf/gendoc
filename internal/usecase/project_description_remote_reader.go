//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import (
	"net/url"

	"zahnleiter.org/gendoc/internal/domain"
)

type ProjectDescriptionRemoteReader interface {
	Read(*url.URL) (*domain.ProjectDescription, error)
}
