//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

type Arc42TemplateName int
type Arc42Template string
type Arc42Templates map[Arc42TemplateName]Arc42Template

const (
	// ---- Content aka architecture documentation ----
	// Developer's sections are copied into these templates.
	A42Template01Introduction   Arc42TemplateName = 1
	A42Template02Constraints    Arc42TemplateName = 2
	A42Template03Context        Arc42TemplateName = 3
	A42Template04Strategy       Arc42TemplateName = 4
	A42Template05BuildingBlocks Arc42TemplateName = 5
	A42Template06Runtime        Arc42TemplateName = 6
	A42Template07Deployment     Arc42TemplateName = 7
	A42Template08Concepts       Arc42TemplateName = 8
	A42Template09Decisions      Arc42TemplateName = 9
	A42Template10Quality        Arc42TemplateName = 10
	A42Template11Risks          Arc42TemplateName = 11
	A42Template12Glossary       Arc42TemplateName = 12
	// ---- Configs etc. ----
	A42TemplateDocToolchainConf Arc42TemplateName = 100
	// Populated from landing page section in project description.
	A42TemplateLandingPage Arc42TemplateName = 101
	// ---- Diagrams and tables ----
	// These templates are populated from project description.  They do not re-
	// present complete  AsciiDoc files.  Rather, they are generated  pieces of
	// AsciiDoc that can be included by developers into their documentation.
	A42TemplateSystemContext        Arc42TemplateName = 200
	A42TemplateDefinedInterfaces    Arc42TemplateName = 201
	A42TemplateDeploymentVariants   Arc42TemplateName = 202
	A42TemplateDomainDiagram        Arc42TemplateName = 203
	A42TemplateDomainStructureTable Arc42TemplateName = 204
	A42TemplateBriefDescription     Arc42TemplateName = 205
)
