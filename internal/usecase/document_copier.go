//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import "regexp"

type DocumentCopier interface {
	// Fails if file does not exist and all other errors.
	MustCopyAll() error
	// Fails if file does not exist and all other errors.
	MustCopyAllBut(*regexp.Regexp) error
	// Does not fail if file does not exist, but fails in case of all other er-
	// rors.
	CopyAll() error
	// Does not fail if file does not exist, but fails in case of all other er-
	// rors.
	CopyAllBut(*regexp.Regexp) error
}
