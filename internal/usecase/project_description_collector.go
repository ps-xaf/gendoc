//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import (
	"fmt"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

type ProjectDescriptionsCollector interface {
	CollectAll() (*[]*domain.ProjectDescription, error)
}

//---- Excludes big picture ---------------------------------------------------

type NoProjectDescriptionsCollector struct{}

var _ ProjectDescriptionsCollector = (*NoProjectDescriptionsCollector)(nil)

func NewNoProjectDescriptionsCollector() *NoProjectDescriptionsCollector {
	return &NoProjectDescriptionsCollector{}
}

func (b *NoProjectDescriptionsCollector) CollectAll() (*[]*domain.ProjectDescription, error) {
	return &[]*domain.ProjectDescription{}, nil
}

//---- Includes big picture ---------------------------------------------------

type SCMProjectDescriptionsCollector struct {
	scmCrawler     SCMCrawler
	prjDescrReader ProjectDescriptionRemoteReader
}

var _ ProjectDescriptionsCollector = (*SCMProjectDescriptionsCollector)(nil)

func NewSCMProjectDescriptionsCollector(scmCrawler SCMCrawler, prjDescrReader ProjectDescriptionRemoteReader) *SCMProjectDescriptionsCollector {
	return &SCMProjectDescriptionsCollector{
		scmCrawler:     scmCrawler,
		prjDescrReader: prjDescrReader,
	}
}

func (b *SCMProjectDescriptionsCollector) CollectAll() (*[]*domain.ProjectDescription, error) {
	projectsInfos, err := b.scmCrawler.InfosOfAllProjects()
	if err != nil {
		return &[]*domain.ProjectDescription{}, fmt.Errorf("failed to gather basic information on projects: %w", err)
	}
	var projectDescriptions = make([]*domain.ProjectDescription, 0)
	for _, projectInfos := range *projectsInfos {
		prjDescr, err := b.prjDescrReader.Read(&projectInfos.SCMWebURL)
		if err != nil {
			if IsIgnorableRepo(err) {
				continue // Simpy ignore Git repos without project.yaml (404) or private repos (403)
			}
			return &[]*domain.ProjectDescription{}, fmt.Errorf("failed to gather project description: %w", err)
		}
		// GitLab tags,  these are later merged with topics  from project.yaml.
		// Merging does discard duplicates. So, each topic no matter if comming
		// from GitLab tags or project.yaml will be in the list only once.
		AddAllTopics(prjDescr, &projectInfos.Topics)
		projectDescriptions = append(projectDescriptions, prjDescr)
		prjDescr.SCMWebURL = domain.SCMWebURL(projectInfos.SCMWebURL)
	}
	return &projectDescriptions, nil
}

func AddAllTopics(prjDescr *domain.ProjectDescription, topics *[]string) {
	prjDescr.Topics = make(domain.Topics)
	for _, topic := range *topics {
		normalizedTopic := domain.NormalizedTopic(&topic)
		prjDescr.Topics.Unite(&normalizedTopic)
	}
}

func IsIgnorableRepo(err error) bool {
	return HasNoProjectYAML(err) || IsPrivateRepo(err)
}

func HasNoProjectYAML(err error) bool {
	return strings.Contains(err.Error(), "failed to HTTP GET project description: 404")
}

func IsPrivateRepo(err error) bool {
	return strings.Contains(err.Error(), "failed to HTTP GET project description: 403")
}
