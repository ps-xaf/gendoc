//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import "zahnleiter.org/gendoc/internal/domain"

type ProjectDescriptionSource interface {
	Read() (*domain.ProjectDescription, error)
}
