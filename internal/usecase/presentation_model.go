//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

type PresentationModel struct {
	ProjectDescription ProjectDescription
	PageDesign         PageDesign
	SystemContext      Diagram
	DomainDiagram      Diagram
	DomainTable        []DomainTableRow
	BriefDescription   BriefDescription
}

// ---- Project description (from formal sources) -----------------------------

type ProjectDescription struct {
	ArtifactID             string
	FriendlyName           string
	Description            string
	Version                string
	ArtifactType           string
	DefinedInterfaces      []InterfaceDefinition
	DeploymentVariants     []DeploymentVariant
	DeveloperDocumentation DeveloperDocumentation
}

type InterfaceDefinition struct {
	LocalID          string
	FriendlyName     string
	Description      string
	SpecificationURL string
	Application      ApplicationSpec
	Communication    CommunicationSpec
	Transport        TransportSpec
}

type ApplicationSpec struct {
	Format   string
	Security string
}

type CommunicationSpec struct {
	Pattern  string
	Style    string
	Format   string
	Encoding string
}

type TransportSpec struct {
	Protocol string
	Security string
}

type DeploymentVariant struct {
	LocalID                 string
	FriendlyName            string
	Purpose                 string
	DeploymentDescriptorURL string
	ExposedInterfaces       []ExposedInterface
	ConsumedInterfaces      []ConsumedInterface
}

type ExposedInterface struct {
	LocalRef string
}

type ConsumedInterface struct {
	InternalInterface *ConsumedInternalInterface
	ExternalInterface *ConsumedExternalInterface
}

type ConsumedInternalInterface struct {
	ArtifactRef   string
	DeploymentRef string
	InterfaceRef  string
}

type ConsumedExternalInterface struct {
	OrganizationName string
	SystemName       string
	InterfaceName    string
}

// ---- Arch42 chapters, handwitten by developers -----------------------------

type DeveloperDocumentation struct {
	LandingPage  LandingPage
	Arc42Chapter DevelopersArc42Chapters
}

type LandingPage struct {
	Title       string
	Subtitle    string
	Description string
	Features    []LandingPageFeature
}

type LandingPageFeature struct {
	Title       string
	Description string
}

type DevelopersArc42Chapters struct {
	IntroductionAndGoals    string
	ArchitectureConstraints string
	SystemScopeAndContext   string
	SolutionStrategy        string
	BuildingBlockView       string
	RuntimeView             string
	DeploymentView          string
	Concepts                string
	ArchitectureDecisions   string
	QualityRequirements     string
	TechnicalRisks          string
	Glossary                string
}

// ---- Additional information needed for actual documentation generator -------

type PageDesign struct {
	PageName        string
	ArcDocMenuEntry string
}

type Diagram struct {
	UniqueName string
	Diagram    string
}

type DomainTableRow struct {
	Organization string
	System       string
	Domain       string
	Subdomain    string
	Artifact     string
	Team         string
	SCMURL       string // SCM repository, e.g. GitLab
	//TODO link to gitlab page,
}

type BriefDescription struct {
	ArtifactType string
	FriendlyName string
	Title        string
	System       string
	Domain       string
	Subdomain    string
	Artifact     string
	Version      string
	Owner        string
	Team         string
	SCMURL       string
	SCMWebURL    string
}
