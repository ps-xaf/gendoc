//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase_test

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

// Processes  only one very  simple template with only  one value  from project
// description. The generator tries all 14 templates it internally knows about.
// But only one template gets supplied in this test,  so only this one will ac-
// tually be successful.
func TestMostSimpleTemplateCanBeProcessed(t *testing.T) {
	// GIVEN
	domainModel := domain.ProjectDescription{
		ArtifactID: domain.FullyQualifiedArtifactName{
			System: "gendoc",
		},
		ArtifactType: domain.ArtifactTypeSystem,
		Version: domain.SemanticVersion{
			Major: 1,
			Minor: 2,
			Patch: 3,
			Addendum: domain.VersionAddendum{
				Type:      domain.AddendumReleaseCandidate,
				RCVersion: 4,
			},
		},
		LandingPage: domain.LandingPage{
			Title:       "LP Title",
			Description: "LP Description",
			Subtitle:    "LP Subtitle",
		},
	}
	prjYAMLSourceMock := ProjectYAMLSourceMock{}
	prjYAMLSourceMock.SetProjectDescription(domainModel)
	arc42Templates := usecase.Arc42Templates{
		usecase.A42Template01Introduction: "{{.TheExpectedTemplate}}",
	}
	templateSourceMock := TemplateSourceMock{}
	templateSourceMock.SetTemplates(arc42Templates)
	docsDest := NewDocumentationDestinationMock()
	templateEngineMock := NewTemplateEngineMock()
	tmplEngineMock := func(tmpl usecase.Arc42Template, prjDescr usecase.EffectivePresentationModel, dest usecase.DocumentationDestination) error {
		return templateEngineMock.PretendToBeATemplateEngine(tmpl, prjDescr, dest)
	}
	generator := usecase.NewUseCaseGenerateDocumentation().
		WithAdditionalDocumentsCopier(&DocumentsCopierMock{}).
		WithDeveloperChaptersFrom(&ChaptersSourceMock{}).
		WithDocumentationStoredAt(docsDest).
		WithImageCopier(&DocumentsCopierMock{}).
		WithSystemContextDiagramRenderer(&SystemContextRendererMock{}).
		WithTemplateEngine(tmplEngineMock).
		WithLocalSCMInfosFrom(&SCMInfoSourceMock{}).
		WithProjectYAMLFrom(&prjYAMLSourceMock).
		WithTemplatesFrom(&templateSourceMock).
		WithVersionPropertiesFrom(&VersionPropertiesSourceMock{}).
		WithProjectDescriptionsCollector(usecase.NewNoProjectDescriptionsCollector()) // This does nothing
	// WHEN
	err := generator.Generate(usecase.LangEN)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, 18, len(docsDest.output))
	// Our original template,  untouched,  as our mock template engine does not
	// replace anything but passes through instead.
	assert.Equal(t, "{{.TheExpectedTemplate}}", string(docsDest.output[usecase.DocumentName01Introduction]))
	assert.Equal(t, 18, templateEngineMock.invokationCount)
}

//TODO more test cases

//---- Mocks and helpers ------------------------------------------------------

type DocumentationDestinationMock struct {
	output map[usecase.DocumentName][]byte
	curr   usecase.DocumentName
}

var _ usecase.DocumentationDestination = (*DocumentationDestinationMock)(nil)

func NewDocumentationDestinationMock() *DocumentationDestinationMock {
	return &DocumentationDestinationMock{
		output: make(map[usecase.DocumentName][]byte),
	}
}

func (d *DocumentationDestinationMock) Open(docName usecase.DocumentName) error {
	d.curr = docName
	return nil
}

func (d *DocumentationDestinationMock) Write(buff []byte) (int, error) {
	d.output[d.curr] = append(d.output[d.curr], buff...)
	return len(buff), nil
}

func (d *DocumentationDestinationMock) Close() error {
	return nil
}

type TemplateEngineMock struct {
	invokationCount int
}

func NewTemplateEngineMock() *TemplateEngineMock {
	return &TemplateEngineMock{invokationCount: 0}
}

func (t *TemplateEngineMock) PretendToBeATemplateEngine(template usecase.Arc42Template, prjDescr usecase.EffectivePresentationModel, dest usecase.DocumentationDestination) error {
	t.invokationCount += 1
	dest.Write([]byte(template))
	return nil
}

type SystemContextRendererMock struct{}

var _ usecase.UMLGraphRenderer = (*SystemContextRendererMock)(nil)

func (r *SystemContextRendererMock) Render(*domain.Node) string {
	return "IMPLEMENT WHEN REQUIRED"
}

type DocumentsCopierMock struct{}

var _ usecase.DocumentCopier = (*DocumentsCopierMock)(nil)

func (*DocumentsCopierMock) MustCopyAll() error {
	// Implement when required
	return nil
}

func (*DocumentsCopierMock) MustCopyAllBut(*regexp.Regexp) error {
	// Implement when required
	return nil
}

func (*DocumentsCopierMock) CopyAll() error {
	// Implement when required
	return nil
}

func (*DocumentsCopierMock) CopyAllBut(*regexp.Regexp) error {
	// Implement when required
	return nil
}

type ChaptersSourceMock struct{}

var _ usecase.DevelopersArc42ChaptersSource = (*ChaptersSourceMock)(nil)

func (s *ChaptersSourceMock) ReadAll() (domain.DevelopersArc42Chapters, error) {
	// Implement when required
	return domain.DevelopersArc42Chapters{}, nil
}

type SCMInfoSourceMock struct{}

var _ usecase.ProjectDescriptionSource = (*SCMInfoSourceMock)(nil)

func (s *SCMInfoSourceMock) Read() (*domain.ProjectDescription, error) {
	// Implement when required
	return &domain.ProjectDescription{}, nil
}

type VersionPropertiesSourceMock struct{}

var _ usecase.ProjectDescriptionSource = (*VersionPropertiesSourceMock)(nil)

func (s *VersionPropertiesSourceMock) Read() (*domain.ProjectDescription, error) {
	// Implement when required
	return &domain.ProjectDescription{}, nil
}

type ProjectYAMLSourceMock struct {
	prjDescr domain.ProjectDescription
}

var _ usecase.ProjectDescriptionSource = (*ProjectYAMLSourceMock)(nil)

func (s *ProjectYAMLSourceMock) Read() (*domain.ProjectDescription, error) {
	return &s.prjDescr, nil
}

func (s *ProjectYAMLSourceMock) SetProjectDescription(prjDescr domain.ProjectDescription) {
	s.prjDescr = prjDescr
}

type TemplateSourceMock struct {
	templates usecase.Arc42Templates
}

var _ usecase.Arc42TemplateSource = (*TemplateSourceMock)(nil)

func (s *TemplateSourceMock) Read(usecase.LanguageCode) (usecase.Arc42Templates, error) {
	return s.templates, nil
}

func (s *TemplateSourceMock) SetTemplates(templates usecase.Arc42Templates) {
	s.templates = templates
}
