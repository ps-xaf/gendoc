//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

func TestEmptySysContextRenderedToEmptyPlantUML(t *testing.T) {
	// GIVEN
	sysctx := &domain.Node{
		Name:      "x",
		Archetype: domain.ArchetypeSystemContext,
	}
	renderer := usecase.NewPlantUMLRenderer()
	// WHEN
	plantUML := renderer.Render(sysctx)
	// THEN
	assert.Equal(t, "frame \"x\" <<system context>> as CTX_0\n\n\n\n", plantUML)
}

// Render hand-built graph.
func TestSimpleSysContextRenderedToPlantUML(t *testing.T) {
	// GIVEN
	factory := domain.NewNodeFactory()
	interface1 := factory.AcquireInterfaceNode("Interface #1", "if1")
	deployment1 := factory.AcquireDeploymentNode("Deployment #1", "dep1")
	deployment1.ExposesInterface(interface1)
	system := factory.AcquireSystemNode("My System", "sys1")
	system.HasDeploymentVariant(deployment1)

	deployment2 := factory.AcquireDeploymentNode("Deployment #2", "dep2")
	deployment2.ConsumesInterface(interface1)
	system.HasDeploymentVariant(deployment2)

	sysctx := factory.AcquireSystemContextGraph("My System", "ctx1")
	sysctx.ContainsSystem(system)
	sysctx.Contains(interface1)

	renderer := usecase.NewPlantUMLRenderer()
	// WHEN
	plantUML := renderer.Render(sysctx)
	// THEN
	assert.Equal(
		t,
		`frame "My System" <<system context>> as CTX_0 {
    component "Interface #1" <<interface>> as IFACE_1
    rectangle "My System" <<system>> as SYS_2 {
        rectangle "Deployment #1" <<deployment>> as DEP_3
        rectangle "Deployment #2" <<deployment>> as DEP_4
    }
}


DEP_3 -down-> IFACE_1:"exposes"

DEP_4 -down-> IFACE_1:"consumes"
`,
		plantUML)
}
