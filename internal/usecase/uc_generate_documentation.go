//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import (
	"fmt"
	"regexp"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

type UseCaseGenerateDocumentation struct {
	projectYAMLReader            ProjectDescriptionSource
	versionPropertiesReader      ProjectDescriptionSource
	localSCMInfo                 ProjectDescriptionSource
	templateSource               Arc42TemplateSource
	developerChaptersSource      DevelopersArc42ChaptersSource
	systemContextDiagramRenderer UMLGraphRenderer
	generateDocumentFrom         TemplateEngine
	imageCopier                  DocumentCopier
	additionalDocumentsCopier    DocumentCopier
	documentationDestination     DocumentationDestination
	projectDescriptionReader     ProjectDescriptionRemoteReader
	projectDescriptionCollector  ProjectDescriptionsCollector
}

func NewUseCaseGenerateDocumentation() *UseCaseGenerateDocumentation {
	return &UseCaseGenerateDocumentation{}
}

func (uc *UseCaseGenerateDocumentation) WithProjectYAMLFrom(reader ProjectDescriptionSource) *UseCaseGenerateDocumentation {
	uc.projectYAMLReader = reader
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithVersionPropertiesFrom(reader ProjectDescriptionSource) *UseCaseGenerateDocumentation {
	uc.versionPropertiesReader = reader
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithLocalSCMInfosFrom(reader ProjectDescriptionSource) *UseCaseGenerateDocumentation {
	uc.localSCMInfo = reader
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithTemplatesFrom(source Arc42TemplateSource) *UseCaseGenerateDocumentation {
	uc.templateSource = source
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithDeveloperChaptersFrom(source DevelopersArc42ChaptersSource) *UseCaseGenerateDocumentation {
	uc.developerChaptersSource = source
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithSystemContextDiagramRenderer(sysContextRenderer UMLGraphRenderer) *UseCaseGenerateDocumentation {
	uc.systemContextDiagramRenderer = sysContextRenderer
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithTemplateEngine(engine TemplateEngine) *UseCaseGenerateDocumentation {
	uc.generateDocumentFrom = engine
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithImageCopier(imageCopier DocumentCopier) *UseCaseGenerateDocumentation {
	uc.imageCopier = imageCopier
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithAdditionalDocumentsCopier(docsCopier DocumentCopier) *UseCaseGenerateDocumentation {
	uc.additionalDocumentsCopier = docsCopier
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithDocumentationStoredAt(dest DocumentationDestination) *UseCaseGenerateDocumentation {
	uc.documentationDestination = dest
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithProjectDescriptionReader(reader ProjectDescriptionRemoteReader) *UseCaseGenerateDocumentation {
	uc.projectDescriptionReader = reader
	return uc
}

func (uc *UseCaseGenerateDocumentation) WithProjectDescriptionsCollector(collector ProjectDescriptionsCollector) *UseCaseGenerateDocumentation {
	uc.projectDescriptionCollector = collector
	return uc
}

func (g *UseCaseGenerateDocumentation) Generate(language LanguageCode) error {
	prjDescr, err := g.determineEffectiveProjectDescription()
	if err != nil {
		return err
	}
	templates, err := g.readAllTemplates(language)
	if err != nil {
		return err
	}
	developersChapters, err := g.developerChaptersSource.ReadAll()
	if err != nil {
		return err
	}
	allProjects, err := g.projectDescriptionCollector.CollectAll()
	if err != nil {
		return err
	}
	if i := FindThisProjectInRemotes(prjDescr, allProjects); i >= 0 {
		prjDescrFromRemote := (*allProjects)[i]
		TakeOverInformationFromRemote(prjDescr, prjDescrFromRemote)
		remotesWithoutThisProject := append((*allProjects)[:i], (*allProjects)[i+1:]...)
		allProjects = &remotesWithoutThisProject
	}
	sysContextDiagram := g.systemContextDiagramRenderer.Render(
		domain.SystemContextGraphFrom(prjDescr))
	domainDiagram, err := g.generateDomainDiagram(prjDescr, allProjects)
	if err != nil {
		return err
	}
	membershipStructure := domain.MembershipStructureFrom(prjDescr, allProjects)
	presentationModel := PresentationModelFrom(language, prjDescr, &developersChapters, sysContextDiagram, domainDiagram, membershipStructure)
	if err := g.populateAllArchitectureTemplates(templates, &presentationModel); err != nil {
		return err
	}
	if err := g.copyAllImages(); err != nil {
		return err
	}
	if err := g.copyAdditionalDocuments(); err != nil {
		return err
	}
	return nil
}

func (g *UseCaseGenerateDocumentation) determineEffectiveProjectDescription() (*domain.ProjectDescription, error) {
	projectYAML, err := g.projectYAMLReader.Read()
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read project description: %w", err)
	}
	versionProps, err := g.versionPropertiesReader.Read()
	if err != nil && !strings.HasSuffix(err.Error(), "no such file or directory") {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read additional/optional version information: %w", err)
	}
	localSCMInfo, err := g.localSCMInfo.Read()
	if err != nil && !strings.HasSuffix(err.Error(), "repository does not exist") && !strings.HasSuffix(err.Error(), "artifact ID nested too deeply") {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read from local SCM repository: %w", err)
	}
	effectivePrjDescr, err := domain.EffectiveProjectDescription(projectYAML, versionProps, localSCMInfo)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to construct effective project description: %w", err)
	}
	return effectivePrjDescr, err
}

func (g *UseCaseGenerateDocumentation) readAllTemplates(language LanguageCode) (*Arc42Templates, error) {
	arc42Templates, err := g.templateSource.Read(language)
	if err != nil {
		return &Arc42Templates{}, fmt.Errorf("failed to read templates: %w", err)
	}
	return &arc42Templates, nil
}

func (g *UseCaseGenerateDocumentation) populateAllArchitectureTemplates(templates *Arc42Templates, presentationModel *PresentationModel) error {
	for template := range *allTemplates() {
		if g.isAllInformationAvailableToPopulate(template, presentationModel) {
			renderingModel := templateRenderingSpecifications[template].renderingModel(presentationModel)
			if err := g.populateTemplate(template, templates, renderingModel); err != nil {
				return err
			}
		}
	}
	return nil
}

func allTemplates() *generatableSpecifications {
	return &templateRenderingSpecifications
}

func (g *UseCaseGenerateDocumentation) isAllInformationAvailableToPopulate(templateName Arc42TemplateName, presentationModel *PresentationModel) bool {
	return templateRenderingSpecifications[templateName].isGenerationPossible(presentationModel)
}

func (g *UseCaseGenerateDocumentation) populateTemplate(templateName Arc42TemplateName, arc42Templates *Arc42Templates, presentationModel EffectivePresentationModel) error {
	template := (*arc42Templates)[templateName]
	templateInfo := templateRenderingSpecifications[templateName]
	if err := g.documentationDestination.Open(templateInfo.documentName); err != nil {
		return fmt.Errorf("failed to open target diagram/table: %w", err)
	}
	defer g.documentationDestination.Close()
	err := g.generateDocumentFrom(template, presentationModel, g.documentationDestination)
	if err != nil {
		return fmt.Errorf("templating error: %w", err)
	}
	return nil
}

type generatablePredicate func(*PresentationModel) bool
type renderableModel func(*PresentationModel) EffectivePresentationModel

func UnrestrictedPresentationModel(p *PresentationModel) EffectivePresentationModel { return p }

type generatableSpecification struct {
	documentName         DocumentName
	isGenerationPossible generatablePredicate
	renderingModel       renderableModel
}
type generatableSpecifications map[Arc42TemplateName]generatableSpecification

var templateRenderingSpecifications = generatableSpecifications{
	// Content aka Arc42 architecture documentation
	A42Template01Introduction: {
		documentName:         DocumentName01Introduction,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template02Constraints: {
		documentName:         DocumentName02Constraints,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template03Context: {
		documentName:         DocumentName03Context,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template04Strategy: {
		documentName:         DocumentName04Strategy,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template05BuildingBlocks: {
		documentName:         DocumentName05BuildingBlocks,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template06Runtime: {
		documentName:         DocumentName06Runtime,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template07Deployment: {
		documentName:         DocumentName07Deployment,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template08Concepts: {
		documentName:         DocumentName08Concepts,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template09Decisions: {
		documentName:         DocumentName09Decisions,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template10Quality: {
		documentName:         DocumentName10Quality,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template11Risks: {
		documentName:         DocumentName11Risks,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42Template12Glossary: {
		documentName:         DocumentName12Glossary,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	// Configs etc.
	A42TemplateDocToolchainConf: {
		documentName:         DocumentNameDocToolchainConf,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	// Landing page
	A42TemplateLandingPage: {
		documentName:         DocumentNameLandingPage,
		isGenerationPossible: always,
		renderingModel:       UnrestrictedPresentationModel,
	},
	// Diagrams and tables
	A42TemplateSystemContext: {
		documentName:         DocumentNameSystemContext,
		isGenerationPossible: inCaseSystemContextAvailable,
		renderingModel:       func(p *PresentationModel) EffectivePresentationModel { return p.SystemContext },
	},
	A42TemplateDefinedInterfaces: {
		documentName:         DocumentNameDefinedInterfaces,
		isGenerationPossible: inCaseInterfacesAreDefined,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42TemplateDeploymentVariants: {
		documentName:         DocumentNameDeploymentVariants,
		isGenerationPossible: inCaseDeploymentsAreDefined,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42TemplateDomainDiagram: {
		documentName:         DocumentNameDomainDiagram,
		isGenerationPossible: inCaseDomainDiagramAvailable,
		renderingModel:       func(p *PresentationModel) EffectivePresentationModel { return p.DomainDiagram },
	},
	A42TemplateDomainStructureTable: {
		documentName:         DocumentNameDomainStructureTable,
		isGenerationPossible: inCaseMembershipTableAvailable,
		renderingModel:       UnrestrictedPresentationModel,
	},
	A42TemplateBriefDescription: {
		documentName:         DocumentNameBriefDescription,
		isGenerationPossible: always,
		renderingModel:       func(p *PresentationModel) EffectivePresentationModel { return p.BriefDescription },
	},
}

//---- "Ordinary" documents ---------------------------------------------------

func always(*PresentationModel) bool { return true }

//---- System context ---------------------------------------------------------

func inCaseSystemContextAvailable(presentationModel *PresentationModel) bool {
	return presentationModel.SystemContext.Diagram != ""
}

//---- Table of defined interfaces --------------------------------------------

func inCaseInterfacesAreDefined(presentationModel *PresentationModel) bool {
	return len(presentationModel.ProjectDescription.DefinedInterfaces) > 0
}

//---- Table of deployment variants -------------------------------------------

func inCaseDeploymentsAreDefined(presentationModel *PresentationModel) bool {
	return len(presentationModel.ProjectDescription.DeploymentVariants) > 0
}

//---- Domain diagram ---------------------------------------------------------

func inCaseDomainDiagramAvailable(presentationModel *PresentationModel) bool {
	return presentationModel.DomainDiagram.Diagram != ""
}

func inCaseMembershipTableAvailable(presentationModel *PresentationModel) bool {
	return len(presentationModel.DomainTable) > 0
}

//---- Copy images ------------------------------------------------------------

func (g *UseCaseGenerateDocumentation) copyAllImages() error {
	return g.imageCopier.CopyAll()
}

//---- Documents in addition to Arc42 chapters --------------------------------

func (g *UseCaseGenerateDocumentation) copyAdditionalDocuments() error {
	var fileNamesToExclude = regexp.MustCompile(`^(0[1-9])|(1[0-2])_.*\.adoc$`)
	return g.additionalDocumentsCopier.CopyAllBut(fileNamesToExclude)
}

//---- House keeping ----------------------------------------------------------

func FindThisProjectInRemotes(prjDescrFromLocal *domain.ProjectDescription, prjDescrsFromRemote *[]*domain.ProjectDescription) int {
	for i, prjDescrFromRemote := range *prjDescrsFromRemote {
		if prjDescrFromLocal.IsSameArtifact(prjDescrFromRemote) {
			return i
		}
	}
	return -1
}

func TakeOverInformationFromRemote(prjDescrFromLocal, prjDescrFromRemote *domain.ProjectDescription) {
	for _, topicFromRemote := range prjDescrFromLocal.Topics {
		prjDescrFromLocal.Topics.Unite(topicFromRemote)
	}
	prjDescrFromLocal.SCMWebURL = prjDescrFromRemote.SCMWebURL
}

//---- Generate big picture (domain diagram) ----------------------------------

func (g *UseCaseGenerateDocumentation) generateDomainDiagram(rootProject *domain.ProjectDescription, allProjects *[]*domain.ProjectDescription) (string, error) {
	diagram := domain.DomainGraphFrom(rootProject, allProjects)
	plantUML := g.systemContextDiagramRenderer.Render(diagram)
	return plantUML, nil
}
