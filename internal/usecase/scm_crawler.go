//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import "net/url"

type SCMCrawler interface {
	InfosOfAllProjects() (*[]*SCMProjectDescriptor, error)
}

type SCMProjectDescriptor struct {
	ID        string
	SCMWebURL url.URL
	Topics    []string // GitLab tags, not to be confused with Git tags
}
