//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import (
	"fmt"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

type PlantUMLRenderer struct {
	ID       int
	depth    int
	plantUML strings.Builder
	plantID  map[domain.NodeID]string
}

var _ UMLGraphRenderer = (*PlantUMLRenderer)(nil)

func NewPlantUMLRenderer() *PlantUMLRenderer {
	return &PlantUMLRenderer{
		depth:    0,
		ID:       0,
		plantUML: strings.Builder{},
		plantID:  map[domain.NodeID]string{},
	}
}

func (r *PlantUMLRenderer) Render(graph *domain.Node) string {
	r.ID = 0
	r.depth = 0
	r.plantUML.Reset()
	graph.SortRelations() // stable order results in stable rendering, helps testing
	r.renderContainsRelation(graph)
	r.renderNewLine()
	r.renderNewLine()
	r.renderDeployedAsRelation(graph)
	r.renderNewLine()
	r.renderExposesInterfaceRelation(graph)
	r.renderNewLine()
	r.renderConsumesInterfaceRelation(graph)
	return r.plantUML.String()
}

func (r *PlantUMLRenderer) renderContainsRelation(node *domain.Node) {
	if node.HoldsOtherComponents() {
		r.renderInnerNode(node)
	} else {
		r.renderLeafNode(node)
	}
}

func (r *PlantUMLRenderer) renderLeafNode(node *domain.Node) {
	r.render("%s %q <<%s>> as %s",
		leafType[node.Archetype], node.Name, archetypeName[node.Archetype], r.newPlantID(node))
}

func (r *PlantUMLRenderer) renderInnerNode(parent *domain.Node) {
	r.render("%s %q <<%s>> as %s {", containerType[parent.Archetype], parent.Name, archetypeName[parent.Archetype], r.newPlantID(parent))
	r.increaseIndentation()
	for _, child := range parent.ContainedComponents() {
		r.renderNewLine()
		r.renderContainsRelation(child)
	}
	r.decreaseIndentation()
	r.renderNewLine()
	r.renderRune('}')
}

var leafType = map[domain.Archetype]string{
	domain.ArchetypeApplication:         "component",
	domain.ArchetypeDeployment:          "rectangle",
	domain.ArchetypeDomain:              "rectangle",
	domain.ArchetypeDomainDiagram:       "frame",
	domain.ArchetypeInterface:           "component",
	domain.ArchetypeInterfaceDefinition: "component",
	domain.ArchetypeLibrary:             "component",
	domain.ArchetypeOrganization:        "rectangle",
	domain.ArchetypeService:             "component",
	domain.ArchetypeSubdomain:           "rectangle",
	domain.ArchetypeSystem:              "rectangle",
	domain.ArchetypeSystemContext:       "frame",
	domain.ArchetypeTool:                "component",
	domain.ArchetypeAnyArtifact:         "rectangle",
}

var containerType = map[domain.Archetype]string{
	domain.ArchetypeApplication:         "rectangle",
	domain.ArchetypeDeployment:          "rectangle",
	domain.ArchetypeDomain:              "rectangle",
	domain.ArchetypeDomainDiagram:       "frame",
	domain.ArchetypeInterface:           "rectangle",
	domain.ArchetypeInterfaceDefinition: "rectangle",
	domain.ArchetypeLibrary:             "rectangle",
	domain.ArchetypeOrganization:        "rectangle",
	domain.ArchetypeService:             "rectangle",
	domain.ArchetypeSubdomain:           "rectangle",
	domain.ArchetypeSystem:              "rectangle",
	domain.ArchetypeSystemContext:       "frame",
	domain.ArchetypeTool:                "rectangle",
	domain.ArchetypeAnyArtifact:         "rectangle",
}

// TODO localize
var archetypeName = map[domain.Archetype]string{
	domain.ArchetypeApplication:         "application",
	domain.ArchetypeDeployment:          "deployment",
	domain.ArchetypeDomain:              "domain",
	domain.ArchetypeDomainDiagram:       "domain diagram",
	domain.ArchetypeInterface:           "interface",
	domain.ArchetypeInterfaceDefinition: "interface definition",
	domain.ArchetypeLibrary:             "library",
	domain.ArchetypeOrganization:        "organization",
	domain.ArchetypeService:             "service",
	domain.ArchetypeSubdomain:           "subdomain",
	domain.ArchetypeSystem:              "system",
	domain.ArchetypeSystemContext:       "system context",
	domain.ArchetypeTool:                "tool",
	domain.ArchetypeAnyArtifact:         "app/service/...",
}

func (r *PlantUMLRenderer) newPlantID(node *domain.Node) string {
	newID := fmt.Sprintf("%s_%d", plantUMLIDPrefix[node.Archetype], r.nextID())
	r.plantID[node.ID] = newID
	return newID
}

func (r *PlantUMLRenderer) nextID() int {
	curr := r.ID
	r.ID += 1
	return curr
}

var plantUMLIDPrefix = map[domain.Archetype]string{
	domain.ArchetypeApplication:         "APP",
	domain.ArchetypeDeployment:          "DEP",
	domain.ArchetypeDomain:              "DOM",
	domain.ArchetypeDomainDiagram:       "DOMDIAG",
	domain.ArchetypeInterface:           "IFACE",
	domain.ArchetypeInterfaceDefinition: "IFACEDEF",
	domain.ArchetypeLibrary:             "LIB",
	domain.ArchetypeOrganization:        "ORG",
	domain.ArchetypeService:             "SRVS",
	domain.ArchetypeSubdomain:           "SUBDOM",
	domain.ArchetypeSystem:              "SYS",
	domain.ArchetypeSystemContext:       "CTX",
	domain.ArchetypeTool:                "TOOL",
	domain.ArchetypeAnyArtifact:         "PROV",
}

func (r *PlantUMLRenderer) renderDeployedAsRelation(node *domain.Node) {
	for _, iface := range node.DeployedComponents() {
		//TODO if one of these %s, %q etc missing --> error,
		//     should not happen as structure should be verified, so make it assertions
		r.render("%s -down-> %s:%q", r.plantID[node.ID], r.plantID[iface.ID], relationLabel[domain.RelationTypeDeployedAs])
		r.renderNewLine()
	}
	for _, child := range node.ContainedComponents() {
		r.renderDeployedAsRelation(child)
	}
}

func (r *PlantUMLRenderer) renderExposesInterfaceRelation(node *domain.Node) {
	for _, iface := range node.ExposedInterfaces() {
		r.render("%s -down-> %s:%q", r.plantID[node.ID], r.plantID[iface.ID], relationLabel[domain.RelationTypeExposesInterface])
		r.renderNewLine()
	}
	for _, child := range node.ContainedComponents() {
		r.renderExposesInterfaceRelation(child)
	}
}

func (r *PlantUMLRenderer) renderConsumesInterfaceRelation(node *domain.Node) {
	for _, iface := range node.ConsumedInterfaces() {
		r.render("%s -down-> %s:%q", r.plantID[node.ID], r.plantID[iface.ID], relationLabel[domain.RelationTypeConsumesInterface])
		r.renderNewLine()
	}
	for _, child := range node.ContainedComponents() {
		r.renderConsumesInterfaceRelation(child)
	}
}

// TODO localize
var relationLabel = map[domain.RelationType]string{
	domain.RelationTypeContains:          "contains",
	domain.RelationTypeDeployedAs:        "deployed as",
	domain.RelationTypeExposesInterface:  "exposes",
	domain.RelationTypeConsumesInterface: "consumes",
}

func (r *PlantUMLRenderer) increaseIndentation() {
	r.depth += 1
}

func (r *PlantUMLRenderer) decreaseIndentation() {
	r.depth -= 1
}

func (r *PlantUMLRenderer) renderNewLine() {
	r.plantUML.WriteRune('\n')
	for i := 0; i < r.depth; i++ {
		r.plantUML.WriteString("    ")
	}
}

func (r *PlantUMLRenderer) render(format string, a ...any) {
	r.plantUML.WriteString(fmt.Sprintf(format, a...))
}

func (r *PlantUMLRenderer) renderRune(singleChar rune) {
	r.plantUML.WriteRune(singleChar)
}
