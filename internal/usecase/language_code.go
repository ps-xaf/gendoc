//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

type LanguageCode int

const (
	LangDE LanguageCode = 0
	LangEN LanguageCode = 1
	LangES LanguageCode = 2
)
