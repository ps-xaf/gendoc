//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

type Arc42TemplateSource interface {
	Read(LanguageCode) (Arc42Templates, error)
}
