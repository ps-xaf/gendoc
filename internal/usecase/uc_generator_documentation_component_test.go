//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
	"zahnleiter.org/gendoc/internal/usecase"
)

// This tests the  generator component in conjunction  with other components of
// this project.  It is no integration test though as it does not test the exe-
// cutable  nor the  docker container.  Also it is only a  partially integrated
// application, as not all components are covered.
func TestGeneratorComponent(t *testing.T) {
	// GIVEN
	domainModel := domain.ProjectDescription{
		ArtifactID: domain.FullyQualifiedArtifactName{
			System: "gendoc",
		},
		ArtifactType: domain.ArtifactTypeSystem,
		Version: domain.SemanticVersion{
			Major: 1,
			Minor: 2,
			Patch: 3,
			Addendum: domain.VersionAddendum{
				Type:      domain.AddendumReleaseCandidate,
				RCVersion: 4,
			},
		},
		LandingPage: domain.LandingPage{
			Title:       "LP Title",
			Description: "LP Description",
			Subtitle:    "LP Subtitle",
		},
	}
	prjYAMLSourceMock := ProjectYAMLSourceMock{}
	prjYAMLSourceMock.SetProjectDescription(domainModel)
	arc42Templates := usecase.Arc42Templates{
		usecase.A42Template01Introduction: "{{.PageDesign.PageName}} | {{.PageDesign.ArcDocMenuEntry}}",
	}
	templateSourceMock := TemplateSourceMock{}
	templateSourceMock.SetTemplates(arc42Templates)
	docsDest := NewDocumentationDestinationMock()
	generator := usecase.NewUseCaseGenerateDocumentation().
		WithAdditionalDocumentsCopier(&DocumentsCopierMock{}).
		WithDeveloperChaptersFrom(&ChaptersSourceMock{}).
		WithDocumentationStoredAt(docsDest).
		WithImageCopier(&DocumentsCopierMock{}).
		WithSystemContextDiagramRenderer(&SystemContextRendererMock{}).
		WithTemplateEngine(infrastructure.GoTemplateEngine).
		WithLocalSCMInfosFrom(&SCMInfoSourceMock{}).
		WithProjectYAMLFrom(&prjYAMLSourceMock).
		WithTemplatesFrom(&templateSourceMock).
		WithVersionPropertiesFrom(&VersionPropertiesSourceMock{}).
		WithProjectDescriptionsCollector(usecase.NewNoProjectDescriptionsCollector()) // This does nothing
	// WHEN
	err := generator.Generate(usecase.LangEN)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, 1, len(docsDest.output))
	assert.Equal(t, "System gendoc | Architecture Documentation", string(docsDest.output[usecase.DocumentName01Introduction]))
}
