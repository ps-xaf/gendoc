//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

import "io"

type DocumentationDestination interface {
	Open(DocumentName) error
	Write([]byte) (int, error)
	Close() error
}

var _ io.Writer = (DocumentationDestination)(nil)
var _ io.Closer = (DocumentationDestination)(nil)
