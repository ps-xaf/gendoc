//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package usecase

// You can actually pass  everything into the  template enginge as presentation
// model as long as the Go template understands it.  Most of the time this will
// be an instance of struct PresentationModel though. This way one can pass ex-
// cerps of PresentationModel to the template engine so that some templates may
// only see a relevant subset instead of the complete presentation model.
type EffectivePresentationModel interface{}

type TemplateEngine func(Arc42Template, EffectivePresentationModel, DocumentationDestination) error
