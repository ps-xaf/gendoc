//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"os"

	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

type DevelopersArc42ChaptersFileSource struct {
	basePath string
}

var _ usecase.DevelopersArc42ChaptersSource = (*DevelopersArc42ChaptersFileSource)(nil)

func NewDevelopersArc42ChaptersFileSource(basePath string) *DevelopersArc42ChaptersFileSource {
	return &DevelopersArc42ChaptersFileSource{basePath: basePath}
}

func (s *DevelopersArc42ChaptersFileSource) ReadAll() (domain.DevelopersArc42Chapters, error) {
	var chapters domain.DevelopersArc42Chapters
	for templateName := range chapterFileNames {
		chapter, err := s.readChapter(templateName)
		if err != nil {
			return domain.DevelopersArc42Chapters{}, err
		}
		mapChapterIntoChapters[templateName](chapter, &chapters)
	}
	return chapters, nil
}

func (s *DevelopersArc42ChaptersFileSource) readChapter(templateName usecase.Arc42TemplateName) (domain.DeveloperArc42Chapter, error) {
	fileName := s.basePath + "/" + chapterFileNames[templateName]
	fileContent, err := os.ReadFile(fileName)
	if err != nil {
		return "", err
	}
	template := domain.DeveloperArc42Chapter(string(fileContent))
	return template, nil
}

var chapterFileNames = map[usecase.Arc42TemplateName]string{
	usecase.A42Template01Introduction:   "01_introduction_and_goals.adoc",
	usecase.A42Template02Constraints:    "02_architecture_constraints.adoc",
	usecase.A42Template03Context:        "03_system_scope_and_context.adoc",
	usecase.A42Template04Strategy:       "04_solution_strategy.adoc",
	usecase.A42Template05BuildingBlocks: "05_building_block_view.adoc",
	usecase.A42Template06Runtime:        "06_runtime_view.adoc",
	usecase.A42Template07Deployment:     "07_deployment_view.adoc",
	usecase.A42Template08Concepts:       "08_concepts.adoc",
	usecase.A42Template09Decisions:      "09_architecture_decisions.adoc",
	usecase.A42Template10Quality:        "10_quality_requirements.adoc",
	usecase.A42Template11Risks:          "11_technical_risks.adoc",
	usecase.A42Template12Glossary:       "12_glossary.adoc",
}

type chapterMapper func(domain.DeveloperArc42Chapter, *domain.DevelopersArc42Chapters)

var mapChapterIntoChapters = map[usecase.Arc42TemplateName]chapterMapper{
	usecase.A42Template01Introduction: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.IntroductionAndGoals = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template02Constraints: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.ArchitectureConstraints = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template03Context: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.SystemScopeAndContext = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template04Strategy: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.SolutionStrategy = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template05BuildingBlocks: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.BuildingBlockView = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template06Runtime: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.RuntimeView = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template07Deployment: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.DeploymentView = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template08Concepts: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.Concepts = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template09Decisions: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.ArchitectureDecisions = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template10Quality: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.QualityRequirements = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template11Risks: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.TechnicalRisks = domain.DeveloperArc42Chapter(val)
	},
	usecase.A42Template12Glossary: func(val domain.DeveloperArc42Chapter, target *domain.DevelopersArc42Chapters) {
		target.Glossary = domain.DeveloperArc42Chapter(val)
	},
}
