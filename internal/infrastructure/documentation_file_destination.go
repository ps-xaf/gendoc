//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"zahnleiter.org/gendoc/internal/usecase"
)

type DocumentationFileDestination struct {
	basePath string
	file     *os.File
}

var _ usecase.DocumentationDestination = (*DocumentationFileDestination)(nil)

func NewDocumentFileDestination(basePath string) *DocumentationFileDestination {
	return &DocumentationFileDestination{basePath: basePath}
}

func (d *DocumentationFileDestination) Open(documentName usecase.DocumentName) error {
	// if err := os.Mkdir(d.basePath, os.ModePerm); err != nil && !strings.HasSuffix(err.Error(), "file exists") {
	// 	return fmt.Errorf("failed to create build directory: %w", err)
	// }
	fileName := d.basePath + "/" + documentFileNames[documentName]
	err := os.MkdirAll(directoryPathOnly(fileName), os.ModePerm)
	if err != nil && !strings.HasSuffix(err.Error(), "exists") {
		return fmt.Errorf("failed to create folder for generated document: %w", err)
	}
	file, err := os.Create(fileName)
	if err != nil {
		return fmt.Errorf("failed to create file for generated document: %w", err)
	}
	d.file = file
	return nil
}

func directoryPathOnly(fileName string) string {
	dir, _ := filepath.Split(fileName)
	return dir
}

func (d *DocumentationFileDestination) Write(buffer []byte) (int, error) {
	return d.file.Write(buffer)
}

func (d *DocumentationFileDestination) Close() error {
	err := d.file.Close()
	d.file = nil
	return err
}

var documentFileNames = map[usecase.DocumentName]string{
	// Content aka architecture documentation
	usecase.DocumentName01Introduction:   "docs/arc42/chapters/01_introduction_and_goals.adoc",
	usecase.DocumentName02Constraints:    "docs/arc42/chapters/02_architecture_constraints.adoc",
	usecase.DocumentName03Context:        "docs/arc42/chapters/03_system_scope_and_context.adoc",
	usecase.DocumentName04Strategy:       "docs/arc42/chapters/04_solution_strategy.adoc",
	usecase.DocumentName05BuildingBlocks: "docs/arc42/chapters/05_building_block_view.adoc",
	usecase.DocumentName06Runtime:        "docs/arc42/chapters/06_runtime_view.adoc",
	usecase.DocumentName07Deployment:     "docs/arc42/chapters/07_deployment_view.adoc",
	usecase.DocumentName08Concepts:       "docs/arc42/chapters/08_concepts.adoc",
	usecase.DocumentName09Decisions:      "docs/arc42/chapters/09_architecture_decisions.adoc",
	usecase.DocumentName10Quality:        "docs/arc42/chapters/10_quality_requirements.adoc",
	usecase.DocumentName11Risks:          "docs/arc42/chapters/11_technical_risks.adoc",
	usecase.DocumentName12Glossary:       "docs/arc42/chapters/12_glossary.adoc",
	// Configs etc.
	usecase.DocumentNameDocToolchainConf: "docToolchain.groovy",
	usecase.DocumentNameLandingPage:      "docs/landingpage.gsp",
	// Diagrams and tables
	usecase.DocumentNameSystemContext:        "docs/arc42/chapters/system_context_generated.adoc",
	usecase.DocumentNameDefinedInterfaces:    "docs/arc42/chapters/table_of_defined_interfaces_generated.adoc",
	usecase.DocumentNameDeploymentVariants:   "docs/arc42/chapters/table_of_deployment_variants_generated.adoc",
	usecase.DocumentNameDomainDiagram:        "docs/arc42/chapters/domain_diagram_generated.adoc",
	usecase.DocumentNameDomainStructureTable: "docs/arc42/chapters/table_of_domain_structure_generated.adoc",
	usecase.DocumentNameBriefDescription:     "docs/arc42/chapters/brief_description.adoc",
}
