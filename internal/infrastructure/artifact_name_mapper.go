//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

// For enterprise projects,  that are project made up of  multiple repositories
// to hold  many microservice,  libraries and so on,  we interpret the unstruc-
// tured artifact name like so:
// - "system"
// - "system/domain"
// - "system/domain/subdomain"
// - "system/domain/subdomain/artifact"
//
// Furthermore we need to take the artifact type into account.
//
// For isolated projects, that are projectst that are made up of one single re-
// pository and that  are delivering a single artifact,  we interpret the arti-
// fact name as of one of these forms:
// -                         "artifact"
// -                  "domain/artifact"
// -        "domain/subdomain/artifact"
// - "system/domain/subdomain/artifact"
//
// The artifact type is not relevant in this case.
func SetStructuredArtifactName(unstructured string, prjType domain.ProjectType, artifactType domain.ArtifactType, structured *domain.FullyQualifiedArtifactName) error {
	if strings.HasPrefix(unstructured, "/") {
		return fmt.Errorf(`illegal slash "/" at beginning of artifact ID`)
	}
	parts := strings.Split(unstructured, "/")
	if len(parts) <= 0 || strings.TrimSpace(parts[0]) == "" {
		return fmt.Errorf("artifact ID missing")
	}
	if len(parts) > 4 {
		return fmt.Errorf("artifact ID nested too deeply")
	}
	if prjType == domain.ProjectTypeEnterprise {
		return setStructuredArtifactNameEnterprise(parts, artifactType, structured)
	} else {
		return setStructuredArtifactNameSingle(parts, structured)
	}
}

func setStructuredArtifactNameEnterprise(parts []string, artifactType domain.ArtifactType, structured *domain.FullyQualifiedArtifactName) error {
	if len(parts) == 1 {
		if artifactType == domain.ArtifactTypeSystem {
			structured.System = domain.SystemName(parts[0])
			structured.Domain = ""
			structured.Subdomain = ""
			structured.Artifact = ""
		} else {
			structured.System = ""
			structured.Domain = ""
			structured.Subdomain = ""
			structured.Artifact = domain.ArtifactName(parts[0])
		}
	} else if len(parts) == 2 {
		if artifactType == domain.ArtifactTypeDomain {
			structured.System = domain.SystemName(parts[0])
			structured.Domain = domain.DomainName(parts[1])
			structured.Subdomain = ""
			structured.Artifact = ""
		} else {
			structured.System = domain.SystemName(parts[0])
			structured.Domain = ""
			structured.Subdomain = ""
			structured.Artifact = domain.ArtifactName(parts[1])
		}
	} else if len(parts) == 3 {
		if artifactType == domain.ArtifactTypeSubdomain {
			structured.System = domain.SystemName(parts[0])
			structured.Domain = domain.DomainName(parts[1])
			structured.Subdomain = domain.SubdomainName(parts[2])
			structured.Artifact = ""
		} else {
			structured.System = domain.SystemName(parts[0])
			structured.Domain = domain.DomainName(parts[1])
			structured.Subdomain = ""
			structured.Artifact = domain.ArtifactName(parts[2])
		}
	} else if len(parts) == 4 {
		structured.System = domain.SystemName(parts[0])
		structured.Domain = domain.DomainName(parts[1])
		structured.Subdomain = domain.SubdomainName(parts[2])
		structured.Artifact = domain.ArtifactName(parts[3])
	}
	return nil
}

func setStructuredArtifactNameSingle(parts []string, structured *domain.FullyQualifiedArtifactName) error {
	if len(parts) == 1 {
		structured.System = ""
		structured.Domain = ""
		structured.Subdomain = ""
		structured.Artifact = domain.ArtifactName(parts[0])
	} else if len(parts) == 2 {
		structured.System = ""
		structured.Domain = domain.DomainName(parts[0])
		structured.Subdomain = ""
		structured.Artifact = domain.ArtifactName(parts[1])
	} else if len(parts) == 3 {
		structured.System = ""
		structured.Domain = domain.DomainName(parts[0])
		structured.Subdomain = domain.SubdomainName(parts[1])
		structured.Artifact = domain.ArtifactName(parts[2])
	} else if len(parts) == 4 {
		structured.System = domain.SystemName(parts[0])
		structured.Domain = domain.DomainName(parts[1])
		structured.Subdomain = domain.SubdomainName(parts[2])
		structured.Artifact = domain.ArtifactName(parts[3])
	}
	return nil
}
