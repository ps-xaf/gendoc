//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/magiconair/properties"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestEmptyVersionProperties(t *testing.T) {
	// GIVEN
	emptyVerProps := properties.NewProperties()
	// WHEN
	_, err := infrastructure.DomainRepresentationFromVersionProperties(emptyVerProps)
	// THEN
	assert.EqualError(t, err, "version property missing or empty")
}

func TestIncompleteVersionProperties(t *testing.T) {
	// GIVEN
	incompleteVerProps := properties.NewProperties()
	incompleteVerProps.Set("version", "1.0")
	// WHEN
	_, err := infrastructure.DomainRepresentationFromVersionProperties(incompleteVerProps)
	// THEN
	assert.EqualError(t, err, "no semantic versioning, expect major.minor.patch and optional -addendum")
}

func TestOvercompleteVersionProperties(t *testing.T) {
	// GIVEN
	overcompleteVerProps := properties.NewProperties()
	overcompleteVerProps.Set("version", "1.2.3.4")
	// WHEN
	_, err := infrastructure.DomainRepresentationFromVersionProperties(overcompleteVerProps)
	// THEN
	assert.EqualError(t, err, "no semantic versioning, expect major.minor.patch and optional -addendum")
}

func TestValidVersionProperties(t *testing.T) {
	// GIVEN
	validVerProps := properties.NewProperties()
	validVerProps.Set("version", "1.2.3")
	// WHEN
	prjDescr, err := infrastructure.DomainRepresentationFromVersionProperties(validVerProps)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumUndefined, prjDescr.Version.Addendum.Type)
	assert.Equal(t, domain.RCVersion(0), prjDescr.Version.Addendum.RCVersion)
}

func TestValidVersionPropertiesWithAddendum(t *testing.T) {
	// GIVEN
	validVerProps := properties.NewProperties()
	validVerProps.Set("version", "1.2.3-RC4")
	// WHEN
	prjDescr, err := infrastructure.DomainRepresentationFromVersionProperties(validVerProps)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumReleaseCandidate, prjDescr.Version.Addendum.Type)
	assert.Equal(t, domain.RCVersion(4), prjDescr.Version.Addendum.RCVersion)
}

func TestAddendumMustNotBeLeftOutAfterDash(t *testing.T) {
	// GIVEN
	invalidVerProps := properties.NewProperties()
	invalidVerProps.Set("version", "1.2.3-")
	// WHEN
	_, err := infrastructure.DomainRepresentationFromVersionProperties(invalidVerProps)
	// THEN
	assert.EqualError(t, err, "addendum to version missing after dash")
}
