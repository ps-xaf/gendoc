//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestCannotMapEmptyOriginURL(t *testing.T) {
	// GIVEN
	emptyPath := ""
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		emptyPath, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &artefactName)
	// THEN
	assert.EqualError(t, err, "artifact ID missing")
}

func TestOriginURLMappedToArtifactName(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName(""), artefactName.System)
	assert.Equal(t, domain.DomainName(""), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName(""), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

func TestOriginURLMappedToDomainAndArtifactName(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "mygroup/myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName(""), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName(""), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

func TestOriginURLMappedToDomainAndSubdomainAndArtifactName(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "mygroup/mysubgroup/myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName(""), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName("mysubgroup"), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

func TestOriginURLMappedToSystemAndDomainAndSubdomainAndArtifactName(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "mysystem/mygroup/mysubgroup/myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("mysystem"), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName("mysubgroup"), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

// The origin URL is prefixed with an slash "/" when cloned via HHTP, it seems.
// When cloned via SSH, no such prefix exists.
func TestOriginURLMappedCorrectlyWhenClonedViaHTTP(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "/mysystem/mygroup/mysubgroup/myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("mysystem"), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName("mysubgroup"), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

func TestOriginURLMustNotBeNestedTooDeeply(t *testing.T) {
	// GIVEN
	pathWithoutGitGroup := "to-deep/mysystem/mygroup/mysubgroup/myrepo.git"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetArtifactNameFromOriginURL(
		pathWithoutGitGroup, domain.ProjectTypeSingle, domain.ArtifactTypeService, &artefactName)
	// THEN
	assert.EqualError(t, err, "artifact ID nested too deeply")
}
