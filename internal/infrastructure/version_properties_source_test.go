//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestReadingNonExistingVersionProperties(t *testing.T) {
	// GIVEN
	nonExistingVersionFile := "../test_files/doesnotexist.properties"
	reader := infrastructure.NewVersionPropertiesSource(nonExistingVersionFile)
	// WHEN
	_, err := reader.Read()
	// THEN
	assert.EqualError(t, err, "failed to read version properties: open ../test_files/doesnotexist.properties: no such file or directory")
}

func TestReadingEmptyVersionProperties(t *testing.T) {
	// GIVEN
	emptyVersionFile := "../../test_files/empty_version.properties"
	reader := infrastructure.NewVersionPropertiesSource(emptyVersionFile)
	// WHEN
	_, err := reader.Read()
	// THEN
	assert.EqualError(t, err, "failed to unmarshal version properties: version property missing or empty")
}

func TestReadingValidVersionProperties(t *testing.T) {
	// GIVEN
	validVersionFile := "../../test_files/valid_version.properties"
	reader := infrastructure.NewVersionPropertiesSource(validVersionFile)
	// WHEN
	prjDescr, err := reader.Read()
	// THEN
	require.NoError(t, err)
	assert.NotNil(t, prjDescr)
	// No detailed checks as mapper is tested separately
}
