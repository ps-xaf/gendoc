//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestMappingCommpleteProjectYAML(t *testing.T) {
	// GIVEN
	prjYAML := infrastructure.ProjectYAML{
		ArtifactID:   "smith-books/endcustomer/sales/webshop",
		Version:      "1.2.3-RC1",
		ArtifactType: "service",
		FriendlyName: "Smith Books - Web Shop",
		Description:  "Web shop for end customers",
		DefinesInterfaces: []infrastructure.InterfaceDefinition{
			{
				LocalID:          "books",
				FriendlyName:     "Books API",
				Description:      "Manage book resources via this REST API.",
				SpecificationURL: "models/books_api.yaml",
				Application: infrastructure.ApplicationSpec{
					Format:   "Books",
					Security: "login/password"},
				Communication: infrastructure.CommunicationSpec{
					Pattern:  "request/response",
					Style:    "sync",
					Format:   "JSON",
					Encoding: "UTF-8"},
				Transport: infrastructure.TransportSpec{
					Protocol: "http",
					Security: "TLS 1.2"},
			},
			{
				LocalID:          "authors",
				FriendlyName:     "Authors API",
				Description:      "Manage author resources via this REST API.",
				SpecificationURL: "models/authors_api.yaml",
				Application: infrastructure.ApplicationSpec{
					Format:   "Authors",
					Security: "login/password"},
				Communication: infrastructure.CommunicationSpec{
					Pattern:  "request/response",
					Style:    "sync",
					Format:   "JSON",
					Encoding: "UTF-8"},
				Transport: infrastructure.TransportSpec{
					Protocol: "http",
					Security: "TLS 1.2"},
			},
		},
		DeploymentVariants: []infrastructure.DeploymentVariant{
			{
				LocalID:                 "bookstore",
				FriendlyName:            "Bookstore",
				Purpose:                 "n/a",
				DeploymentDescriptorURL: "deployments/charts",
				ExposesInterfaces: []infrastructure.ExposedInterface{
					{LocalRef: "books"},
					{LocalRef: "authors"},
				},
				ConsumesInterfaces: []infrastructure.ConsumedInterface{
					{
						ArtifactRef:   "smith-books/endcustomer/sales/fulfillment",
						DeploymentRef: "fulfillment",
						InterfaceRef:  "fulfillment-api",
					},
				},
			},
		},
	}
	// WHEN
	prjDescr, err := infrastructure.DomainRepresentationFromProjectYAML(
		&prjYAML, domain.ProjectTypeEnterprise)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("smith-books"), prjDescr.ArtifactID.System)
	assert.Equal(t, domain.DomainName("endcustomer"), prjDescr.ArtifactID.Domain)
	assert.Equal(t, domain.SubdomainName("sales"), prjDescr.ArtifactID.Subdomain)
	assert.Equal(t, domain.ArtifactName("webshop"), prjDescr.ArtifactID.Artifact)

	assert.Equal(t, domain.ArtifactTypeService, prjDescr.ArtifactType)

	assert.Equal(t, 2, len(prjDescr.DefinedInterfaces))
	assert.Equal(t, domain.AppFormat("Books"), prjDescr.DefinedInterfaces[0].Application.Format)
	assert.Equal(t, domain.ApplicationSecurity("login/password"), prjDescr.DefinedInterfaces[0].Application.Security)
	assert.Equal(t, domain.CommsEncoding("UTF-8"), prjDescr.DefinedInterfaces[0].Communication.Encoding)
	assert.Equal(t, domain.CommsFormat("JSON"), prjDescr.DefinedInterfaces[0].Communication.Format)
	assert.Equal(t, domain.CommsPattern("request/response"), prjDescr.DefinedInterfaces[0].Communication.Pattern)
	assert.Equal(t, domain.CommsStyle("sync"), prjDescr.DefinedInterfaces[0].Communication.Style)
	assert.Equal(t, domain.InterfaceDescription("Manage book resources via this REST API."), prjDescr.DefinedInterfaces[0].Description)
	assert.Equal(t, domain.InterfaceName("Books API"), prjDescr.DefinedInterfaces[0].FriendlyName)
	assert.Equal(t, domain.LocalInterfaceID("books"), prjDescr.DefinedInterfaces[0].LocalID)
	URL, err := url.Parse("models/books_api.yaml")
	require.NoError(t, err)
	assert.Equal(t, domain.SpecificationURL(*URL), prjDescr.DefinedInterfaces[0].SpecificationURL)
	assert.Equal(t, domain.TransportProtocol("http"), prjDescr.DefinedInterfaces[0].Transport.Protocol)
	assert.Equal(t, domain.TransportSecurity("TLS 1.2"), prjDescr.DefinedInterfaces[0].Transport.Security)
	assert.Equal(t, domain.AppFormat("Authors"), prjDescr.DefinedInterfaces[1].Application.Format)
	assert.Equal(t, domain.ApplicationSecurity("login/password"), prjDescr.DefinedInterfaces[1].Application.Security)
	assert.Equal(t, domain.CommsEncoding("UTF-8"), prjDescr.DefinedInterfaces[1].Communication.Encoding)
	assert.Equal(t, domain.CommsFormat("JSON"), prjDescr.DefinedInterfaces[1].Communication.Format)
	assert.Equal(t, domain.CommsPattern("request/response"), prjDescr.DefinedInterfaces[1].Communication.Pattern)
	assert.Equal(t, domain.CommsStyle("sync"), prjDescr.DefinedInterfaces[1].Communication.Style)
	assert.Equal(t, domain.InterfaceDescription("Manage author resources via this REST API."), prjDescr.DefinedInterfaces[1].Description)
	assert.Equal(t, domain.InterfaceName("Authors API"), prjDescr.DefinedInterfaces[1].FriendlyName)
	assert.Equal(t, domain.LocalInterfaceID("authors"), prjDescr.DefinedInterfaces[1].LocalID)
	URL, err = url.Parse("models/authors_api.yaml")
	require.NoError(t, err)
	assert.Equal(t, domain.SpecificationURL(*URL), prjDescr.DefinedInterfaces[1].SpecificationURL)
	assert.Equal(t, domain.TransportProtocol("http"), prjDescr.DefinedInterfaces[1].Transport.Protocol)
	assert.Equal(t, domain.TransportSecurity("TLS 1.2"), prjDescr.DefinedInterfaces[1].Transport.Security)

	assert.Equal(t, 1, len(prjDescr.DeploymentVariants))
	URL, err = url.Parse("deployments/charts")
	require.NoError(t, err)
	assert.Equal(t, domain.DeploymentDescriptorURL(*URL), prjDescr.DeploymentVariants[0].DeploymentDescriptorURL)
	assert.Equal(t, domain.VariantName("Bookstore"), prjDescr.DeploymentVariants[0].FriendlyName)
	assert.Equal(t, 1, len(prjDescr.DeploymentVariants[0].ConsumedInterfaces))
	consumedIface := prjDescr.DeploymentVariants[0].ConsumedInterfaces[0].(*domain.ConsumedInternalInterface)
	assert.Equal(t, domain.ArtifactName("fulfillment"), consumedIface.ArtifactRef.Artifact)
	assert.Equal(t, domain.DomainName("endcustomer"), consumedIface.ArtifactRef.Domain)
	assert.Equal(t, domain.SubdomainName("sales"), consumedIface.ArtifactRef.Subdomain)
	assert.Equal(t, domain.SystemName("smith-books"), consumedIface.ArtifactRef.System)
	assert.Equal(t, domain.DeploymentLocalID("fulfillment"), consumedIface.DeploymentRef)
	assert.Equal(t, domain.LocalInterfaceID("fulfillment-api"), consumedIface.InterfaceRef)
	assert.Equal(t, 2, len(prjDescr.DeploymentVariants[0].ExposedInterfaces))
	assert.Equal(t, domain.LocalInterfaceID("books"), prjDescr.DeploymentVariants[0].ExposedInterfaces[0].LocalRef)
	assert.Equal(t, domain.LocalInterfaceID("authors"), prjDescr.DeploymentVariants[0].ExposedInterfaces[1].LocalRef)
	assert.Equal(t, domain.DeploymentLocalID("bookstore"), prjDescr.DeploymentVariants[0].LocalID)
	assert.Equal(t, domain.PurposeOfVariant("n/a"), prjDescr.DeploymentVariants[0].Purpose)
}

//TODO more tests, testing single aspects, not whole object tree
