//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

type ProjectYAMLFileSource struct {
	path        string
	projectType domain.ProjectType
}

var _ usecase.ProjectDescriptionSource = (*ProjectYAMLFileSource)(nil)

func NewProjectYAMLSource(path string, projectType domain.ProjectType) *ProjectYAMLFileSource {
	return &ProjectYAMLFileSource{
		path:        path,
		projectType: projectType,
	}
}

func (r *ProjectYAMLFileSource) Read() (*domain.ProjectDescription, error) {
	projectYAML, err := ReadProjectYAML(r.path)
	if err != nil {
		return &domain.ProjectDescription{}, err
	}
	return DomainRepresentationFromProjectYAML(projectYAML, r.projectType)
}

func ReadProjectYAML(path string) (*ProjectYAML, error) {
	yfile, err := os.ReadFile(path)
	if err != nil {
		return &ProjectYAML{}, fmt.Errorf("failed to read project description: %w", err)
	}
	var projectYAML ProjectYAML
	err = yaml.Unmarshal(yfile, &projectYAML)
	if err != nil {
		return &ProjectYAML{}, fmt.Errorf("failed to unmarshal project description: %w", err)
	}
	return &projectYAML, err
}
