//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"os"
	"regexp"
	"strings"

	copy "github.com/otiai10/copy"
	"zahnleiter.org/gendoc/internal/usecase"
)

type DocumentFileCopier struct {
	sourceDir      string
	destinationDir string
}

var _ usecase.DocumentCopier = (*DocumentFileCopier)(nil)

func NewDocumentsFileCopier(src, dest string) *DocumentFileCopier {
	return &DocumentFileCopier{sourceDir: src, destinationDir: dest}
}

func (c *DocumentFileCopier) MustCopyAll() error {
	return copy.Copy(c.sourceDir, c.destinationDir)
}

func (c *DocumentFileCopier) MustCopyAllBut(filesToExclude *regexp.Regexp) error {
	return copy.Copy(
		c.sourceDir,
		c.destinationDir,
		copy.Options{
			Skip: func(srcinfo os.FileInfo, src, dest string) (bool, error) {
				toBeSkipped := srcinfo.IsDir() || filesToExclude.MatchString(srcinfo.Name())
				return toBeSkipped, nil
			}})
}

func (c *DocumentFileCopier) CopyAll() error {
	if err := c.MustCopyAll(); !isIgnorable(err) {
		return err
	}
	return nil
}

func isIgnorable(err error) bool {
	return err == nil || strings.HasSuffix(err.Error(), "no such file or directory")
}

func (c *DocumentFileCopier) CopyAllBut(filesToExclude *regexp.Regexp) error {
	if err := c.MustCopyAllBut(filesToExclude); !isIgnorable(err) {
		return err
	}
	return nil
}
