//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"strings"

	"github.com/go-git/go-git/v5/plumbing"
	"zahnleiter.org/gendoc/internal/domain"
)

func SetArtifactNameFromOriginURL(repoPath string, prjType domain.ProjectType, artifactType domain.ArtifactType, artifact *domain.FullyQualifiedArtifactName) error {
	// Remove trailing ".git" file name extension
	normalized := strings.ReplaceAll(repoPath, ".git", "")
	// Pathes are prefixed with "/"  when cloned via HTTP,  there is no leading
	// "/" when cloned via SSH, it seems
	if strings.HasPrefix(normalized, "/") {
		normalized = strings.Replace(normalized, "/", "", 1)
	}
	return SetStructuredArtifactName(normalized, prjType, artifactType, artifact)
}

// Tags that are  not in the  semantic versioning  scheme are ignored.  We only
// care for tags that represent versions.
func MapTag(ref *plumbing.Reference, prjDescr *domain.ProjectDescription) {
	if version, err := StructuredVersionInformation(ref.Name().Short()); err == nil {
		if version.IsMoreRecentThan(&prjDescr.Version) {
			prjDescr.Version = version
		}
	}
}
