//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"text/template"

	"zahnleiter.org/gendoc/internal/usecase"
)

func GoTemplateEngine(sourceTemplate usecase.Arc42Template, data usecase.EffectivePresentationModel, targetDoc usecase.DocumentationDestination) error {
	engine := template.New("engine")
	if executableTemplate, err := engine.Parse(string(sourceTemplate)); err == nil {
		return executableTemplate.Execute(targetDoc, data)
	} else {
		return err
	}
}
