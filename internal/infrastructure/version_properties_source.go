//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"

	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"

	"github.com/magiconair/properties"
)

type VersionPropertiesSource struct {
	path string
}

var _ usecase.ProjectDescriptionSource = (*VersionPropertiesSource)(nil)

func NewVersionPropertiesSource(path string) *VersionPropertiesSource {
	return &VersionPropertiesSource{path: path}
}

func (r *VersionPropertiesSource) Read() (*domain.ProjectDescription, error) {
	// No custom struct for properties, use standard runtime type, as proper-
	// ties are very generic. For project.yaml and other more specific infor-
	// mation we defined custom structures.
	props, err := properties.LoadFile(r.path, properties.UTF8)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read version properties: %w", err)
	}
	prjDescr, err := DomainRepresentationFromVersionProperties(props)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to unmarshal version properties: %w", err)
	}
	return prjDescr, nil
}
