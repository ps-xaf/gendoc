//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"os"
	"path/filepath"

	"zahnleiter.org/gendoc/internal/usecase"
)

type Arc42TemplateFileSource struct {
	basePath string
}

var _ usecase.Arc42TemplateSource = (*Arc42TemplateFileSource)(nil)

func NewArc42TemplateFileSource(basePath string) *Arc42TemplateFileSource {
	return &Arc42TemplateFileSource{basePath: basePath}
}

func (s *Arc42TemplateFileSource) Read(lang usecase.LanguageCode) (usecase.Arc42Templates, error) {
	templates := usecase.Arc42Templates{}
	path := filepath.Join(s.basePath, languageFolderNames[lang])
	for name := range templateFileNames {
		template, err := readTemplate(path, name)
		if err != nil {
			return usecase.Arc42Templates{}, fmt.Errorf("failed to load arc42 template: %w", err)
		}
		templates[name] = *template
	}
	return templates, nil
}

func readTemplate(basePath string, templateName usecase.Arc42TemplateName) (*usecase.Arc42Template, error) {
	fileName := filepath.Join(basePath, templateFileNames[templateName])
	fileContent, err := os.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	template := usecase.Arc42Template(string(fileContent))
	return &template, nil
}

var languageFolderNames = map[usecase.LanguageCode]string{
	usecase.LangDE: "de",
	usecase.LangEN: "en",
	usecase.LangES: "es",
}

var templateFileNames = map[usecase.Arc42TemplateName]string{
	// Content aka architecture documentation
	usecase.A42Template01Introduction:   "docs/arc42/chapters/01_introduction_and_goals.adoc",
	usecase.A42Template02Constraints:    "docs/arc42/chapters/02_architecture_constraints.adoc",
	usecase.A42Template03Context:        "docs/arc42/chapters/03_system_scope_and_context.adoc",
	usecase.A42Template04Strategy:       "docs/arc42/chapters/04_solution_strategy.adoc",
	usecase.A42Template05BuildingBlocks: "docs/arc42/chapters/05_building_block_view.adoc",
	usecase.A42Template06Runtime:        "docs/arc42/chapters/06_runtime_view.adoc",
	usecase.A42Template07Deployment:     "docs/arc42/chapters/07_deployment_view.adoc",
	usecase.A42Template08Concepts:       "docs/arc42/chapters/08_concepts.adoc",
	usecase.A42Template09Decisions:      "docs/arc42/chapters/09_architecture_decisions.adoc",
	usecase.A42Template10Quality:        "docs/arc42/chapters/10_quality_requirements.adoc",
	usecase.A42Template11Risks:          "docs/arc42/chapters/11_technical_risks.adoc",
	usecase.A42Template12Glossary:       "docs/arc42/chapters/12_glossary.adoc",
	// Configs etc.
	usecase.A42TemplateDocToolchainConf: "docToolchain.groovy",
	usecase.A42TemplateLandingPage:      "docs/landingpage.gsp",
	// Diagrams and tables
	usecase.A42TemplateSystemContext:        "docs/arc42/plantuml_diagram.adoc",
	usecase.A42TemplateDefinedInterfaces:    "docs/arc42/chapters/table_of_defined_interfaces.adoc",
	usecase.A42TemplateDeploymentVariants:   "docs/arc42/chapters/table_of_deployment_variants.adoc",
	usecase.A42TemplateDomainDiagram:        "docs/arc42/plantuml_diagram.adoc",
	usecase.A42TemplateDomainStructureTable: "docs/arc42/chapters/table_of_domain_structure.adoc",
	usecase.A42TemplateBriefDescription:     "docs/arc42/chapters/brief_description.adoc",
}
