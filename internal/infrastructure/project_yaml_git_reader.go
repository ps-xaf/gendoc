//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"gopkg.in/yaml.v2"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

type ProjectDescriptionGitReader struct {
	projectType domain.ProjectType
}

var _ usecase.ProjectDescriptionRemoteReader = (*ProjectDescriptionGitReader)(nil)

func NewProjectDescriptionGitReader(prjType domain.ProjectType) *ProjectDescriptionGitReader {
	return &ProjectDescriptionGitReader{
		projectType: prjType,
	}
}

func (c *ProjectDescriptionGitReader) Read(repositoryWebURL *url.URL) (*domain.ProjectDescription, error) {
	effectiveURL, err := url.JoinPath(repositoryWebURL.String(), "-", "raw", "main", "project.yaml")
	if os.Getenv("GENDOC_DEBUG") != "" {
		fmt.Printf("---- Try to read project.yaml from remote: %s\n", repositoryWebURL.String())
	}
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to construct path to project description: %w", err)
	}
	resp, err := http.Get(effectiveURL)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to HTTP GET project description for technical reasons: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to HTTP GET project description: %d, %s", resp.StatusCode, resp.Status)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read project description: %w", err)
	}
	var projectYAML ProjectYAML
	err = yaml.Unmarshal(body, &projectYAML)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to unmarshal project description: %w", err)
	}
	prjDescr, err := DomainRepresentationFromProjectYAML(&projectYAML, c.projectType)
	if err != nil {
		return prjDescr, err
	}
	return prjDescr, nil
}
