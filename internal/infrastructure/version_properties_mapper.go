//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"strings"

	"github.com/magiconair/properties"
	"zahnleiter.org/gendoc/internal/domain"
)

func DomainRepresentationFromVersionProperties(props *properties.Properties) (*domain.ProjectDescription, error) {
	versionStr := props.GetString("version", "")
	if strings.TrimSpace(versionStr) == "" {
		return &domain.ProjectDescription{}, fmt.Errorf("version property missing or empty")
	}
	var prjDescr domain.ProjectDescription
	err := SetStructuredVersionInformation(versionStr, &prjDescr)
	return &prjDescr, err
}
