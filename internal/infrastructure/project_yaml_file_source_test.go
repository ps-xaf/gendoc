//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

//-----------------------------------------------------------------------------
// Testing raw/pure YAML reading and unmarshalling.
//-----------------------------------------------------------------------------

func TestReadingYAMLFailsIfFileDoesNotExist(t *testing.T) {
	// GIVEN
	pathToNonExistingProjectYAML := "../test_files/doesnotextist.yaml"
	// WHEN
	_, err := infrastructure.ReadProjectYAML(pathToNonExistingProjectYAML)
	// THEN
	assert.EqualError(t, err, "failed to read project description: open ../test_files/doesnotextist.yaml: no such file or directory")
}

func TestValidYAMLCanBeRead(t *testing.T) {
	// GIVEN
	pathToValidProjectYAML := "../../test_files/simple_project.yaml"
	// WHEN
	prjYAML, err := infrastructure.ReadProjectYAML(pathToValidProjectYAML)
	// THEN
	require.NoError(t, err)

	assert.Equal(t, "smith-books/endcustomer/sales/webshop", prjYAML.ArtifactID)

	assert.Equal(t, "1.2.3-RC4", prjYAML.Version)

	assert.Equal(t, "service", prjYAML.ArtifactType)

	assert.Equal(t, "Smith Books - Web Shop", prjYAML.FriendlyName)
	assert.Equal(t, "The bookstore app.\nAccessible for customers via internet.\nAccessible for employees via intranet.\n", prjYAML.Description)

	assert.Equal(t, 2, len(prjYAML.DefinesInterfaces))
	assert.Equal(t, "Books", prjYAML.DefinesInterfaces[0].Application.Format)
	assert.Equal(t, "login/password", prjYAML.DefinesInterfaces[0].Application.Security)
	assert.Equal(t, "UTF-8", prjYAML.DefinesInterfaces[0].Communication.Encoding)
	assert.Equal(t, "JSON", prjYAML.DefinesInterfaces[0].Communication.Format)
	assert.Equal(t, "request/response", prjYAML.DefinesInterfaces[0].Communication.Pattern)
	assert.Equal(t, "sync", prjYAML.DefinesInterfaces[0].Communication.Style)
	assert.Equal(t, "Manage books (resources) via this REST API", prjYAML.DefinesInterfaces[0].Description)
	assert.Equal(t, "Books", prjYAML.DefinesInterfaces[0].FriendlyName)
	assert.Equal(t, "books", prjYAML.DefinesInterfaces[0].LocalID)
	assert.Equal(t, "models/books_api.yaml", prjYAML.DefinesInterfaces[0].SpecificationURL)
	assert.Equal(t, "http", prjYAML.DefinesInterfaces[0].Transport.Protocol)
	assert.Equal(t, "TLS 1.2", prjYAML.DefinesInterfaces[0].Transport.Security)
	assert.Equal(t, "Authors", prjYAML.DefinesInterfaces[1].Application.Format)
	assert.Equal(t, "login/password", prjYAML.DefinesInterfaces[1].Application.Security)
	assert.Equal(t, "UTF-8", prjYAML.DefinesInterfaces[1].Communication.Encoding)
	assert.Equal(t, "JSON", prjYAML.DefinesInterfaces[1].Communication.Format)
	assert.Equal(t, "request/response", prjYAML.DefinesInterfaces[1].Communication.Pattern)
	assert.Equal(t, "sync", prjYAML.DefinesInterfaces[1].Communication.Style)
	assert.Equal(t, "Manage authors (resources) via this REST API", prjYAML.DefinesInterfaces[1].Description)
	assert.Equal(t, "Authors", prjYAML.DefinesInterfaces[1].FriendlyName)
	assert.Equal(t, "authors", prjYAML.DefinesInterfaces[1].LocalID)
	assert.Equal(t, "models/authors_api.yaml", prjYAML.DefinesInterfaces[1].SpecificationURL)
	assert.Equal(t, "http", prjYAML.DefinesInterfaces[1].Transport.Protocol)
	assert.Equal(t, "TLS 1.2", prjYAML.DefinesInterfaces[1].Transport.Security)

	assert.Equal(t, 1, len(prjYAML.DeploymentVariants))
	assert.Equal(t, "bookstore", prjYAML.DeploymentVariants[0].LocalID)
	assert.Equal(t, "deployments/charts", prjYAML.DeploymentVariants[0].DeploymentDescriptorURL)
	assert.Equal(t, "Bookstore", prjYAML.DeploymentVariants[0].FriendlyName)
	assert.Equal(t, "Only one deployment variant required.", prjYAML.DeploymentVariants[0].Purpose)
	assert.Equal(t, 1, len(prjYAML.DeploymentVariants[0].ConsumesInterfaces))
	assert.Equal(t, "smith-books/endcustomer/sales/fulfillment", prjYAML.DeploymentVariants[0].ConsumesInterfaces[0].ArtifactRef)
	assert.Equal(t, "fulfillment", prjYAML.DeploymentVariants[0].ConsumesInterfaces[0].DeploymentRef)
	assert.Equal(t, "fulfillment-api", prjYAML.DeploymentVariants[0].ConsumesInterfaces[0].InterfaceRef)
	assert.Equal(t, 2, len(prjYAML.DeploymentVariants[0].ExposesInterfaces))
	assert.Equal(t, "books", prjYAML.DeploymentVariants[0].ExposesInterfaces[0].LocalRef)
	assert.Equal(t, "authors", prjYAML.DeploymentVariants[0].ExposesInterfaces[1].LocalRef)
}

//-----------------------------------------------------------------------------
// Only a limited set of tests on the reader as raw YAML reading and mapping is
// tested separately already.
//-----------------------------------------------------------------------------

func TestReadingFailsIfFileDoesNotExist(t *testing.T) {
	// GIVEN
	pathToNonExistingProjectYAML := "../test_files/doesnotextist.yaml"
	reader := infrastructure.NewProjectYAMLSource(pathToNonExistingProjectYAML, domain.ProjectTypeEnterprise)
	// WHEN
	_, err := reader.Read()
	// THEN
	assert.EqualError(t, err, "failed to read project description: open ../test_files/doesnotextist.yaml: no such file or directory")
}

func TestValidYAMLCanBeReadAndMappedToDomain(t *testing.T) {
	// GIVEN
	pathToValidProjectYAML := "../../test_files/simple_project.yaml"
	reader := infrastructure.NewProjectYAMLSource(pathToValidProjectYAML, domain.ProjectTypeEnterprise)
	// WHEN
	prjDescr, err := reader.Read()
	// THEN
	require.NoError(t, err)

	assert.Equal(t, domain.SystemName("smith-books"), prjDescr.ArtifactID.System)
	assert.Equal(t, domain.DomainName("endcustomer"), prjDescr.ArtifactID.Domain)
	assert.Equal(t, domain.SubdomainName("sales"), prjDescr.ArtifactID.Subdomain)
	assert.Equal(t, domain.ArtifactName("webshop"), prjDescr.ArtifactID.Artifact)

	assert.Equal(t, domain.ArtifactTypeService, prjDescr.ArtifactType)

	assert.Equal(t, 2, len(prjDescr.DefinedInterfaces))
	assert.Equal(t, domain.AppFormat("Books"), prjDescr.DefinedInterfaces[0].Application.Format)
	assert.Equal(t, domain.ApplicationSecurity("login/password"), prjDescr.DefinedInterfaces[0].Application.Security)
	assert.Equal(t, domain.CommsEncoding("UTF-8"), prjDescr.DefinedInterfaces[0].Communication.Encoding)
	assert.Equal(t, domain.CommsFormat("JSON"), prjDescr.DefinedInterfaces[0].Communication.Format)
	assert.Equal(t, domain.CommsPattern("request/response"), prjDescr.DefinedInterfaces[0].Communication.Pattern)
	assert.Equal(t, domain.CommsStyle("sync"), prjDescr.DefinedInterfaces[0].Communication.Style)
	assert.Equal(t, domain.InterfaceDescription("Manage books (resources) via this REST API"), prjDescr.DefinedInterfaces[0].Description)
	assert.Equal(t, domain.InterfaceName("Books"), prjDescr.DefinedInterfaces[0].FriendlyName)
	assert.Equal(t, domain.LocalInterfaceID("books"), prjDescr.DefinedInterfaces[0].LocalID)
	URL, err := url.Parse("models/books_api.yaml")
	require.NoError(t, err)
	assert.Equal(t, domain.SpecificationURL(*URL), prjDescr.DefinedInterfaces[0].SpecificationURL)
	assert.Equal(t, domain.TransportProtocol("http"), prjDescr.DefinedInterfaces[0].Transport.Protocol)
	assert.Equal(t, domain.TransportSecurity("TLS 1.2"), prjDescr.DefinedInterfaces[0].Transport.Security)
	assert.Equal(t, domain.AppFormat("Authors"), prjDescr.DefinedInterfaces[1].Application.Format)
	assert.Equal(t, domain.ApplicationSecurity("login/password"), prjDescr.DefinedInterfaces[1].Application.Security)
	assert.Equal(t, domain.CommsEncoding("UTF-8"), prjDescr.DefinedInterfaces[1].Communication.Encoding)
	assert.Equal(t, domain.CommsFormat("JSON"), prjDescr.DefinedInterfaces[1].Communication.Format)
	assert.Equal(t, domain.CommsPattern("request/response"), prjDescr.DefinedInterfaces[1].Communication.Pattern)
	assert.Equal(t, domain.CommsStyle("sync"), prjDescr.DefinedInterfaces[1].Communication.Style)
	assert.Equal(t, domain.InterfaceDescription("Manage authors (resources) via this REST API"), prjDescr.DefinedInterfaces[1].Description)
	assert.Equal(t, domain.InterfaceName("Authors"), prjDescr.DefinedInterfaces[1].FriendlyName)
	assert.Equal(t, domain.LocalInterfaceID("authors"), prjDescr.DefinedInterfaces[1].LocalID)
	URL, err = url.Parse("models/authors_api.yaml")
	require.NoError(t, err)
	assert.Equal(t, domain.SpecificationURL(*URL), prjDescr.DefinedInterfaces[1].SpecificationURL)
	assert.Equal(t, domain.TransportProtocol("http"), prjDescr.DefinedInterfaces[1].Transport.Protocol)
	assert.Equal(t, domain.TransportSecurity("TLS 1.2"), prjDescr.DefinedInterfaces[1].Transport.Security)

	assert.Equal(t, 1, len(prjDescr.DeploymentVariants))
	URL, err = url.Parse("deployments/charts")
	require.NoError(t, err)
	assert.Equal(t, domain.DeploymentDescriptorURL(*URL), prjDescr.DeploymentVariants[0].DeploymentDescriptorURL)
	assert.Equal(t, domain.VariantName("Bookstore"), prjDescr.DeploymentVariants[0].FriendlyName)
	assert.Equal(t, 1, len(prjDescr.DeploymentVariants[0].ConsumedInterfaces))
	consumedIface := prjDescr.DeploymentVariants[0].ConsumedInterfaces[0].(*domain.ConsumedInternalInterface)
	assert.Equal(t, domain.ArtifactName("fulfillment"), consumedIface.ArtifactRef.Artifact)
	assert.Equal(t, domain.DomainName("endcustomer"), consumedIface.ArtifactRef.Domain)
	assert.Equal(t, domain.SubdomainName("sales"), consumedIface.ArtifactRef.Subdomain)
	assert.Equal(t, domain.SystemName("smith-books"), consumedIface.ArtifactRef.System)
	assert.Equal(t, domain.DeploymentLocalID("fulfillment"), consumedIface.DeploymentRef)
	assert.Equal(t, domain.LocalInterfaceID("fulfillment-api"), consumedIface.InterfaceRef)
	assert.Equal(t, 2, len(prjDescr.DeploymentVariants[0].ExposedInterfaces))
	assert.Equal(t, domain.LocalInterfaceID("books"), prjDescr.DeploymentVariants[0].ExposedInterfaces[0].LocalRef)
	assert.Equal(t, domain.LocalInterfaceID("authors"), prjDescr.DeploymentVariants[0].ExposedInterfaces[1].LocalRef)
	assert.Equal(t, domain.DeploymentLocalID("bookstore"), prjDescr.DeploymentVariants[0].LocalID)
	assert.Equal(t, domain.PurposeOfVariant("Only one deployment variant required."), prjDescr.DeploymentVariants[0].Purpose)
}
