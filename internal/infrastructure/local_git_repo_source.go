//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	giturls "github.com/whilp/git-urls"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

type LocalGitRepoSource struct {
	path         string
	projectType  domain.ProjectType
	atrifactType domain.ArtifactType
}

var _ usecase.ProjectDescriptionSource = (*LocalGitRepoSource)(nil)

func NewLocalGitRepoSource(path string, projectType domain.ProjectType) *LocalGitRepoSource {
	return &LocalGitRepoSource{path: path, projectType: projectType}
}

func (r *LocalGitRepoSource) Read() (*domain.ProjectDescription, error) {
	gitRepo, err := git.PlainOpen(r.path)
	if err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read project description from local Git repository: %w", err)
	}
	var infoFromGit domain.ProjectDescription
	if err = readArtifactNameFromOriginURL(gitRepo, r.projectType, r.atrifactType, &infoFromGit); err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read project description from local Git repository: %w", err)
	}
	if err = readVersionTags(gitRepo, &infoFromGit); err != nil {
		return &domain.ProjectDescription{}, fmt.Errorf("failed to read project description from local Git repository: %w", err)
	}
	return &infoFromGit, nil
}

func readArtifactNameFromOriginURL(gitRepo *git.Repository, prjType domain.ProjectType, artifactType domain.ArtifactType, prjDescr *domain.ProjectDescription) error {
	localConfig, err := gitRepo.Config()
	if err != nil {
		return err
	}
	if remoteCfg, found := localConfig.Remotes["origin"]; found {
		if len(remoteCfg.URLs) < 1 {
			return fmt.Errorf("missing remote URL for origin")
		}
		originURL, err := giturls.Parse(remoteCfg.URLs[0])
		if err != nil {
			return fmt.Errorf("failed to map origin URL: %w", err)
		}
		prjDescr.SCMURL = domain.SCMURL(*originURL)
		return SetArtifactNameFromOriginURL(originURL.Path, prjType, artifactType, &prjDescr.ArtifactID)
	}
	return fmt.Errorf("no origin configured")
}

func readVersionTags(gitRepo *git.Repository, prjDescr *domain.ProjectDescription) error {
	tags, err := gitRepo.Tags()
	if err != nil {
		return err
	}
	return tags.ForEach(func(ref *plumbing.Reference) error {
		MapTag(ref, prjDescr)
		return nil
	})
}
