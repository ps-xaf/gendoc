//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

type ProjectYAML struct {
	Organization       string                `yaml:"organization,omitempty"`
	Team               string                `yaml:"team,omitempty"`
	ArtifactID         string                `yaml:"ID,omitempty"`
	ArtifactType       string                `yaml:"type"`              // Service, job, library, ...
	Version            string                `yaml:"version,omitempty"` // Overwrite for version derived from Git tags. Iself overwritten by version.properties
	FriendlyName       string                `yaml:"friendly name"`
	Description        string                `yaml:"description"`
	DefinesInterfaces  []InterfaceDefinition `yaml:"defines interfaces"`
	DeploymentVariants []DeploymentVariant   `yaml:"deployment variants"`
	LandingPage        LandingPage           `yaml:"landing page"`
}

type InterfaceDefinition struct {
	LocalID          string            `yaml:"local ID"` // Unique only within this file
	FriendlyName     string            `yaml:"friendly name"`
	Description      string            `yaml:"description"`
	SpecificationURL string            `yaml:"specification URL"` // An openAPI (Swagger) file or similar
	Application      ApplicationSpec   `yaml:"application"`
	Communication    CommunicationSpec `yaml:"communication"`
	Transport        TransportSpec     `yaml:"transport"`
}
type ApplicationSpec struct {
	Format   string `yaml:"format"` // Functional (not technical) format
	Security string `yaml:"security"`
}
type CommunicationSpec struct {
	Pattern  string `yaml:"pattern"`  // Req/Resp, Pub/Sub, ...
	Style    string `yaml:"style"`    // sync, async, ...
	Format   string `yaml:"format"`   // JSON, XML, ...
	Encoding string `yaml:"encoding"` // UTF-8, ...
}
type TransportSpec struct {
	Protocol string `yaml:"protocol"` // http, amqp, mqtt, ...
	Security string `yaml:"security"` // tls 1.2, ...
}

type DeploymentVariant struct {
	LocalID                 string              `yaml:"local ID"` // Unique only within this file
	FriendlyName            string              `yaml:"friendly name"`
	Purpose                 string              `yaml:"purpose"`
	DeploymentDescriptorURL string              `yaml:"deployment descriptor URL"`
	ExposesInterfaces       []ExposedInterface  `yaml:"exposes interfaces"`
	ConsumesInterfaces      []ConsumedInterface `yaml:"consumes interfaces"`
}

type ExposedInterface struct {
	LocalRef string `yaml:"local ref"` // Within this file
}

// Reference to other Git repo or external system (informative only)
type ConsumedInterface struct {
	ArtifactRef   string `yaml:"artifact ref"`
	DeploymentRef string `yaml:"deployment ref"`
	InterfaceRef  string `yaml:"interface ref"`
	SCMURL        string `yaml:"SCM URL"`
	// This is meant for external systems, where we do not know many details.
	// These fields are mutually exclusive with the fields above
	Organization string `yaml:"organization"`
	System       string `yaml:"system"`
	Interface    string `yaml:"interface"`
}

func (i *ConsumedInterface) IsInternalInterface() bool {
	return (i.ArtifactRef != "" || i.DeploymentRef != "" || i.InterfaceRef != "" || i.SCMURL != "") &&
		i.Organization == "" && i.System == "" && i.Interface == ""
}

func (i *ConsumedInterface) IsExternalInterface() bool {
	return i.ArtifactRef == "" && i.DeploymentRef == "" && i.InterfaceRef == "" && i.SCMURL == "" &&
		(i.Organization != "" || i.System != "" || i.Interface != "")
}

type LandingPage struct {
	Title       string    `yaml:"title"`
	Subtitle    string    `yaml:"subtitle"`
	Description string    `yaml:"description"`
	Features    []Feature `yaml:"features"`
}

type Feature struct {
	Title       string `yaml:"title"`
	Description string `yaml:"description"`
}
