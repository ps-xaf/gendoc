//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestCannotMapEmptyArtifactName(t *testing.T) {
	// GIVEN
	emptyUnstructuredArtifactName := ""
	var structuredArtifactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		emptyUnstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &structuredArtifactName)
	// THEN
	assert.EqualError(t, err, "artifact ID missing")
}

func TestUnstructuredArtifactNameMappedCorrectly(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "myrepo"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("myrepo"), artefactName.System)
	assert.Equal(t, domain.DomainName(""), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName(""), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName(""), artefactName.Artifact)
}

func TestUnstructuredArtifactNameAndDomainMappedCorrectly(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "myrepo/mygroup"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeDomain, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("myrepo"), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName(""), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName(""), artefactName.Artifact)
}

func TestUnstructuredArtifactNameAndDomainAndSubDomainMappedCorrectly(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "mygroup/mysubgroup/myrepo"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSubdomain, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("mygroup"), artefactName.System)
	assert.Equal(t, domain.DomainName("mysubgroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName("myrepo"), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName(""), artefactName.Artifact)
}

func TestUnstructuredArtifactNameAndDomainAndSubDomainAndSystemMappedCorrectly(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "myaccount/mygroup/mysubroup/myrepo"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &artefactName)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.SystemName("myaccount"), artefactName.System)
	assert.Equal(t, domain.DomainName("mygroup"), artefactName.Domain)
	assert.Equal(t, domain.SubdomainName("mysubroup"), artefactName.Subdomain)
	assert.Equal(t, domain.ArtifactName("myrepo"), artefactName.Artifact)
}

func TestCannotMapUnstructuredArtifactNameThatIsNestedTooDeeply(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "xxx/myaccount/mygroup/mysubroup/myrepo"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &artefactName)
	// THEN
	assert.EqualError(t, err, "artifact ID nested too deeply")
}

func TestUnstructuredArtifactNamePrefixedWithSlash(t *testing.T) {
	// GIVEN
	unstructuredArtifactName := "/myaccount/mygroup/mysubroup/myrepo"
	var artefactName domain.FullyQualifiedArtifactName
	// WHEN
	err := infrastructure.SetStructuredArtifactName(
		unstructuredArtifactName, domain.ProjectTypeEnterprise, domain.ArtifactTypeSystem, &artefactName)
	// THEN
	assert.EqualError(t, err, `illegal slash "/" at beginning of artifact ID`)
}

// TODO more test cases for single project...
