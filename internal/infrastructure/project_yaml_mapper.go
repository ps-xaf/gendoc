//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"net/url"

	"zahnleiter.org/gendoc/internal/domain"
)

// Mapper first instantiates the resulting domain object. It then calls several
// "set" functions that are mapping  portions of project.yaml to the domain and
// set the corresponding values within the resulting domain object.
func DomainRepresentationFromProjectYAML(prjYAML *ProjectYAML, prjType domain.ProjectType) (*domain.ProjectDescription, error) {
	var result domain.ProjectDescription

	artiType, err := DomainRepresentationOfArtifactType(prjYAML.ArtifactType)
	if err != nil {
		return &domain.ProjectDescription{}, err
	}
	result.ArtifactType = artiType

	result.Organization = domain.ProjectOrganizationName(prjYAML.Organization)

	result.Team = domain.TeamName(prjYAML.Team)

	err = SetStructuredArtifactName(prjYAML.ArtifactID, prjType, artiType, &result.ArtifactID)
	if err != nil {
		return &domain.ProjectDescription{}, err
	}
	// Version is optional
	if prjYAML.Version != "" {
		err := SetStructuredVersionInformation(prjYAML.Version, &result)
		if err != nil {
			return &domain.ProjectDescription{}, err
		}
	}
	result.Description = domain.ArtifactDescription(prjYAML.Description)
	result.FriendlyName = domain.HumanReadableArtifactName(prjYAML.FriendlyName)
	err = setInterfacesDefined(&prjYAML.DefinesInterfaces, &result)
	if err != nil {
		return &domain.ProjectDescription{}, err
	}
	err = setDeploymentVariants(&prjYAML.DeploymentVariants, prjType, artiType, &result)
	if err != nil {
		return &domain.ProjectDescription{}, err
	}
	setLandingPage(&prjYAML.LandingPage, &result)
	return &result, nil
}

func setInterfacesDefined(interfaces *[]InterfaceDefinition, mapped *domain.ProjectDescription) error {
	mapped.DefinedInterfaces = make([]domain.InterfaceDefinition, len(*interfaces))
	for i, interfaceDef := range *interfaces {
		err := setInterfaceDef(&interfaceDef, &mapped.DefinedInterfaces[i])
		if err != nil {
			return fmt.Errorf("failed to map interface defined by artifact: %w", err)
		}
	}
	return nil
}

func setInterfaceDef(interfaceDef *InterfaceDefinition, mapped *domain.InterfaceDefinition) error {
	mapped.Application.Format = domain.AppFormat(interfaceDef.Application.Format)
	mapped.Application.Security = domain.ApplicationSecurity(interfaceDef.Application.Security)
	mapped.Communication.Encoding = domain.CommsEncoding(interfaceDef.Communication.Encoding)
	mapped.Communication.Format = domain.CommsFormat(interfaceDef.Communication.Format)
	mapped.Communication.Pattern = domain.CommsPattern(interfaceDef.Communication.Pattern)
	mapped.Communication.Style = domain.CommsStyle(interfaceDef.Communication.Style)
	mapped.Description = domain.InterfaceDescription(interfaceDef.Description)
	mapped.FriendlyName = domain.InterfaceName(interfaceDef.FriendlyName)
	mapped.LocalID = domain.LocalInterfaceID(interfaceDef.LocalID)
	specURL, err := url.Parse(interfaceDef.SpecificationURL)
	if err != nil {
		return err
	}
	mapped.SpecificationURL = domain.SpecificationURL(*specURL)
	mapped.Transport.Protocol = domain.TransportProtocol(interfaceDef.Transport.Protocol)
	mapped.Transport.Security = domain.TransportSecurity(interfaceDef.Transport.Security)
	return nil
}

func setDeploymentVariants(variants *[]DeploymentVariant, prjType domain.ProjectType, artifactType domain.ArtifactType, mapped *domain.ProjectDescription) error {
	mapped.DeploymentVariants = make([]domain.DeploymentVariant, len(*variants))
	for i, variant := range *variants {
		err := setDeploymentVariant(&variant, prjType, artifactType, &mapped.DeploymentVariants[i])
		if err != nil {
			return fmt.Errorf("failed to map deployment variant: %w", err)
		}
	}
	return nil
}

func setDeploymentVariant(variant *DeploymentVariant, prjType domain.ProjectType, artifactType domain.ArtifactType, mapped *domain.DeploymentVariant) error {
	chartURL, err := url.Parse(variant.DeploymentDescriptorURL)
	if err != nil {
		return err
	}
	mapped.DeploymentDescriptorURL = domain.DeploymentDescriptorURL(*chartURL)
	mapped.FriendlyName = domain.VariantName(variant.FriendlyName)
	mapped.LocalID = domain.DeploymentLocalID(variant.LocalID)
	mapped.Purpose = domain.PurposeOfVariant(variant.Purpose)
	setExposedInterfaces(&variant.ExposesInterfaces, mapped)
	err = setConsumedInterfaces(&variant.ConsumesInterfaces, prjType, artifactType, mapped)
	if err != nil {
		return err
	}
	return nil
}

func setExposedInterfaces(providedInterfaces *[]ExposedInterface, mapped *domain.DeploymentVariant) {
	mapped.ExposedInterfaces = make([]domain.ExposedInterface, len(*providedInterfaces))
	for i, exposedInterface := range *providedInterfaces {
		mapped.ExposedInterfaces[i].LocalRef = domain.LocalInterfaceID(exposedInterface.LocalRef)
	}
}

func setConsumedInterfaces(consumedInterfaces *[]ConsumedInterface, prjType domain.ProjectType, artifactType domain.ArtifactType, mapped *domain.DeploymentVariant) error {
	mapped.ConsumedInterfaces = make([]domain.ConsumedInterface, len(*consumedInterfaces))
	for i, consumedIface := range *consumedInterfaces {
		iface, err := consumedInterface(&consumedIface, prjType, artifactType)
		if err != nil {
			return err
		}
		mapped.ConsumedInterfaces[i] = iface
	}
	return nil
}

func consumedInterface(consumedInterface *ConsumedInterface, prjType domain.ProjectType, artifactType domain.ArtifactType) (domain.ConsumedInterface, error) {
	switch {
	case consumedInterface.IsExternalInterface():
		return consumedExternalInterface(consumedInterface, prjType, artifactType)
	case consumedInterface.IsInternalInterface():
		return consumedInternalInterface(consumedInterface, prjType, artifactType)
	default:
		return nil, fmt.Errorf("consumed interface has to be either external or internal but cannot be both")
	}
}

func consumedExternalInterface(consumedInterface *ConsumedInterface, prjType domain.ProjectType, artifactType domain.ArtifactType) (domain.ConsumedInterface, error) {
	return &domain.ConsumedExternalInterface{
		Organization: domain.ExternalOrganizationName(consumedInterface.Organization),
		System:       domain.ExternalSystemName(consumedInterface.System),
		Interface:    domain.ExternalInterfaceName(consumedInterface.Interface),
	}, nil
}

func consumedInternalInterface(consumedInterface *ConsumedInterface, prjType domain.ProjectType, artifactType domain.ArtifactType) (domain.ConsumedInterface, error) {
	SourceCodeManagementURL, err := url.Parse(consumedInterface.SCMURL)
	if err != nil {
		return &domain.ConsumedInternalInterface{}, fmt.Errorf("consumed interface has invalid SCM URL: %w ", err)
	}
	mapped := domain.ConsumedInternalInterface{}
	err = SetStructuredArtifactName(consumedInterface.ArtifactRef, prjType, artifactType, &mapped.ArtifactRef)
	if err != nil {
		return &domain.ConsumedInternalInterface{}, fmt.Errorf("failed to reference artifact that provides interface: %w", err)
	}
	mapped.DeploymentRef = domain.DeploymentLocalID(consumedInterface.DeploymentRef)
	mapped.InterfaceRef = domain.LocalInterfaceID(consumedInterface.InterfaceRef)
	mapped.SCMWebURL = domain.InterfacesSCMWebURL(*SourceCodeManagementURL)
	return &mapped, nil
}

func setLandingPage(landingPage *LandingPage, mapped *domain.ProjectDescription) {
	mapped.LandingPage.Title = domain.LandingPageTitle(landingPage.Title)
	mapped.LandingPage.Subtitle = domain.LandingPageSubtitle(landingPage.Subtitle)
	mapped.LandingPage.Description = domain.LandingPageDescription(landingPage.Description)
	setFeatures(&landingPage.Features, &mapped.LandingPage)
}

func setFeatures(features *[]Feature, mapped *domain.LandingPage) {
	mapped.Features = make([]domain.LandingPageFeature, len(*features))
	for i, feature := range *features {
		mapped.Features[i].Description = domain.FeatureDescription(feature.Description)
		mapped.Features[i].Title = domain.FeatureTitle(feature.Title)
	}
}
