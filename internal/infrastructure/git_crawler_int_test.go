//go:build int_test
// +build int_test

//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

// This test is only active in case the int_test Build Tag is set. In this case
// a valid access token is required to access the GitLab API. That token should
// only have read access. It is passed in via the GIT_LAB_ACCESS_TOKEN environ-
// ment variable.
func TestRetrieveAllMyProjectsFromGitLab(t *testing.T) {
	// GIVEN
	projectURL, err := url.Parse("https://gitlab.com/hzahnlei/gendoc")
	require.NoError(t, err)
	accessToken := os.Getenv("GIT_LAB_ACCESS_TOKEN")
	crawler := infrastructure.NewGitLabCrawler(projectURL, accessToken)
	// WHEN
	projects, err := crawler.InfosOfAllProjects()
	// THEN
	require.NoError(t, err)
	assert.True(t, len(*projects) > 0)
}
