//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

// TODO restructure test so that function StructuredVersionInformation() gets tested in detail.
// Function SetStructuredVersionInformation() should only be tested 2 cases, success and failure

func TestEmptyUnstructuredVersion(t *testing.T) {
	// GIVEN
	emptyUnstructuredVer := ""
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(emptyUnstructuredVer, &prjDescr)
	// THEN
	assert.EqualError(t, err, "version missing")
}

func TestIncompleteInstructuredVersion(t *testing.T) {
	// GIVEN
	incompleteUnstructuredVer := "1.0"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(incompleteUnstructuredVer, &prjDescr)
	// THEN
	assert.EqualError(t, err, "no semantic versioning, expect major.minor.patch and optional -addendum")
}

func TestOvercompleteUnstructuredVersion(t *testing.T) {
	// GIVEN
	overcompleteUnstructuredVer := "1.2.3.4"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(overcompleteUnstructuredVer, &prjDescr)
	// THEN
	assert.EqualError(t, err, "no semantic versioning, expect major.minor.patch and optional -addendum")
}

func TestValidUnstructuredVersion(t *testing.T) {
	// GIVEN
	validUnstructuredVer := "1.2.3"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(validUnstructuredVer, &prjDescr)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumUndefined, prjDescr.Version.Addendum.Type)
}

func TestValidUnstructuredVersionWithRC(t *testing.T) {
	// GIVEN
	validUnstructuredVer := "1.2.3-RC4"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(validUnstructuredVer, &prjDescr)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumReleaseCandidate, prjDescr.Version.Addendum.Type)
	assert.Equal(t, domain.RCVersion(4), prjDescr.Version.Addendum.RCVersion)
}

func TestValidUnstructuredVersionWithSnapshot(t *testing.T) {
	// GIVEN
	validUnstructuredVer := "1.2.3-SNAPSHOT"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(validUnstructuredVer, &prjDescr)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumSnapshot, prjDescr.Version.Addendum.Type)
	assert.Equal(t, domain.RCVersion(0), prjDescr.Version.Addendum.RCVersion)
}

func TestValidUnstructuredVersionWithRelease(t *testing.T) {
	// GIVEN
	validUnstructuredVer := "1.2.3-RELEASE"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(validUnstructuredVer, &prjDescr)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.MajorVersion(1), prjDescr.Version.Major)
	assert.Equal(t, domain.MinorVersion(2), prjDescr.Version.Minor)
	assert.Equal(t, domain.PatchVersion(3), prjDescr.Version.Patch)
	assert.Equal(t, domain.AddendumRelease, prjDescr.Version.Addendum.Type)
	assert.Equal(t, domain.RCVersion(0), prjDescr.Version.Addendum.RCVersion)
}

func TestAddensumMustNotBeLeftOutFromUnstructuredRepresentationAfterDash(t *testing.T) {
	// GIVEN
	validUnstructuredVer := "1.2.3-"
	var prjDescr domain.ProjectDescription
	// WHEN
	err := infrastructure.SetStructuredVersionInformation(validUnstructuredVer, &prjDescr)
	// THEN
	assert.EqualError(t, err, "addendum to version missing after dash")
}
