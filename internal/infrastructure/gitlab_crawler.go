//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"net/url"
	"os"
	"strconv"

	"github.com/xanzy/go-gitlab"
	"zahnleiter.org/gendoc/internal/usecase"
)

type GitLabCrawler struct {
	scmWebURL   url.URL
	accessToken string
	crawlOwned  bool
}

var _ usecase.SCMCrawler = (*GitLabCrawler)(nil)

// TODO in case of "single" projects we should restrict the number of repos crawled.
// actually it only makes sense to retrive additional GitLab data for THE project.
// all project.yamls and all GitLab infos for all other repos are irrelevant.
func NewGitLabCrawler(scmURL *url.URL, accessToken string, crawlOwned bool) *GitLabCrawler {
	return &GitLabCrawler{
		scmWebURL:   *scmURL,
		accessToken: accessToken,
		crawlOwned:  crawlOwned,
	}
}

func (c *GitLabCrawler) InfosOfAllProjects() (*[]*usecase.SCMProjectDescriptor, error) {
	git, err := c.connectToGitLab()
	if err != nil {
		return &[]*usecase.SCMProjectDescriptor{},
			fmt.Errorf("failed to connect to remote GitLab API server: %w", err)
	}
	projectList := make([]*usecase.SCMProjectDescriptor, 0)
	if err := ReadAllProjectsFromGitLab(git, &projectList, c.crawlOwned); err != nil {
		return &[]*usecase.SCMProjectDescriptor{},
			fmt.Errorf("failed to read project information from remote GitLab API server: %w", err)
	}
	return &projectList, nil
}

func (c *GitLabCrawler) connectToGitLab() (*gitlab.Client, error) {
	baseURL := SCMBaseURL(c.scmWebURL)
	return gitlab.NewClient(c.accessToken, gitlab.WithBaseURL(baseURL))
}

func SCMBaseURL(scmWebURL url.URL) string {
	baseURL := url.URL{
		Scheme: scmWebURL.Scheme,
		Host:   scmWebURL.Host,
	}
	return baseURL.String()
}

func ReadAllProjectsFromGitLab(git *gitlab.Client, projectInfos *[]*usecase.SCMProjectDescriptor, crawlOwned bool) error {
	if os.Getenv("GENDOC_DEBUG") != "" {
		fmt.Printf("---- Trying to read all project infos from remote\n")
	}
	searchOptions := ListAllMyProjects(crawlOwned)
	for {
		projects, resp, err := git.Projects.ListProjects(searchOptions)
		if err != nil {
			return fmt.Errorf("failed to read all projects from GitLab API server: %w", err)
		}
		err = appendAllProjects(projects, projectInfos)
		if err != nil {
			return err
		}
		if resp.NextPage == 0 {
			break
		}
		searchOptions.Page = resp.NextPage
	}
	return nil
}

func appendAllProjects(projects []*gitlab.Project, projectInfos *[]*usecase.SCMProjectDescriptor) error {
	for _, project := range projects {
		webURL, err := url.Parse(project.WebURL)
		if err != nil {
			return fmt.Errorf("found project with invalid web URL: %w", err)
		}
		prj := usecase.SCMProjectDescriptor{
			ID:        strconv.Itoa(project.ID),
			SCMWebURL: *webURL,
			Topics:    project.Topics,
		}
		if os.Getenv("GENDOC_DEBUG") != "" {
			fmt.Printf("---- Read project info from remote: %s\n", project.WebURL)
		}
		*projectInfos = append(*projectInfos, &prj)
	}
	return nil
}

func ListAllMyProjects(crawlOwned bool) *gitlab.ListProjectsOptions {
	if os.Getenv("GENDOC_DEBUG") != "" {
		if crawlOwned {
			fmt.Printf("---- Crawl only owned repos\n")
		} else {
			fmt.Printf("---- Crawl all repos\n")
		}
	}
	return &gitlab.ListProjectsOptions{
		Owned:   gitlab.Bool(crawlOwned), // Crawling owned repos requires access token
		Simple:  gitlab.Bool(true),
		Sort:    gitlab.String("asc"),
		OrderBy: gitlab.String("created_at"),
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 100,
		},
	}
}
