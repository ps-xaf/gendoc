//go:build int_test
// +build int_test

//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestReadOwnProjectYAMLFromOwnGitLabRepo(t *testing.T) {
	// GIVEN
	scmURL, err := url.Parse("https://gitlab.com/hzahnlei/gendoc")
	require.NoError(t, err)
	gitlab := infrastructure.NewProjectDescriptionGitReader(domain.ProjectTypeEnterprise)
	// WHEN
	prjDescr, err := gitlab.Read(scmURL)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.HumanReadableArtifactName("GenDoc"), prjDescr.FriendlyName)
}
