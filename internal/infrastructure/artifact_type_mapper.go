//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

func DomainRepresentationOfArtifactType(artifact string) (domain.ArtifactType, error) {
	normalized := strings.ToLower(artifact)
	if normalized == "application" || normalized == "app" {
		return domain.ArtifactTypeApplication, nil
	} else if normalized == "domain" {
		return domain.ArtifactTypeDomain, nil
	} else if normalized == "library" || normalized == "lib" {
		return domain.ArtifactTypeLibrary, nil
	} else if normalized == "service" {
		return domain.ArtifactTypeService, nil
	} else if normalized == "subdomain" || normalized == "sub-domain" {
		return domain.ArtifactTypeSubdomain, nil
	} else if normalized == "system" {
		return domain.ArtifactTypeSystem, nil
	} else if normalized == "tool" {
		return domain.ArtifactTypeTool, nil
	} else {
		return 0, fmt.Errorf("unknown artifact type")
	}
}
