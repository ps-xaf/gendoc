//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
)

func TestMappingArtifactTypeApplication(t *testing.T) {
	// GIVEN
	name := "application"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeApplication, value)
}

func TestMappingArtifactTypeDomain(t *testing.T) {
	// GIVEN
	name := "domain"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeDomain, value)
}

func TestMappingArtifactTypeLibrary(t *testing.T) {
	// GIVEN
	name := "library"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeLibrary, value)
}

func TestMappingArtifactTypeService(t *testing.T) {
	// GIVEN
	name := "service"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeService, value)
}

func TestMappingArtifactTypeSubdomain(t *testing.T) {
	// GIVEN
	name := "subdomain"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeSubdomain, value)
}

func TestMappingArtifactTypeSystem(t *testing.T) {
	// GIVEN
	name := "system"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeSystem, value)
}

func TestMappingArtifactTypeTool(t *testing.T) {
	// GIVEN
	name := "Tool"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeTool, value)
}

func TestMappingArtifactTypeDoesNotCareForCase(t *testing.T) {
	// GIVEN
	name := "dOmAiN"
	// WHEN
	value, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, domain.ArtifactTypeDomain, value)
}

func TestMappingArtifactTypeDoesNotRecognizeArbitraryStrings(t *testing.T) {
	// GIVEN
	name := "some unknown artifact type"
	// WHEN
	_, err := infrastructure.DomainRepresentationOfArtifactType(name)
	// THEN
	require.EqualError(t, err, "unknown artifact type")
}
