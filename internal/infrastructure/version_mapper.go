//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package infrastructure

import (
	"fmt"
	"strconv"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
)

func SetStructuredVersionInformation(unstructured string, prjDescr *domain.ProjectDescription) error {
	structured, err := StructuredVersionInformation(unstructured)
	if err != nil {
		return err
	}
	prjDescr.Version = structured
	return nil
}

func StructuredVersionInformation(unstructured string) (domain.SemanticVersion, error) {
	var structured domain.SemanticVersion
	if strings.TrimSpace(unstructured) == "" {
		return domain.SemanticVersion{}, fmt.Errorf("version missing")
	}
	parts := strings.Split(unstructured, ".")
	if len(parts) != 3 {
		return domain.SemanticVersion{}, fmt.Errorf("no semantic versioning, expect major.minor.patch and optional -addendum")
	}
	major, err := strconv.ParseInt(parts[0], 10, 0) //TODO Uint
	if err != nil || major < 0 {
		return domain.SemanticVersion{}, fmt.Errorf("invalid major version")
	}
	structured.Major = domain.MajorVersion(major)
	minor, err := strconv.ParseInt(parts[1], 10, 0)
	if err != nil || minor < 0 {
		return domain.SemanticVersion{}, fmt.Errorf("invalid minor version")
	}
	structured.Minor = domain.MinorVersion(minor)
	patchAndAddendum := strings.Split(parts[2], "-")
	if len(patchAndAddendum) < 1 {
		return domain.SemanticVersion{}, fmt.Errorf("patch version missing")
	}
	if len(patchAndAddendum) > 2 {
		return domain.SemanticVersion{}, fmt.Errorf("can have only patch version and addendum but no more")
	}
	patch, err := strconv.ParseInt(patchAndAddendum[0], 10, 0)
	if err != nil || patch < 0 {
		return domain.SemanticVersion{}, fmt.Errorf("invalid patch version")
	}
	structured.Patch = domain.PatchVersion(patch)
	if len(patchAndAddendum) > 1 {
		if strings.TrimSpace(patchAndAddendum[1]) == "" {
			return domain.SemanticVersion{}, fmt.Errorf("addendum to version missing after dash")
		}
		addendum := strings.ToUpper(patchAndAddendum[1])
		if addendum == "RELEASE" {
			structured.Addendum.Type = domain.AddendumRelease
			structured.Addendum.RCVersion = 0
		} else if addendum == "SNAPSHOT" {
			structured.Addendum.Type = domain.AddendumSnapshot
			structured.Addendum.RCVersion = 0
		} else if strings.HasPrefix(addendum, "RC") {
			rank, err := strconv.ParseUint(strings.ReplaceAll(addendum, "RC", ""), 10, 0)
			if err != nil {
				return domain.SemanticVersion{}, fmt.Errorf("invalid release candidate numbering: %w", err)
			}
			structured.Addendum.Type = domain.AddendumReleaseCandidate
			structured.Addendum.RCVersion = domain.RCVersion(rank)
		} else {
			return domain.SemanticVersion{}, fmt.Errorf("failed to recognize addendum")
		}
	}
	return structured, nil
}
