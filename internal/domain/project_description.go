//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import (
	"net/url"
	"strings"
)

// Version taken from Git tags is preferred. However, this automatically deter-
// mined  version can be  overwritten in  version.properties and  project.yaml,
// where version.properties wins over project.yaml.
type ProjectDescription struct {
	ProjectType        ProjectType
	Organization       ProjectOrganizationName
	Team               TeamName
	ArtifactID         FullyQualifiedArtifactName
	FriendlyName       HumanReadableArtifactName
	Description        ArtifactDescription
	Version            SemanticVersion // Preferrably from Git tag
	ArtifactType       ArtifactType    // Service, library, ...
	SCMURL             SCMURL
	SCMWebURL          SCMWebURL
	Topics             Topics
	DefinedInterfaces  []InterfaceDefinition
	DeploymentVariants []DeploymentVariant
	LandingPage        LandingPage
}

type ProjectType int

const (
	ProjectTypeEnterprise ProjectType = 0
	ProjectTypeSingle     ProjectType = 1
)

type ProjectOrganizationName string

type TeamName string

type FullyQualifiedArtifactName struct {
	System    SystemName // From Git path/name, can be overwritten in project.yaml
	Domain    DomainName
	Subdomain SubdomainName
	Artifact  ArtifactName
}
type SystemName string
type DomainName string
type SubdomainName string
type ArtifactName string
type HumanReadableArtifactName string
type ArtifactDescription string

func (n TeamName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n ProjectOrganizationName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n *FullyQualifiedArtifactName) IsMissing() bool {
	return n.System == "" && n.Domain == "" && n.Subdomain == "" && n.Artifact == ""
}

func (n SystemName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n DomainName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n SubdomainName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n ArtifactName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (lhs *ProjectDescription) IsSameArtifact(rhs *ProjectDescription) bool {
	return lhs.ArtifactID.Artifact == rhs.ArtifactID.Artifact &&
		lhs.ArtifactID.Subdomain == rhs.ArtifactID.Subdomain &&
		lhs.ArtifactID.Domain == rhs.ArtifactID.Domain &&
		lhs.ArtifactID.System == rhs.ArtifactID.System
}

func (n HumanReadableArtifactName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (p *ProjectDescription) InterfaceByLocalID(ID LocalInterfaceID) (*InterfaceDefinition, bool) {
	for _, iface := range p.DefinedInterfaces {
		if iface.LocalID == ID {
			return &iface, true
		}
	}
	return nil, false
}

type SemanticVersion struct {
	Major    MajorVersion
	Minor    MinorVersion
	Patch    PatchVersion
	Addendum VersionAddendum
}
type MajorVersion uint
type MinorVersion uint
type PatchVersion uint
type VersionAddendum struct {
	Type      AddendumType
	RCVersion RCVersion
}
type AddendumType int

const (
	AddendumUndefined        AddendumType = 0
	AddendumSnapshot         AddendumType = 1
	AddendumReleaseCandidate AddendumType = 2
	AddendumRelease          AddendumType = 3
)

type RCVersion uint

func (version *SemanticVersion) IsEmpty() bool {
	return version.Addendum.RCVersion == 0 && version.Addendum.Type == AddendumUndefined &&
		version.Major == 0 && version.Minor == 0 && version.Patch == 0
}

func (lhs *SemanticVersion) IsMoreRecentThan(rhs *SemanticVersion) bool {
	return (lhs.Major > rhs.Major) ||
		(lhs.Major == rhs.Major && lhs.Minor > rhs.Minor) ||
		(lhs.Major == rhs.Major && lhs.Minor == rhs.Minor && lhs.Patch > rhs.Patch) ||
		(lhs.Major == rhs.Major && lhs.Minor == rhs.Minor && lhs.Patch == rhs.Patch && lhs.Addendum.Type > rhs.Addendum.Type) ||
		(lhs.Major == rhs.Major && lhs.Minor == rhs.Minor && lhs.Patch == rhs.Patch && lhs.Addendum.Type == rhs.Addendum.Type && lhs.Addendum.RCVersion > rhs.Addendum.RCVersion)
}

type ArtifactType int

const (
	ArtifactTypeApplication ArtifactType = 0 // Web, mobile, desktop frontend
	ArtifactTypeDomain      ArtifactType = 1
	ArtifactTypeLibrary     ArtifactType = 2
	ArtifactTypeService     ArtifactType = 3 // Back-end
	ArtifactTypeSubdomain   ArtifactType = 4
	ArtifactTypeSystem      ArtifactType = 5
	ArtifactTypeTool        ArtifactType = 6 // Command line tool
)

type SCMURL url.URL
type SCMWebURL url.URL

// Topics are kind of tags.  One can add topics to project.yaml in order to tag
// the project.  These topics, or tags can be used for queries and evaluations.
// Another source for topics might be the GitLab tags,  not to be confused with
// Git tags.  The domain logic makes sure topics from project.yaml and optional
// GitLab crawlings are merged.  GenDoc tries to respect all tags, comming from
// project.yaml and GitLab.
type Topic string
type Topics map[Topic]*Topic

func NormalizedTopic(topic *string) Topic {
	return Topic(strings.ToLower(*topic))
}

// Destructive union
func (topics *Topics) Unite(topic *Topic) {
	if _, found := (*topics)[*topic]; !found {
		(*topics)[*topic] = topic
	}
}

type InterfaceDefinition struct {
	LocalID          LocalInterfaceID // Unique only within this project
	FriendlyName     InterfaceName
	Description      InterfaceDescription
	SpecificationURL SpecificationURL // URL to openAPI specification or similar
	Application      ApplicationSpec
	Communication    CommunicationSpec
	Transport        TransportSpec
}
type LocalInterfaceID string
type InterfaceName string
type InterfaceDescription string
type SpecificationURL url.URL
type ApplicationSpec struct {
	Format   AppFormat // Override here, should otherwise be extracted from specification
	Security ApplicationSecurity
}
type AppFormat string // Functional (not technical) format
type ApplicationSecurity string
type CommunicationSpec struct {
	Pattern  CommsPattern  // Req/Resp, Pub/Sub, ...
	Style    CommsStyle    // Sync, async, ...
	Format   CommsFormat   // JSON, XML, binary, ...
	Encoding CommsEncoding // UTF-8, ...
}
type CommsPattern string
type CommsStyle string
type CommsFormat string
type CommsEncoding string
type TransportSpec struct {
	Protocol TransportProtocol // http, amqp, mqtt, ...
	Security TransportSecurity // TLS 1.2, ...
}
type TransportProtocol string
type TransportSecurity string

// Usually, only one variant of an service is deployed, albeit multiple instan-
// ces or replicas usually exist to  scale and handle the load.  However, some-
// times multiple variants of the  same service exist.  They may behave exactly
// the same or differently. One reason for example might be the need for multi-
// tenancy.
type DeploymentVariant struct {
	LocalID                 DeploymentLocalID
	FriendlyName            VariantName
	Purpose                 PurposeOfVariant
	DeploymentDescriptorURL DeploymentDescriptorURL // Helm chart, for example
	ExposedInterfaces       []ExposedInterface
	ConsumedInterfaces      []ConsumedInterface
}

type DeploymentLocalID string
type VariantName string
type VariantDescription string
type PurposeOfVariant string
type DeploymentDescriptorURL url.URL

type ExposedInterface struct {
	LocalRef LocalInterfaceID // Unique within this project
}

type ConsumedInterface interface {
}

// Combinded key uniqely identifying the interface actually consumed:
//   - Artifact name and artifact local ID uniquely identify the functional in-
//     terface.
//   - Together with the variant ID  we also know which physical deployment va-
//     riant of an service is providing the functionality.
type ConsumedInternalInterface struct {
	ArtifactRef   FullyQualifiedArtifactName
	DeploymentRef DeploymentLocalID
	InterfaceRef  LocalInterfaceID
	SCMWebURL     InterfacesSCMWebURL
}

var _ ConsumedInterface = (*ConsumedInternalInterface)(nil)

type InterfacesSCMWebURL url.URL

type ConsumedExternalInterface struct {
	Organization ExternalOrganizationName
	System       ExternalSystemName
	Interface    ExternalInterfaceName
}

var _ ConsumedInterface = (*ConsumedExternalInterface)(nil)

type ExternalOrganizationName string
type ExternalSystemName string
type ExternalInterfaceName string

func (n DeploymentLocalID) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n LocalInterfaceID) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (p *ProjectDescription) HasDeploymentVariants() bool {
	return len(p.DeploymentVariants) > 0
}

func (p *ProjectDescription) DoesDefineInterfaces() bool {
	return len(p.DefinedInterfaces) > 0
}

func (p *ProjectDescription) DoesExposeInterfaces() bool {
	for _, deployment := range p.DeploymentVariants {
		if deployment.DoesExposeInterfaces() {
			return true
		}
	}
	return false
}

func (p *ProjectDescription) DoesConsumeInterfaces() bool {
	for _, deployment := range p.DeploymentVariants {
		if deployment.DoesConsumeInterfaces() {
			return true
		}
	}
	return false
}

func (d *DeploymentVariant) DoesExposeInterfaces() bool {
	return len(d.ExposedInterfaces) > 0
}

func (d *DeploymentVariant) DoesConsumeInterfaces() bool {
	return len(d.ConsumedInterfaces) > 0
}

func (n ExternalOrganizationName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n ExternalSystemName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

func (n ExternalInterfaceName) IsPresent() bool {
	return strings.TrimSpace(string(n)) != ""
}

type LandingPage struct {
	Title       LandingPageTitle
	Subtitle    LandingPageSubtitle
	Description LandingPageDescription
	Features    []LandingPageFeature
}
type LandingPageTitle string
type LandingPageSubtitle string
type LandingPageDescription string

type LandingPageFeature struct {
	Title       FeatureTitle
	Description FeatureDescription
}
type FeatureTitle string
type FeatureDescription string

type DevelopersArc42Chapters struct {
	IntroductionAndGoals    DeveloperArc42Chapter
	ArchitectureConstraints DeveloperArc42Chapter
	SystemScopeAndContext   DeveloperArc42Chapter
	SolutionStrategy        DeveloperArc42Chapter
	BuildingBlockView       DeveloperArc42Chapter
	RuntimeView             DeveloperArc42Chapter
	DeploymentView          DeveloperArc42Chapter
	Concepts                DeveloperArc42Chapter
	ArchitectureDecisions   DeveloperArc42Chapter
	QualityRequirements     DeveloperArc42Chapter
	TechnicalRisks          DeveloperArc42Chapter
	Glossary                DeveloperArc42Chapter
}

type DeveloperArc42Chapter string
