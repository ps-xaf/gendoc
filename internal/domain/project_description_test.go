//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/gendoc/internal/domain"
)

// ---- Empty artifact name ----------------------------------------------------
func TestRecognizeEmptyArtifactName(t *testing.T) {
	// GIVEN
	empty := domain.FullyQualifiedArtifactName{}
	// WHEN
	isEmpty := empty.IsMissing()
	// THEN
	assert.True(t, isEmpty)
}

func TestRecognizeNonEmptyArtifactName(t *testing.T) {
	// GIVEN
	nonEmpty := domain.FullyQualifiedArtifactName{Artifact: "myproject"}
	// WHEN
	isEmpty := nonEmpty.IsMissing()
	// THEN
	assert.False(t, isEmpty)
}

// ---- Empty semantic version ------------------------------------------------
func TestRecognizeEmptyVersion(t *testing.T) {
	// GIVEN
	empty := domain.SemanticVersion{}
	// WHEN
	isEmpty := empty.IsEmpty()
	// THEN
	assert.True(t, isEmpty)
}

func TestRecognizeNonEmptyVersion(t *testing.T) {
	// GIVEN
	nonEmpty := domain.SemanticVersion{Major: 1, Minor: 2, Patch: 3,
		Addendum: domain.VersionAddendum{Type: domain.AddendumReleaseCandidate, RCVersion: 4}}
	// WHEN
	isEmpty := nonEmpty.IsEmpty()
	// THEN
	assert.False(t, isEmpty)
}
