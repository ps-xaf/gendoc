//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/gendoc/internal/domain"
)

func TestSimpleSystemContextGraphConstrcutedFromSystemDescription(t *testing.T) {
	// GIVEN
	prjDescr := domain.ProjectDescription{
		FriendlyName: "System X",
		ArtifactType: domain.ArtifactTypeSystem,
		DefinedInterfaces: []domain.InterfaceDefinition{
			{
				LocalID:      "iface1",
				FriendlyName: "Interface 1",
			},
		},
		DeploymentVariants: []domain.DeploymentVariant{
			{
				LocalID:      "dep1",
				FriendlyName: "default deployment",
				ExposedInterfaces: []domain.ExposedInterface{
					{
						LocalRef: "iface1",
					},
				},
				ConsumedInterfaces: []domain.ConsumedInterface{
					&domain.ConsumedInternalInterface{
						ArtifactRef: domain.FullyQualifiedArtifactName{
							System: "sys2",
						},
						DeploymentRef: "dep2",
						InterfaceRef:  "iface2",
					},
				},
			},
		},
	}
	// WHEN
	graph := domain.SystemContextGraphFrom(&prjDescr)
	// THEN
	assert.Equal(t, domain.NodeName("System X"), graph.Name)
	assert.Equal(t, domain.ArchetypeSystemContext, graph.Archetype)
	//TODO further asserts
}
