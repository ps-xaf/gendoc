//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import (
	"sort"
)

type Node struct {
	Name      NodeName
	Archetype Archetype
	ID        NodeID
	relations map[RelationType]*[]*Node
}
type NodeName string
type Archetype int
type NodeID string

const (
	ArchetypeAnyArtifact         Archetype = 0 // We do not know exact artifact type in case of external systems. Might be a service, an application etc.
	ArchetypeApplication         Archetype = 1
	ArchetypeDeployment          Archetype = 2
	ArchetypeDomain              Archetype = 3
	ArchetypeDomainDiagram       Archetype = 4
	ArchetypeInterface           Archetype = 5
	ArchetypeInterfaceDefinition Archetype = 6
	ArchetypeLibrary             Archetype = 7
	ArchetypeOrganization        Archetype = 8
	ArchetypeService             Archetype = 9
	ArchetypeSubdomain           Archetype = 10
	ArchetypeSystem              Archetype = 11
	ArchetypeSystemContext       Archetype = 12
	ArchetypeTool                Archetype = 13
)

type RelationType int

const (
	RelationTypeContains          RelationType = 0 // Rendered as a nested box
	RelationTypeDeployedAs        RelationType = 1 // Rendered as an arrow
	RelationTypeExposesInterface  RelationType = 2 // Rendered as an arrow
	RelationTypeConsumesInterface RelationType = 3 // Rendered as an arrow
)

func (n *Node) addToRelation(other *Node, relType RelationType) {
	relation := n.relations[relType]
	if relation == nil {
		n.relations[relType] = &[]*Node{other}
	} else if !contains(relation, other) {
		*relation = append(*relation, other)
	}
}

func contains(nodes *[]*Node, other *Node) bool {
	for _, node := range *nodes {
		if node == other {
			return true
		}
	}
	return false
}

//---- Modifier methods -------------------------------------------------------

func (n *Node) Contains(other *Node) {
	n.addToRelation(other, RelationTypeContains)
}

func (n *Node) DeployedAs(other *Node) {
	n.addToRelation(other, RelationTypeDeployedAs)
}

func (n *Node) ExposesInterface(other *Node) {
	n.addToRelation(other, RelationTypeExposesInterface)
}

func (n *Node) ConsumesInterface(other *Node) {
	n.addToRelation(other, RelationTypeConsumesInterface)
}

func (n *Node) SortRelations() {
	for relation := range n.relations {
		sortRelation(n.relations[relation])
	}
}

func sortRelation(nodes *[]*Node) {
	sort.Slice(
		*nodes,
		func(i, j int) bool {
			return (*nodes)[i].Archetype < (*nodes)[j].Archetype ||
				((*nodes)[i].Archetype == (*nodes)[j].Archetype &&
					(*nodes)[i].Name < (*nodes)[j].Name)
		})
	for _, child := range *nodes {
		child.SortRelations()
	}
}

// ---- Predicate methods -----------------------------------------------------

func (n *Node) IsInterface() bool {
	return n.Archetype == ArchetypeInterface
}

//---- Accessor methods -------------------------------------------------------

func (n *Node) ContainedComponents() []*Node {
	if nodes, found := n.relations[RelationTypeContains]; found {
		return *nodes
	}
	return []*Node{}
}

func (n *Node) HoldsOtherComponents() bool {
	if nodes, found := n.relations[RelationTypeContains]; found {
		return len(*nodes) > 0
	}
	return false
}

func (n *Node) DeployedComponents() []*Node {
	if nodes, found := n.relations[RelationTypeDeployedAs]; found {
		return *nodes
	}
	return []*Node{}
}

func (n *Node) ExposedInterfaces() []*Node {
	if nodes, found := n.relations[RelationTypeExposesInterface]; found {
		return *nodes
	}
	return []*Node{}
}

func (n *Node) ConsumedInterfaces() []*Node {
	if nodes, found := n.relations[RelationTypeConsumesInterface]; found {
		return *nodes
	}
	return []*Node{}
}

// ---- Node builder ----------------------------------------------------------

func (n *Node) ContainsSystem(other *Node) {
	n.Contains(other)
}

func (n *Node) ContainsOrganization(other *Node) {
	n.Contains(other)
}

func (n *Node) HasDomain(other *Node) {
	n.Contains(other)
}

func (n *Node) HasSubdomain(other *Node) {
	n.Contains(other)
}

func (n *Node) ContainsArtifact(other *Node) {
	n.Contains(other)
}

func (n *Node) DefinesInterface(other *Node) {
	n.Contains(other)
}

func (n *Node) HasDeploymentVariant(other *Node) {
	n.Contains(other)
}

func (n *Node) IsDeployedAs(other *Node) {
	n.DeployedAs(other)
}
