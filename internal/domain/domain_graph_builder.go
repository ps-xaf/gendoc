//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

func DomainGraphFrom(rootProject *ProjectDescription, allProjects *[]*ProjectDescription) *Node {
	factory := NewNodeFactory()
	diagram := factory.AcquireDomainGraph(NodeName(rootProject.FriendlyName), NodeID(""))
	reconstructDomainStructure(factory, diagram, rootProject)
	for _, project := range *allProjects {
		reconstructDomainStructure(factory, diagram, project)
	}
	return diagram
}

func reconstructDomainStructure(factory *NodeFactory, parent *Node, prjDescr *ProjectDescription) {
	if prjDescr.Organization.IsPresent() {
		orga := factory.AcquireOrganizationNode(NodeName(prjDescr.Organization), parent.ID)
		parent.ContainsOrganization(orga)
		reconstructDomainStructureSystem(factory, orga, prjDescr)
	} else {
		reconstructDomainStructureSystem(factory, parent, prjDescr)
	}
}

func reconstructDomainStructureSystem(factory *NodeFactory, parent *Node, prjDescr *ProjectDescription) {
	if prjDescr.ArtifactID.System.IsPresent() {
		system := factory.AcquireSystemNode(NodeName(prjDescr.ArtifactID.System), parent.ID)
		parent.ContainsSystem(system)
		reconstructDomainStructureDomain(factory, system, prjDescr)
	} else {
		reconstructDomainStructureDomain(factory, parent, prjDescr)
	}
}

func reconstructDomainStructureDomain(factory *NodeFactory, parent *Node, prjDescr *ProjectDescription) {
	if prjDescr.ArtifactID.Domain.IsPresent() {
		domain := factory.AcquireDomainNode(NodeName(prjDescr.ArtifactID.Domain), parent.ID)
		parent.HasDomain(domain)
		reconstructDomainStructureSubdomain(factory, domain, prjDescr)
	} else {
		reconstructDomainStructureSubdomain(factory, parent, prjDescr)
	}
}

func reconstructDomainStructureSubdomain(factory *NodeFactory, parent *Node, prjDescr *ProjectDescription) {
	if prjDescr.ArtifactID.Subdomain.IsPresent() {
		subdomain := factory.AcquireSubdomainNode(NodeName(prjDescr.ArtifactID.Subdomain), parent.ID)
		parent.HasSubdomain(subdomain)
		reconstructDomainStructureArtifact(factory, subdomain, prjDescr)
	} else {
		reconstructDomainStructureArtifact(factory, parent, prjDescr)
	}
}

func reconstructDomainStructureArtifact(factory *NodeFactory, parent *Node, prjDescr *ProjectDescription) {
	if prjDescr.ArtifactID.Artifact.IsPresent() {
		var artifact *Node
		switch prjDescr.ArtifactType {
		case ArtifactTypeApplication:
			artifact = factory.AcquireApplicationNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		case ArtifactTypeLibrary:
			artifact = factory.AcquireLibraryNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		case ArtifactTypeService:
			artifact = factory.AcquireServiceNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		case ArtifactTypeSystem:
			artifact = factory.AcquireSystemNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		case ArtifactTypeTool:
			artifact = factory.AcquireToolNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		default:
			// TODO error
			artifact = factory.AcquireAnyArtifactNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
		}
		parent.ContainsArtifact(artifact)
	}
}
