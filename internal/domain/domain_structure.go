//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

type OrganizationName string

type MembershipStructure map[OrganizationName]OranizationMembershipStructure
type OranizationMembershipStructure map[SystemName]SystemMembershipStructure
type SystemMembershipStructure map[DomainName]DomainMembershipStructure
type DomainMembershipStructure map[SubdomainName]SubdomainMembershipStructure
type SubdomainMembershipStructure map[ArtifactName]*ProjectDescription

func MembershipStructureFrom(rootProject *ProjectDescription, allPrjDescrs *[]*ProjectDescription) *MembershipStructure {
	membershipStructure := make(MembershipStructure, 0)
	addToMembershipStructure(&membershipStructure, rootProject)
	for _, prjDescr := range *allPrjDescrs {
		addToMembershipStructure(&membershipStructure, prjDescr)
	}
	return &membershipStructure
}

func addToMembershipStructure(membershipStructure *MembershipStructure, prjDescr *ProjectDescription) {
	orgaName := OrganizationName(prjDescr.Organization)
	if (*membershipStructure)[orgaName] == nil {
		(*membershipStructure)[orgaName] = make(OranizationMembershipStructure, 0)
	}
	sysName := prjDescr.ArtifactID.System
	if (*membershipStructure)[orgaName][sysName] == nil {
		(*membershipStructure)[orgaName][sysName] = make(SystemMembershipStructure, 0)
	}
	domName := prjDescr.ArtifactID.Domain
	if (*membershipStructure)[orgaName][sysName][domName] == nil {
		(*membershipStructure)[orgaName][sysName][domName] = make(DomainMembershipStructure, 0)
	}
	subdomName := prjDescr.ArtifactID.Subdomain
	if (*membershipStructure)[orgaName][sysName][domName][subdomName] == nil {
		(*membershipStructure)[orgaName][sysName][domName][subdomName] = make(SubdomainMembershipStructure, 0)
	}
	artiName := prjDescr.ArtifactID.Artifact
	// if (*membershipStructure)[orgaName][sysName][domName][subdomName][artiName] != nil {
	// 	//TODO error
	// }
	(*membershipStructure)[orgaName][sysName][domName][subdomName][artiName] = prjDescr
}
