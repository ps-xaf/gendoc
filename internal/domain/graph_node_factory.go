//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import "fmt"

type NodeFactory struct {
	nodes map[NodeID]*Node
}

func NewNodeFactory() *NodeFactory {
	return &NodeFactory{nodes: map[NodeID]*Node{}}
}

func (f *NodeFactory) acquireNode(name NodeName, archetype Archetype, currPath NodeID) *Node {
	newID := id(currPath, archetype, name)
	if found, exists := f.nodes[newID]; exists {
		return found
	}
	newNode := &Node{
		Name:      name,
		Archetype: archetype,
		ID:        newID,
		relations: map[RelationType]*[]*Node{},
	}
	f.nodes[newID] = newNode
	return newNode
}

func id(currPath NodeID, archetype Archetype, name NodeName) NodeID {
	return NodeID(fmt.Sprintf("%s/%s:%s", currPath, archetypeIDPart[archetype], string(name)))
}

var archetypeIDPart = map[Archetype]string{
	ArchetypeApplication:         "APP",
	ArchetypeDeployment:          "DEP",
	ArchetypeDomain:              "DOM",
	ArchetypeDomainDiagram:       "DOMDIAG",
	ArchetypeInterface:           "IF",
	ArchetypeInterfaceDefinition: "IFDEF",
	ArchetypeLibrary:             "LIB",
	ArchetypeService:             "SRV",
	ArchetypeSubdomain:           "SUB",
	ArchetypeSystem:              "SYS",
	ArchetypeSystemContext:       "CTX",
	ArchetypeTool:                "TOOL",
	ArchetypeAnyArtifact:         "UNDEF",
}

// We may not know the exact  artifact type in case we  consume an interface of
// an  external system  (external to our repository).  Such artifacts mit be an
// application, a service etc.
func (f *NodeFactory) AcquireAnyArtifactNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeAnyArtifact, currPath)
}

func (f *NodeFactory) AcquireApplicationNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeApplication, currPath)
}

func (f *NodeFactory) AcquireDomainNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeDomain, currPath)
}

func (f *NodeFactory) AcquireDeploymentNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeDeployment, currPath)
}

func (f *NodeFactory) AcquireInterfaceDefinitionNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeInterfaceDefinition, currPath)
}

func (f *NodeFactory) AcquireInterfaceNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeInterface, currPath)
}

func (f *NodeFactory) AcquireLibraryNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeLibrary, currPath)
}

func (f *NodeFactory) AcquireOrganizationNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeOrganization, currPath)
}

func (f *NodeFactory) AcquireServiceNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeService, currPath)
}

func (f *NodeFactory) AcquireSubdomainNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeSubdomain, currPath)
}

func (f *NodeFactory) AcquireSystemContextGraph(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeSystemContext, currPath)
}

func (f *NodeFactory) AcquireDomainGraph(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeDomainDiagram, currPath)
}

func (f *NodeFactory) AcquireSystemNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeSystem, currPath)
}

func (f *NodeFactory) AcquireToolNode(name NodeName, currPath NodeID) *Node {
	return f.acquireNode(name, ArchetypeTool, currPath)
}
