//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// ---- HoldsInfoRequiredForSystemContext --------------------------------------

func TestHoldsInfoRequiredForSystemContext(t *testing.T) {
	// GIVEN
	prjDescr := ProjectDescription{
		DefinedInterfaces: []InterfaceDefinition{
			{
				LocalID: "myapi",
			},
		},
		DeploymentVariants: []DeploymentVariant{
			{
				LocalID: "default",
				ExposedInterfaces: []ExposedInterface{
					{
						LocalRef: "myapi",
					},
				},
				ConsumedInterfaces: []ConsumedInterface{
					&ConsumedInternalInterface{
						ArtifactRef: FullyQualifiedArtifactName{
							System: "othersys",
						},
						InterfaceRef: "otherapi",
					},
				},
			},
		},
	}
	// WHEN
	holds := prjDescr.HoldsInfoRequiredForSystemContext()
	// THEN
	assert.True(t, holds)
}
