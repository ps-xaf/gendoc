//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import (
	"fmt"

	"github.com/huandu/go-clone"
)

func EffectiveProjectDescription(projectYAML, versionProps, localGit *ProjectDescription) (*ProjectDescription, error) {
	//TODO currently we clone, but actually project.yaml, version.properties
	//     and info from Git has to be taken over and validated struct by struckt
	//     and field by field. The current workaround requires project.yaml to be
	//     populated completely and correctly.
	effectiveDescription := clone.Clone(projectYAML).(*ProjectDescription)
	if err := effectiveDescription.LandingPage.merge(&projectYAML.LandingPage); err != nil {
		return nil, err
	}
	if err := mergeArtifactID(projectYAML, localGit, effectiveDescription); err != nil {
		return nil, err
	}
	if err := mergeArtifactVersion(projectYAML, versionProps, localGit, effectiveDescription); err != nil {
		return nil, err
	}
	mergeRemoteOrigin(localGit, effectiveDescription)
	return effectiveDescription, nil
}

func (effectiveLandingPage *LandingPage) merge(projectYAMLLandingPage *LandingPage) error {
	if err := projectYAMLLandingPage.Violations(); err != nil {
		return fmt.Errorf("landing page from project.yaml invalid: %w", err)
	}
	effectiveLandingPage.Description = projectYAMLLandingPage.Description
	effectiveLandingPage.Features = make([]LandingPageFeature, len(projectYAMLLandingPage.Features))
	for i, feature := range projectYAMLLandingPage.Features {
		effectiveLandingPage.Features[i].Description = feature.Description
		effectiveLandingPage.Features[i].Title = feature.Title
	}
	effectiveLandingPage.Subtitle = projectYAMLLandingPage.Subtitle
	effectiveLandingPage.Title = projectYAMLLandingPage.Title
	return nil
}

func mergeArtifactID(projectYAML, localGit *ProjectDescription, effectivePrjDescr *ProjectDescription) error {
	artifactType := projectYAML.ArtifactType
	if projectYAML.ArtifactID.IsMissing() {
		if localGit.ArtifactID.IsMissing() {
			return fmt.Errorf("no artifact ID given, neither in project.yaml nor in local Git repo")
		}
		if err := localGit.ArtifactID.Violations(artifactType); err != nil {
			return fmt.Errorf("no artifact ID given in project.yaml. But artifact ID from local Git repo invalid: %w", err)
		}
		effectivePrjDescr.ArtifactID.Artifact = localGit.ArtifactID.Artifact
		effectivePrjDescr.ArtifactID.Domain = localGit.ArtifactID.Domain
		effectivePrjDescr.ArtifactID.Subdomain = localGit.ArtifactID.Subdomain
		effectivePrjDescr.ArtifactID.System = localGit.ArtifactID.System
	}
	if err := projectYAML.ArtifactID.Violations(artifactType); err != nil {
		return fmt.Errorf("artifact ID from project.yaml invalid: %w", err)
	}
	effectivePrjDescr.ArtifactID.Artifact = projectYAML.ArtifactID.Artifact
	effectivePrjDescr.ArtifactID.Domain = projectYAML.ArtifactID.Domain
	effectivePrjDescr.ArtifactID.Subdomain = projectYAML.ArtifactID.Subdomain
	effectivePrjDescr.ArtifactID.System = projectYAML.ArtifactID.System
	return nil
}

func mergeArtifactVersion(projectYAML, versionProps, localGit *ProjectDescription, effectivePrjDescr *ProjectDescription) error {
	if projectYAML.Version.IsEmpty() {
		if versionProps.Version.IsEmpty() {
			if localGit.Version.IsEmpty() {
				return fmt.Errorf("no artifact version given in project.yaml, version.properties nor in local Git repo")
			}
			effectivePrjDescr.Version.Addendum.RCVersion = localGit.Version.Addendum.RCVersion
			effectivePrjDescr.Version.Addendum.Type = localGit.Version.Addendum.Type
			effectivePrjDescr.Version.Major = localGit.Version.Major
			effectivePrjDescr.Version.Minor = localGit.Version.Minor
			effectivePrjDescr.Version.Patch = localGit.Version.Patch
			return nil
		}
		effectivePrjDescr.Version.Addendum.RCVersion = versionProps.Version.Addendum.RCVersion
		effectivePrjDescr.Version.Addendum.Type = versionProps.Version.Addendum.Type
		effectivePrjDescr.Version.Major = versionProps.Version.Major
		effectivePrjDescr.Version.Minor = versionProps.Version.Minor
		effectivePrjDescr.Version.Patch = versionProps.Version.Patch
		return nil
	}
	effectivePrjDescr.Version.Addendum.RCVersion = projectYAML.Version.Addendum.RCVersion
	effectivePrjDescr.Version.Addendum.Type = projectYAML.Version.Addendum.Type
	effectivePrjDescr.Version.Major = projectYAML.Version.Major
	effectivePrjDescr.Version.Minor = projectYAML.Version.Minor
	effectivePrjDescr.Version.Patch = projectYAML.Version.Patch
	return nil
}

func mergeRemoteOrigin(localGit *ProjectDescription, effectivePrjDescr *ProjectDescription) {
	effectivePrjDescr.SCMURL = localGit.SCMURL
}
