//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/gendoc/internal/domain"
)

// ---- ArtefactID Violations function -----------------------------------------

func TestArtifactMustNotBeEmptyInArtifactID(t *testing.T) {
	// GIVEN
	emptyArtefactID := domain.FullyQualifiedArtifactName{}
	// WHEN
	violation := emptyArtefactID.Violations(domain.ArtifactTypeApplication)
	// THEN
	assert.EqualError(t, violation, "empty artifact (application) name")
}

func TestSubdomainRequiresDomainInArtifactID(t *testing.T) {
	// GIVEN
	emptyArtefactID := domain.FullyQualifiedArtifactName{Subdomain: "some subdomain", Artifact: "some artifact"}
	// WHEN
	violation := emptyArtefactID.Violations(domain.ArtifactTypeSubdomain)
	// THEN
	assert.EqualError(t, violation, "cannot have a subdomain without domain")
}
