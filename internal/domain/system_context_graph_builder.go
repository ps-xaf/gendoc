//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

func SystemContextGraphFrom(prjDescr *ProjectDescription) *Node {
	factory := NewNodeFactory()
	systemContext := factory.AcquireSystemContextGraph(NodeName(prjDescr.FriendlyName), NodeID(""))
	domainStructure := describeDomainStructure(prjDescr, factory, systemContext)

	// TODO make identical to domain/artifact of consumed interfaces below
	var artifact *Node = domainStructure
	if prjDescr.ArtifactID.Artifact.IsPresent() {
		artifact = artifactNode(prjDescr, factory, domainStructure)
		domainStructure.ContainsArtifact(artifact)
	}

	describeInterfacesDefinedByArtifact(prjDescr, factory, artifact)
	describeDeploymentsOfArtifact(prjDescr, factory, artifact, systemContext)
	return systemContext
}

//---- Domain structure/artifact ----------------------------------------------

func describeDomainStructure(prjDescr *ProjectDescription, factory *NodeFactory, parent *Node) *Node {
	if prjDescr.Organization.IsPresent() {
		orga := factory.AcquireOrganizationNode(NodeName(prjDescr.Organization), parent.ID)
		parent.ContainsOrganization(orga)
		return describeDomainStructureSystem(prjDescr, factory, orga)
	}
	return describeDomainStructureSystem(prjDescr, factory, parent)
}

func describeDomainStructureSystem(prjDescr *ProjectDescription, factory *NodeFactory, parent *Node) *Node {
	if prjDescr.ArtifactID.System.IsPresent() {
		system := factory.AcquireSystemNode(NodeName(prjDescr.ArtifactID.System), parent.ID)
		parent.ContainsSystem(system)
		return describeDomainStructureDomain(prjDescr, factory, system)
	}
	return describeDomainStructureDomain(prjDescr, factory, parent)
}

func describeDomainStructureDomain(prjDescr *ProjectDescription, factory *NodeFactory, parent *Node) *Node {
	if prjDescr.ArtifactID.Domain.IsPresent() {
		domain := factory.AcquireDomainNode(NodeName(prjDescr.ArtifactID.Domain), parent.ID)
		parent.HasDomain(domain)
		return describeDomainStructureSubdomain(prjDescr, factory, domain)
	}
	return describeDomainStructureSubdomain(prjDescr, factory, parent)
}

func describeDomainStructureSubdomain(prjDescr *ProjectDescription, factory *NodeFactory, parent *Node) *Node {
	if prjDescr.ArtifactID.Subdomain.IsPresent() {
		subdomain := factory.AcquireSubdomainNode(NodeName(prjDescr.ArtifactID.Subdomain), parent.ID)
		parent.HasSubdomain(subdomain)
		return subdomain
	}
	return parent
}

func artifactNode(prjDescr *ProjectDescription, factory *NodeFactory, parent *Node) *Node {
	name := NodeName(prjDescr.ArtifactID.Artifact)
	switch prjDescr.ArtifactType {
	case ArtifactTypeApplication:
		return factory.AcquireApplicationNode(name, parent.ID)
	case ArtifactTypeLibrary:
		return factory.AcquireLibraryNode(name, parent.ID)
	case ArtifactTypeService:
		return factory.AcquireServiceNode(name, parent.ID)
	case ArtifactTypeTool:
		return factory.AcquireToolNode(name, parent.ID)
	default:
		return factory.AcquireServiceNode(NodeName(prjDescr.ArtifactID.Artifact), parent.ID)
	}
}

//---- Defined interfaces -----------------------------------------------------

func describeInterfacesDefinedByArtifact(prjDescr *ProjectDescription, factory *NodeFactory, artifact *Node) {
	for _, iface := range prjDescr.DefinedInterfaces {
		ifaceDef := factory.AcquireInterfaceDefinitionNode(NodeName(iface.LocalID), artifact.ID)
		artifact.DefinesInterface(ifaceDef)
	}
}

//---- Deployments/service variants -------------------------------------------

func describeDeploymentsOfArtifact(prjDescr *ProjectDescription, factory *NodeFactory, artifact *Node, systemContext *Node) {
	for _, deploymentVariant := range prjDescr.DeploymentVariants {
		deployment := factory.AcquireDeploymentNode(NodeName(deploymentVariant.LocalID), artifact.ID)
		systemContext.HasDeploymentVariant(deployment)
		artifact.IsDeployedAs(deployment)
		describeExposedInterfaces(prjDescr, &deploymentVariant, factory, deployment, systemContext)
		describeConsumedInterfaces(&deploymentVariant, factory, deployment, systemContext)
	}
}

func describeExposedInterfaces(prjDescr *ProjectDescription, deploymentVariant *DeploymentVariant, factory *NodeFactory, deployment *Node, systemContext *Node) {
	for _, iface := range deploymentVariant.ExposedInterfaces {
		if ifaceDetails, found := prjDescr.InterfaceByLocalID(iface.LocalRef); found {
			exposedIface := factory.AcquireInterfaceNode(NodeName(ifaceDetails.LocalID), deployment.ID)
			systemContext.Contains(exposedIface)
			deployment.ExposesInterface(exposedIface)
		}
		//TODO error, if interface NOT found
	}
}

func describeConsumedInterfaces(deploymentVariant *DeploymentVariant, factory *NodeFactory, deployment *Node, systemContext *Node) {
	for _, iface := range deploymentVariant.ConsumedInterfaces {
		switch consumedIface := iface.(type) {
		case *ConsumedExternalInterface:
			describeConsumedExternalInterface(consumedIface, factory, deployment, systemContext)
		case *ConsumedInternalInterface:
			describeConsumedInternalInterface(consumedIface, factory, deployment, systemContext)
			//TODO default error
		}
	}
}

func describeConsumedExternalInterface(iface *ConsumedExternalInterface, factory *NodeFactory, deployment *Node, systemContext *Node) {
	exposedInterface := describeConsumedExternalInterfaceStructureOrganization(iface, factory, systemContext)
	//systemContext.Contains(exposedInterface)
	deployment.ConsumesInterface(exposedInterface)
}

func describeConsumedExternalInterfaceStructureOrganization(iface *ConsumedExternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.Organization.IsPresent() {
		orga := factory.AcquireOrganizationNode(NodeName(iface.Organization), parent.ID)
		parent.ContainsOrganization(orga)
		return describeConsumedExternalInterfaceStructureSystem(iface, factory, orga)
	}
	return describeConsumedExternalInterfaceStructureSystem(iface, factory, parent)
}

func describeConsumedExternalInterfaceStructureSystem(iface *ConsumedExternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.System.IsPresent() {
		system := factory.AcquireSystemNode(NodeName(iface.System), parent.ID)
		parent.ContainsSystem(system)
		return describeConsumedExternalInterfaceStructureInterface(iface, factory, system)
	}
	return describeConsumedExternalInterfaceStructureInterface(iface, factory, parent)
}

func describeConsumedExternalInterfaceStructureInterface(iface *ConsumedExternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.Interface.IsPresent() {
		consumedIface := factory.AcquireInterfaceNode(NodeName(iface.Interface), parent.ID)
		parent.DefinesInterface(consumedIface)
		return consumedIface
	}
	return parent
}

func describeConsumedInternalInterface(iface *ConsumedInternalInterface, factory *NodeFactory, deployment *Node, systemContext *Node) {
	ifaceDomainStructure := describeDomainStructureOfConsumedInterface(iface, factory, systemContext)

	definedIface := factory.AcquireInterfaceDefinitionNode(NodeName(iface.InterfaceRef), ifaceDomainStructure.ID)
	ifaceDomainStructure.DefinesInterface(definedIface)

	var consumedDeployment *Node = ifaceDomainStructure
	if iface.DeploymentRef.IsPresent() {
		consumedDeployment = factory.AcquireDeploymentNode(NodeName(iface.DeploymentRef), ifaceDomainStructure.ID)
		systemContext.HasDeploymentVariant(consumedDeployment)
		ifaceDomainStructure.IsDeployedAs(consumedDeployment)
	}

	exposedInterface := factory.AcquireInterfaceNode(NodeName(iface.InterfaceRef), consumedDeployment.ID)
	systemContext.Contains(exposedInterface)
	consumedDeployment.ExposesInterface(exposedInterface)

	deployment.ConsumesInterface(exposedInterface)
}

func describeDomainStructureOfConsumedInterface(iface *ConsumedInternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.ArtifactRef.System.IsPresent() {
		system := factory.AcquireSystemNode(NodeName(iface.ArtifactRef.System), parent.ID)
		parent.ContainsSystem(system)
		return describeDomainStructureDomainOfConsumedInterface(iface, factory, system)
	}
	return describeDomainStructureDomainOfConsumedInterface(iface, factory, parent)
}

func describeDomainStructureDomainOfConsumedInterface(iface *ConsumedInternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.ArtifactRef.Domain.IsPresent() {
		domain := factory.AcquireDomainNode(NodeName(iface.ArtifactRef.Domain), parent.ID)
		parent.HasDomain(domain)
		return describeDomainStructureSubdomainOfConsumedInterface(iface, factory, domain)
	}
	return describeDomainStructureSubdomainOfConsumedInterface(iface, factory, parent)
}

func describeDomainStructureSubdomainOfConsumedInterface(iface *ConsumedInternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.ArtifactRef.Subdomain.IsPresent() {
		subdomain := factory.AcquireSubdomainNode(NodeName(iface.ArtifactRef.Subdomain), parent.ID)
		parent.HasSubdomain(subdomain)
		return describeDomainStructureArtifactOfConsumedInterface(iface, factory, subdomain)
	}
	return describeDomainStructureArtifactOfConsumedInterface(iface, factory, parent)
}

func describeDomainStructureArtifactOfConsumedInterface(iface *ConsumedInternalInterface, factory *NodeFactory, parent *Node) *Node {
	if iface.ArtifactRef.Artifact.IsPresent() {
		artifact := factory.AcquireAnyArtifactNode(NodeName(iface.ArtifactRef.Artifact), parent.ID)
		parent.ContainsArtifact(artifact)
		return artifact
	}
	return parent
}
