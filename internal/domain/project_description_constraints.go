//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

import (
	"fmt"
	"strings"
)

//---- Artifact ID ------------------------------------------------------------

func (artifactID *FullyQualifiedArtifactName) Violations(articatType ArtifactType) error {
	if articatType == ArtifactTypeApplication && strings.TrimSpace(string(artifactID.Artifact)) == "" {
		return fmt.Errorf("empty artifact (application) name")
	}
	if articatType == ArtifactTypeLibrary && strings.TrimSpace(string(artifactID.Artifact)) == "" {
		return fmt.Errorf("empty artifact (library) name")
	}
	if articatType == ArtifactTypeService && strings.TrimSpace(string(artifactID.Artifact)) == "" {
		return fmt.Errorf("empty artifact (service) name")
	}
	if articatType == ArtifactTypeTool && strings.TrimSpace(string(artifactID.Artifact)) == "" {
		return fmt.Errorf("empty artifact (tool) name")
	}
	if articatType == ArtifactTypeSystem && strings.TrimSpace(string(artifactID.System)) == "" {
		return fmt.Errorf("empty system name")
	}
	if articatType == ArtifactTypeDomain && strings.TrimSpace(string(artifactID.Domain)) == "" {
		return fmt.Errorf("empty domain name")
	}
	if articatType == ArtifactTypeSubdomain && strings.TrimSpace(string(artifactID.Subdomain)) == "" {
		return fmt.Errorf("empty subdomain name")
	}
	if strings.TrimSpace(string(artifactID.Domain)) == "" && strings.TrimSpace(string(artifactID.Subdomain)) != "" {
		return fmt.Errorf("cannot have a subdomain without domain")
	}
	return nil
}

//---- Landing page -----------------------------------------------------------

func (landingPage *LandingPage) Violations() error {
	if strings.TrimSpace(string(landingPage.Description)) == "" {
		return fmt.Errorf("description for landing page missing")
	}
	if strings.TrimSpace(string(landingPage.Subtitle)) == "" {
		return fmt.Errorf("subtitle for landing page missing")
	}
	if strings.TrimSpace(string(landingPage.Title)) == "" {
		return fmt.Errorf("title for landing page missing")
	}
	for _, feature := range landingPage.Features {
		if strings.TrimSpace(string(feature.Description)) == "" {
			return fmt.Errorf("description for feature on landing page missing")
		}
		if strings.TrimSpace(string(feature.Title)) == "" {
			return fmt.Errorf("title for feature on landing page missing")
		}
	}
	return nil
}
