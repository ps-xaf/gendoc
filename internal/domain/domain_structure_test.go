//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/gendoc/internal/domain"
)

func TestFullyQualifiedArtifactAddedToMembershipStructure(t *testing.T) {
	// GIVEN
	orgaName := domain.ProjectOrganizationName("Damage Inc.")
	sysName := domain.SystemName("Destructor")
	domName := domain.DomainName("Raw Destruction")
	subdomName := domain.SubdomainName("Individuals")
	artiName := domain.ArtifactName("Axe")
	prjDescr := domain.ProjectDescription{
		FriendlyName: "R.I.P.",
		Organization: orgaName,
		ArtifactID: domain.FullyQualifiedArtifactName{
			System:    sysName,
			Domain:    domName,
			Subdomain: subdomName,
			Artifact:  artiName,
		},
	}
	var allProjects = []*domain.ProjectDescription{
		&prjDescr,
	}
	// WHEN
	membership := domain.MembershipStructureFrom(&prjDescr, &allProjects)
	// THEN
	assert.Equal(t,
		domain.HumanReadableArtifactName("R.I.P."),
		(*membership)[domain.OrganizationName(orgaName)][sysName][domName][subdomName][artiName].FriendlyName)
	assert.Equal(t, 1, len(*membership))
}

func TestIncompletelyQualifiedArtifactAddedToMembershipStructure(t *testing.T) {
	// GIVEN
	orgaName := domain.ProjectOrganizationName("Damage Inc.")
	artiName := domain.ArtifactName("Axe")
	prjDescr := domain.ProjectDescription{
		FriendlyName: "R.I.P.",
		Organization: orgaName,
		ArtifactID: domain.FullyQualifiedArtifactName{
			Artifact: artiName,
		},
	}
	var allProjects = []*domain.ProjectDescription{
		&prjDescr,
	}
	// WHEN
	membership := domain.MembershipStructureFrom(&prjDescr, &allProjects)
	// THEN
	assert.Equal(t,
		domain.HumanReadableArtifactName("R.I.P."),
		(*membership)[domain.OrganizationName(orgaName)][""][""][""][artiName].FriendlyName)
	assert.Equal(t, 1, len(*membership))
}
