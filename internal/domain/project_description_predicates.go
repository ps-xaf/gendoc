//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package domain

func (p *ProjectDescription) HoldsInfoRequiredForSystemContext() bool {
	return (p.DoesDefineInterfaces() && p.DoesExposeInterfaces()) || p.DoesConsumeInterfaces()
}
