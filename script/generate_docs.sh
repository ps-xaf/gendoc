#!/usr/bin/env bash

###############################################################################
#
# GenDoc - Meta documentation generator for docToolchain
#
# (c) 2023 Holger Zahnleiter
#
###############################################################################

set -eou pipefail

# Generate AsciiDoc files as input to docToolchain. The AsciiDoc files are com-
# piled from user documentation (docs folder), meta information (Git repository
# information, version.properties, project.yaml, ...)  and arc42 templates pro-
# vided by docToolchain.
gendoc "$@"

# The generated  files are now available in the build_docs folder.  The dot (.)
# actually represents the  build_docs  folder as this  script is  executed from
# within that folder.  Now run the  docToolchain to  generate the actual  micro
# site.
cd build_docs
doctoolchain . generateSite -PinputPath=. -PmainConfigFile=docToolchain.groovy

exit
