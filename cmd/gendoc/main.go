//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/usecase"
)

var langFlag string
var prjTypeFlag string
var scmURLFlag string
var scmAccessTokenFlag string
var scmCrawlOwnedFlag string

func init() {
	flag.StringVar(&langFlag, "lang", "en", "choose one language out of 'de', 'en', 'es'")
	flag.StringVar(&prjTypeFlag, "type", "enterprise", "chose one project type from 'enterprise' or 'single'")
	flag.StringVar(&scmURLFlag, "scm-url", "", "choose an SCM to crawl for additional information (optional)")
	flag.StringVar(&scmAccessTokenFlag, "scm-token", "", "token for accessing given SCM, mandatory if scm-url is given, omit else")
	flag.StringVar(&scmCrawlOwnedFlag, "scm-crawl-owned", "", "'true' means only crawl SCM repositories owned by access token, 'else' means crawl all repos")
}

func main() {
	flag.Parse()
	language, err := LanguageFromFlag(langFlag)
	if err != nil {
		panic(fmt.Sprintf("Command line error: %s", err.Error()))
	}
	projectType, err := ProjectTypeFromFlag(prjTypeFlag)
	if err != nil {
		panic(fmt.Sprintf("Command line error: %s", err.Error()))
	}
	currDir, err := os.Getwd()
	if err != nil {
		panic(fmt.Sprintf("Failed to determine current working directory: %s", err.Error()))
	}
	scmURL, err := SCMURLFromFlag(scmURLFlag)
	if err != nil {
		panic(fmt.Sprintf("Command line error: %s", err.Error()))
	}
	accessToken, err := SCMAccessTokenFromFlag(scmURLFlag, scmAccessTokenFlag)
	if err != nil {
		panic(fmt.Sprintf("Command line error: %s", err.Error()))
	}
	crawlOwned, err := SCMCrawlOwnedFromFlag(scmURLFlag, scmAccessTokenFlag, scmCrawlOwnedFlag)
	if err != nil {
		panic(fmt.Sprintf("Command line error: %s", err.Error()))
	}
	config := GeneratorConfiguration{
		Language:    language,
		ProjectType: projectType,
		// Mounted into container
		ProjectRepository: ProjectConfiguration{
			ProjectYAMLPath:       filepath.Join(currDir, "project.yaml"),
			VersionPropertiesPath: filepath.Join(currDir, "version.properties"),
			LocalGitRepoPath:      currDir,
			DeveloperChaptersPath: filepath.Join(currDir, "docs"),
			BuildPath:             filepath.Join(currDir, "build_docs"),
		},
		// Embedded in container image
		GenDocFiles: GenDocFilesConfiguration{
			Arc42TemplatesPath: "/doc_templates",
		},
		CrawlableSCM: CrawlableSCM{
			URL:         *scmURL,
			accessToken: accessToken,
			crawlOwned:  crawlOwned,
		},
	}
	err = GenerateDocumentation(config)
	if err != nil {
		panic(fmt.Sprintf("Failed to generate documentation: %s", err.Error()))
	}
}

func LanguageFromFlag(flag string) (usecase.LanguageCode, error) {
	normalized := strings.ToLower(flag)
	if normalized == "de" {
		return usecase.LangDE, nil
	} else if normalized == "en" {
		return usecase.LangEN, nil
	} else if normalized == "es" {
		return usecase.LangES, nil
	} else {
		return usecase.LangEN, fmt.Errorf("unknown language flag %s", flag)
	}
}

func ProjectTypeFromFlag(flag string) (domain.ProjectType, error) {
	normalized := strings.ToLower(flag)
	if normalized == "enterprise" || normalized == "enterprize" {
		return domain.ProjectTypeEnterprise, nil
	} else if normalized == "single" {
		return domain.ProjectTypeSingle, nil
	} else {
		return domain.ProjectTypeEnterprise, fmt.Errorf("unknown project type flag %s", flag)
	}
}

func SCMURLFromFlag(scmURLFlag string) (*url.URL, error) {
	return url.Parse(scmURLFlag)
}

func SCMAccessTokenFromFlag(scmURLFlag string, scmToken string) (string, error) {
	if strings.TrimSpace(scmURLFlag) == "" {
		if strings.TrimSpace(scmToken) != "" {
			return "", fmt.Errorf("a token for accessing an SCM is given, but URL of SCM is missing")
		}
		return "", nil
	}
	if strings.TrimSpace(scmToken) == "" {
		return "", fmt.Errorf("an SCM URL is given, but token for accessing that SCM is missing")
	}
	return scmToken, nil
}

func SCMCrawlOwnedFromFlag(scmURLFlag string, scmToken string, scmOwned string) (bool, error) {
	normalized := strings.ToLower(strings.TrimSpace(scmOwned))
	if normalized == "" {
		return true, nil // Conservative default: Crawl only repos owned by token
	}
	if strings.TrimSpace(scmURLFlag) == "" {
		return true, fmt.Errorf("setting flag 'scm-crawl-owned' does not make sense as no SCM URL given")
	}
	if strings.TrimSpace(scmToken) == "" {
		return true, fmt.Errorf("setting flag 'scm-crawl-owned' does not make sense as no SCM Token given")
	}
	if normalized == "true" || normalized == "yes" || normalized == "1" {
		return true, nil // Crawl only repos owned by token
	} else if normalized == "false" || normalized == "no" || normalized == "0" {
		return false, nil // Crawl only repos owned by token
	} else {
		return true, fmt.Errorf("invalid value for 'scm-crawl-owned', only 'true' and 'false' allowed")
	}
}
