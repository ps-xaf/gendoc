//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/gendoc/internal/usecase"
)

func TestLangFlagDE(t *testing.T) {
	// GIVEN
	flag := "de"
	// WHEN
	code, err := LanguageFromFlag(flag)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, usecase.LangDE, code)
}

func TestLangFlagEN(t *testing.T) {
	// GIVEN
	flag := "en"
	// WHEN
	code, err := LanguageFromFlag(flag)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, usecase.LangEN, code)
}

func TestLangFlagES(t *testing.T) {
	// GIVEN
	flag := "es"
	// WHEN
	code, err := LanguageFromFlag(flag)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, usecase.LangES, code)
}

func TestLangFlagCaseDoesNotMatter(t *testing.T) {
	// GIVEN
	flag := "DE"
	// WHEN
	code, err := LanguageFromFlag(flag)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, usecase.LangDE, code)
}

func TestLangFlagNotRecognized(t *testing.T) {
	// GIVEN
	flag := "unknown language code"
	// WHEN
	_, err := LanguageFromFlag(flag)
	// THEN
	assert.Error(t, err)
}
