//*****************************************************************************
//
// GenDoc - Meta documentation generator for docToolchain
//
// (c) 2023 Holger Zahnleiter
//
//*****************************************************************************

package main

import (
	"net/url"
	"path/filepath"
	"strings"

	"zahnleiter.org/gendoc/internal/domain"
	"zahnleiter.org/gendoc/internal/infrastructure"
	"zahnleiter.org/gendoc/internal/usecase"
)

type GeneratorConfiguration struct {
	Language          usecase.LanguageCode
	ProjectType       domain.ProjectType
	ProjectRepository ProjectConfiguration
	GenDocFiles       GenDocFilesConfiguration
	CrawlableSCM      CrawlableSCM
}

type ProjectConfiguration struct {
	ProjectYAMLPath       string
	VersionPropertiesPath string
	LocalGitRepoPath      string
	DeveloperChaptersPath string
	BuildPath             string
}

type GenDocFilesConfiguration struct {
	Arc42TemplatesPath string
}

type CrawlableSCM struct {
	URL         url.URL
	accessToken string
	crawlOwned  bool
}

func (c *CrawlableSCM) IsPresent() bool {
	if strings.TrimSpace(c.URL.Scheme) != "" && strings.TrimSpace(c.URL.Host) != "" {
		return true
	}
	return false
}

// Folders at the root of the file system, e.g. /doc_templates, are directories
// that are part of the container image. Folders starting with ProjectDirectory
// are folders that  are mounted insided the container.  These are actually the
// files from the Git repo which is to be documented.
func GenerateDocumentation(config GeneratorConfiguration) error {
	projectYAMLReader := infrastructure.NewProjectYAMLSource(
		config.ProjectRepository.ProjectYAMLPath, config.ProjectType)
	versionPropertiesReader := infrastructure.NewVersionPropertiesSource(
		config.ProjectRepository.VersionPropertiesPath)
	localSCMReader := infrastructure.NewLocalGitRepoSource(
		config.ProjectRepository.LocalGitRepoPath, config.ProjectType)
	templatesSource := infrastructure.NewArc42TemplateFileSource(
		config.GenDocFiles.Arc42TemplatesPath)
	developerChapterSources := infrastructure.NewDevelopersArc42ChaptersFileSource(
		config.ProjectRepository.DeveloperChaptersPath)
	sysContextRenderer := usecase.NewPlantUMLRenderer()
	templateEngine := infrastructure.GoTemplateEngine
	imageCopier := infrastructure.NewDocumentsFileCopier(
		DeveloperImagesPath(&config.ProjectRepository), ImageDestinationPath(&config.ProjectRepository))
	additionalDocsCopier := infrastructure.NewDocumentsFileCopier(
		config.ProjectRepository.DeveloperChaptersPath, AdditionalDocsDestinationPath(&config.ProjectRepository))
	docDest := infrastructure.NewDocumentFileDestination(
		config.ProjectRepository.BuildPath)
	var collector usecase.ProjectDescriptionsCollector
	if config.CrawlableSCM.IsPresent() {
		collector = usecase.NewSCMProjectDescriptionsCollector(
			infrastructure.NewGitLabCrawler(
				&config.CrawlableSCM.URL, config.CrawlableSCM.accessToken, config.CrawlableSCM.crawlOwned),
			infrastructure.NewProjectDescriptionGitReader(config.ProjectType))
	} else {
		collector = usecase.NewNoProjectDescriptionsCollector()
	}
	generator := usecase.NewUseCaseGenerateDocumentation().
		WithProjectYAMLFrom(projectYAMLReader).
		WithVersionPropertiesFrom(versionPropertiesReader).
		WithLocalSCMInfosFrom(localSCMReader).
		WithTemplatesFrom(templatesSource).
		WithDeveloperChaptersFrom(developerChapterSources).
		WithSystemContextDiagramRenderer(sysContextRenderer).
		WithTemplateEngine(templateEngine).
		WithImageCopier(imageCopier).
		WithAdditionalDocumentsCopier(additionalDocsCopier).
		WithDocumentationStoredAt(docDest).
		WithProjectDescriptionsCollector(collector)
	return generator.Generate(config.Language)
}

func DeveloperImagesPath(cfg *ProjectConfiguration) string {
	return filepath.Join(cfg.DeveloperChaptersPath, "images")
}

func ImageDestinationPath(cfg *ProjectConfiguration) string {
	return filepath.Join(cfg.BuildPath, "docs", "images")
}

func AdditionalDocsDestinationPath(cfg *ProjectConfiguration) string {
	return filepath.Join(cfg.BuildPath, "docs", "arc42", "chapters")
}
